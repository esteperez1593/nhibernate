﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class AreaController : ApiController
    {


        [HttpGet]
        [Route("api/PZBAREA/GetPZBAREA/")]
        public List<Area> GetPZBAREA()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Area).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAreaRepository repository = new AreaRepository();

            List<Area> respuesta = repository.GetPZBAREA();


            return respuesta;
        }
    }
}
