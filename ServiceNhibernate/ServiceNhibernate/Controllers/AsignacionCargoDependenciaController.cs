﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class AsignacionCargoDependenciaController : ApiController
    {
        [Route("api/AsignacionCargoDependencia/GetAsignacionCargoDependencia/")]
        [HttpGet]
        public List<AsignacionCargoDependencia> GetAsignacionCargoDependencia()
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionCargoDependencia).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionCargoDependenciaRepository repository = new AsignacionCargoDependenciaRepository();

            List<AsignacionCargoDependencia> respuesta = repository.GetAll();


            return respuesta;
        }
        /* GetEvaluados(String idcar, String iddep)*/
        [Route("api/PZRDYPT/GetEvaluados/{idcar}/{iddep}")]
        [HttpGet]
        public List<AsignacionCargoPersona> GetEvaluados(String idcar, String iddep)
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionCargoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionCargoDependenciaRepository repository = new AsignacionCargoDependenciaRepository();

            List<AsignacionCargoPersona> respuesta = repository.GetEvaluados(idcar, iddep);


            return respuesta;
        }

    }
}
