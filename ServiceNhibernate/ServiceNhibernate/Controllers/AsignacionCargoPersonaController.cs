﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class AsignacionCargoPersonaController : ApiController
    {
        [Route("api/AsignacionCargoPersona/GetAsignacionCargoPersona/")]
        [HttpGet]
        public List<AsignacionCargoPersona> GetAsignacionCargoPersona()
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionCargoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionCargoPersonaRepository repository = new AsignacionCargoPersonaRepository();

            List<AsignacionCargoPersona> respuesta = repository.GetAll();


            return respuesta;
        }


        [Route("api/PZRASPE/GetPZRASPEId/{idper}")]
        [HttpGet]
        public List<AsignacionCargoPersona> GetPZRASPEId(int idper)
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionCargoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionCargoPersonaRepository repository = new AsignacionCargoPersonaRepository();

            List<AsignacionCargoPersona> respuesta = repository.GetPZRASPEId(idper);


            return respuesta;
        }

    }
}
