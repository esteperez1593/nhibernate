﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ServiceNhibernate.Controllers
{
    public class AsignacionInstrumentoPersonaController : ApiController
    {

        /// <summary>
        /// 
        /// Devuelve dado el PIDM DEL EVALUADOR 
        /// los evaluados con insrtrumentos pendientes
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/AsignacionInstrumentoPersona/GetPZRPLAPEvaPen/{id}")]
        [HttpGet]
        public List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPen(String id)
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionInstrumentoPersonaRepository repository = new AsignacionInstrumentoPersonaRepository();

            List<AsignacionInstrumentoPersona> respuesta = repository.GetPZRPLAPEvaPen(id);


            return respuesta;
        }


        [HttpGet]
        [ResponseType(typeof(List<Domain.AsignacionInstrumentoPersona>))]
        [Route("api/AsignacionInstrumentoPersona/GetPZRPLAPEvaPenDep/{id}")]
        public List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPenDep(String id) {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionInstrumentoPersonaRepository repository = new AsignacionInstrumentoPersonaRepository();

            List<AsignacionInstrumentoPersona> respuesta = repository.GetPZRPLAPEvaPenDep(id);


            return respuesta;
        }


        [HttpGet]
        [ResponseType(typeof(List<Domain.AsignacionInstrumentoPersona>))]
        [Route("api/AsignacionInstrumentoPersona/GetAll/")]
        public List<AsignacionInstrumentoPersona> GetAll()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPersona).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionInstrumentoPersonaRepository repository = new AsignacionInstrumentoPersonaRepository();

            List<AsignacionInstrumentoPersona> respuesta = repository.GetAll();


            return respuesta;
        }


    }
}
