﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class AsignacionInstrumentoPreguntaController : ApiController
    {

        [HttpGet]
        [Route("api/AsignacionInstrumentoPregunta/GetAll/")]
        public List<AsignacionInstrumentoPregunta> GetAll()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPregunta).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IAsignacionInstrumentoPreguntaRepository repository = new InstrumentoPreguntaRepository();

            List<AsignacionInstrumentoPregunta> respuesta = repository.GetAll();


            return respuesta;
        }

    }
}
