﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class CalendarioController : ApiController
    {
        [Route("api/PZBCALR/GetPZBCALR/")]
        [HttpGet]
        public List<Calendario> GetPZBCALR()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Calendario).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            ICalendarioRepository repository = new CalendarioRepository();

            List<Calendario> respuesta = repository.GetPZBCALR();


            return respuesta;
        }

    }
}
