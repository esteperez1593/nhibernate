﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class CriterioController : ApiController
    {

        [HttpGet]
        [Route("api/PZBCRIT/GetPZBCRIT")]
        public List<Criterio> GetPZBCRIT()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Criterio).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            ICriterioRepository repository = new CriterioRepository();

            List<Criterio> respuesta = repository.GetPZBCRIT();


            return respuesta;
        }


    }
}
