﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class DependeciaController : ApiController
    {

        [Route("api/Dependencia/GetDependencia/")]
        [HttpGet]
        public Dependencia GetDependencia()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Dependencia).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IDependenciaRepository repository = new DependenciaRepository();

            Dependencia respuesta = repository.Add();


            return respuesta;
        }

    }
}
