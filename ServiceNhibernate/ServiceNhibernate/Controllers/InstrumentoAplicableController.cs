﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class InstrumentoAplicableController : ApiController
    {

        [Route("api/PZBAPPL/GetPZBAPPL/")]
        [HttpGet]
        public List<InstrumentoAplicable> GetPZBAPPL()
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(InstrumentoAplicable).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IInstrumentoAplicableRepository repository = new InstrumentoAplicableRepository();

            List<InstrumentoAplicable> respuesta = repository.GetPZBAPPL();


            return respuesta;
        }

        [Route("api/PZBAPPL/GetPZBAPPL2/")]
        [HttpGet]
        public List<InstrumentoAplicable> GetPZBAPPL2()
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(InstrumentoAplicable).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IInstrumentoAplicableRepository repository = new InstrumentoAplicableRepository();

            List<InstrumentoAplicable> respuesta = repository.GetPZBAPPL2();


            return respuesta;
        }

    }
}
