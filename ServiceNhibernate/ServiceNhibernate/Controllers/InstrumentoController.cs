﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class InstrumentoController : ApiController
    {

        [Route("api/PZBPOLL/GetPZBPOLL/")]
        [HttpGet]
        public List<Instrumento> GetPZBPOLL()
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(InstrumentoAplicable).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IInstrumentoRepository repository = new InstrumentoRepository();

            List<Instrumento> respuesta = repository.GetPZBPOLL();


            return respuesta;
        }

        [Route("api/Instrumento/GetPZBPOLLP/{_pidm}/{_status}")]
        [HttpGet]
        public List<InstrumentoAplicable> GetPZBPOLLP(int _pidm, string _status)
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(InstrumentoAplicable).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IInstrumentoRepository repository = new InstrumentoRepository();

            List<InstrumentoAplicable> respuesta = repository.GetPZBPOLLP(_pidm,_status);


            return respuesta;
        }



    }
}
