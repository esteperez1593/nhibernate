﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class PerfilController : ApiController
    {

        [Route("api/PZVPFLE/GetPZVPFLE")]
        [HttpGet]
        public List<Perfil> GetPZVPFLE()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Perfil).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IPerfilRepository repository = new PerfilRepository();


            List<Perfil> respuesta = repository.GetPZVPFLE();


            return respuesta;
        }
    }
}
