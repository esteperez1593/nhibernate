﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class PermisoController : ApiController
    {

        [Route("api/PZVPERM/GetPZVPERM/{pidm}")]
        [HttpGet]
        public List<DefinicionPermisos> GetPZVPERM(int pidm)
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(DefinicionPermisos).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IPermisoRepository repository = new PermisoRepository();

            List<DefinicionPermisos> respuesta = repository.GetPZVPERM(pidm);


            return respuesta;
        }
    }
}
