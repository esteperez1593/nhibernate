﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class PreguntaController : ApiController
    {

        [HttpGet]
        [Route("api/PZBQSTN/GetPZBQSTN/")]
        public List<Pregunta> GetPZBQSTN()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Pregunta).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IPreguntaRepository repository = new PreguntaRepository();

            List<Pregunta> respuesta = repository.GetPZBQSTN();


            return respuesta;
        }

    }
}
