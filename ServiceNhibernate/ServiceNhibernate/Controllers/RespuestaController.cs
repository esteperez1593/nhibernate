﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class RespuestaController : ApiController
    {

        [HttpGet]
        [Route("api/PZRAWAP/GetPZRAWAP/{id}/{idins}/{idcal}/{_plap}")]
        public List<Domain.Respuesta> GetPZRAWAP(int id, string idins, string idcal, string _plap)
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Respuesta).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IRespuestaRepository repository = new RespuestaRepository();

            List<Respuesta> respuesta = repository.GetPZRAWAP(id,idins,idcal,_plap);


            return respuesta;
        }
    }
}
