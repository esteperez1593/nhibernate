﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace ServiceNhibernate.Controllers
{
    public class ResultadoAplicableController : ApiController
    {
      

            [Route("api/ResultadoAplicable/GetPZBAPRS/")]
            [HttpGet]
            [ResponseType(typeof(List<Domain.ResultadosParciales>))]
            public List<Domain.ResultadosParciales> GetPZBAPRS()
            {
                var cfg = new NHibernate.Cfg.Configuration();
                cfg.Configure();
                cfg.AddAssembly(typeof(Domain.ResultadoAplicable).Assembly);
                SchemaExport se = new SchemaExport(cfg);

                IResultadoAplicableRepository repository = new ResultadoAplicableRepository();

                List<ResultadosParciales> respuesta = repository.GetPZBAPRS("7", "EVCOGE",
                                                                            "CACP19", "272588",
                                                                            "GESPRO", "C",
                                                                            138, "E");


                return respuesta;
            }

        }
    }
