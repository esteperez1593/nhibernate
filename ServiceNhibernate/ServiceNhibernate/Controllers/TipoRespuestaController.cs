﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class TipoRespuestaController : ApiController
    {

        [HttpGet]
        [Route("api/PZVTYAW/GetPZVTYAWs/")]
        public List<TipoRespuesta> GetPZVTYAWs()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(TipoRespuesta).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            ITipoRespuestaRepository repository = new TipoRespuestaRepository();

            List<TipoRespuesta> respuesta = repository.GetPZVTYAWs();


            return respuesta;
        }

    }
}
