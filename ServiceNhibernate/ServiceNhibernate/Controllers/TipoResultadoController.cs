﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class TipoResultadoController : ApiController
    {

        [Route("api/PZVTYRS/GetPZVTYRS/")]
        [HttpGet]
        public List<TipoResultado> GetPZBCALR()
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(TipoResultado).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            ITipoResultadoRepository repository = new TipoResultadoRepository();

            List<TipoResultado> respuesta = repository.GetPZVTYRS();


            return respuesta;
        }

    }
}
