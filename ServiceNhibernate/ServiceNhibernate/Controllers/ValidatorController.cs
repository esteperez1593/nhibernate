﻿using NHibernate.Tool.hbm2ddl;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using ServiceNhibernate.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiceNhibernate.Controllers
{
    public class ValidatorController : ApiController
    {

        [HttpGet]
        [Route("api/VALIDATOR/validarComponentesInstrumentos/{parametro}")]
        public String validarComponentesInstrumentos(String parametro)
        {
            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPregunta).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IValidatorRepository repository = new ValidatorRepository();


            String respuesta = repository.validarComponentesInstrumentos(parametro);


            return respuesta;
        }

        [HttpGet]
        [Route("api/VALIDATOR/validarStatusPoll/{_pidm}/{_poll}/{_calr}/{id}")]
        public String validarStatusPoll(string _pidm, string _poll, string _calr, int id)
        {

            var cfg = new NHibernate.Cfg.Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(AsignacionInstrumentoPersonaController).Assembly);
            SchemaExport se = new SchemaExport(cfg);
            //      se.Execute(false, true, false);
            IValidatorRepository repository = new ValidatorRepository();


            String respuesta = repository.validarStatusPoll(_pidm,_poll,_calr,id);


            return respuesta;

        }

    }
}
