﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Area
    {
        public Area()
        {
            this.PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public virtual string PZBAREA_CODE { get; set; }
        public virtual string PZBAREA_DESCRIPTION { get; set; }
        public virtual string PZBAREA_USER { get; set; }
        public virtual System.DateTime PZBAREA_ACTIVITY_DATE { get; set; }
        public virtual string PZBAREA_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionInstrumentoPregunta> PZBPLQS { get; set; }
        
     //   public virtual Area PZBAREA { get; set; }
    }
}
