﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceNhibernate.Domain
{
   
    public class AsignacionCargoDependencia
    {
        public AsignacionCargoDependencia()
        {
            this.PZRASPE = new HashSet<AsignacionCargoPersona>();
        }

        public AsignacionCargoDependencia(Dependencia pZVDNCY)
        {
            PZVDNCY = pZVDNCY;
        }

        public AsignacionCargoDependencia(Cargo pZVPOST)
        {
            PZVPOST = pZVPOST;
        }

        public AsignacionCargoDependencia(Dependencia pZVDNCY, Cargo pZVPOST)
        {
            PZVDNCY = pZVDNCY;
            PZVPOST = pZVPOST;
        }


        public AsignacionCargoDependencia(string pZRDYPT_POST_CODE, string pZRDYPT_DNCY_CODE)
        {
            PZRDYPT_POST_CODE = pZRDYPT_POST_CODE;
            PZRDYPT_DNCY_CODE = pZRDYPT_DNCY_CODE;
        }

        public virtual string PZRDYPT_POST_CODE { get; set; }
        public virtual string PZRDYPT_DNCY_CODE { get; set; }
        public virtual string PZRDYPT_USER { get; set; }
        public virtual System.DateTime PZRDYPT_ACTIVITY_DATE { get; set; }
        public virtual string PZRDYPT_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionCargoPersona> PZRASPE { get; set; }
        public virtual Dependencia PZVDNCY { get; set; }
        public virtual Cargo PZVPOST { get; set; }

      //  public virtual AsignacionCargoDependencia PZRDYPT { get; set; }

        public override bool Equals(object obj)
            {
                var other = obj as AsignacionCargoDependencia;

                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;

                return this.PZRDYPT_POST_CODE == other.PZRDYPT_POST_CODE &&
                       this.PZRDYPT_DNCY_CODE == other.PZRDYPT_DNCY_CODE;
            }

           public override int GetHashCode()
            {
                unchecked
                {
                    int hash = GetType().GetHashCode();
                    hash = (hash * 31) ^ PZVPOST.PZVPOST_CODE.GetHashCode();
                    hash = (hash * 31) ^ PZVDNCY.PZVDNCY_CODE.GetHashCode();
                    
                    

                    return hash;
                }
            }

    }
 
}