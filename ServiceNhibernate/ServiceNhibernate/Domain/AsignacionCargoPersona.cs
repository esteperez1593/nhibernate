﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceNhibernate.Domain
{
    
    public class AsignacionCargoPersona
    {
        public AsignacionCargoPersona()
        {
            this.PZRPLAP = new HashSet<AsignacionInstrumentoPersona>();
            this.PZVPERM = new HashSet<Permiso>();
        }

        public AsignacionCargoPersona(string pZRASPE_POST_CODE, string pZRASPE_DNCY_CODE, string pZRASPE_PFLE_CODE, int pZRASPE_PIDM)
        {
            PZRASPE_POST_CODE = pZRASPE_POST_CODE;
            PZRASPE_DNCY_CODE = pZRASPE_DNCY_CODE;
            PZRASPE_PFLE_CODE = pZRASPE_PFLE_CODE;
            PZRASPE_PIDM = pZRASPE_PIDM;
        }

        public AsignacionCargoPersona(string pZRASPE_POST_CODE, string pZRASPE_DNCY_CODE)
        {
            PZRASPE_POST_CODE = pZRASPE_POST_CODE;
            PZRASPE_DNCY_CODE = pZRASPE_DNCY_CODE;
        }



        public virtual string PZRASPE_POST_CODE { get; set; }
        public virtual string PZRASPE_DNCY_CODE { get; set; }
        public virtual string PZRASPE_PFLE_CODE { get; set; }
        public virtual int PZRASPE_PIDM { get; set; }
        public virtual System.DateTime PZRASPE_START_DATE { get; set; }
        public virtual Nullable<System.DateTime> PZRASPE_END_DATE { get; set; }
        public virtual string PZRASPE_USER { get; set; }
        public virtual System.DateTime PZRASPE_ACTIVITY_DATE { get; set; }
        public virtual string PZRASPE_DATA_ORIGIN { get; set; }

        public virtual Persona PZBPRSO { get; set; }
        public virtual ICollection<AsignacionInstrumentoPersona> PZRPLAP { get; set; }
        public virtual ICollection<Permiso> PZVPERM { get; set; }
        public virtual AsignacionCargoDependencia PZRDYPT { get; set; }
        public virtual Perfil PZVPFLE { get; set; }




        public override bool Equals(object obj)
        {
            var other = obj as AsignacionCargoPersona;

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return this.PZRASPE_POST_CODE == other.PZRASPE_POST_CODE &&
                   this.PZRASPE_DNCY_CODE == other.PZRASPE_DNCY_CODE &&
                   this.PZRASPE_PFLE_CODE == other.PZRASPE_PFLE_CODE &&
                   this.PZRASPE_PIDM == other.PZRASPE_PIDM;
        }

        public override int GetHashCode()
        {
            unchecked
            {
          
                int hash = GetType().GetHashCode();
                //  hash = (hash * 31) ^ (PZRDYPT.PZVPOST.PZVPOST_CODE + "," + PZRDYPT.PZVDNCY.PZVDNCY_CODE).GetHashCode();
              //  hash = (hash * 31) ^ PZRDYPT.GetHashCode();
                hash = (hash * 31) ^ PZVPFLE.PZVPFLE_CODE.GetHashCode();
                hash = (hash * 31) ^ PZBPRSO.PZBPRSO_PIDM.GetHashCode();
                return hash;
            }

        }
    }

    }

