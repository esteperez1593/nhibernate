﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class AsignacionInstrumentoPersona
    {
        public AsignacionInstrumentoPersona()
        {
            this.PZRAWAP = new HashSet<Respuesta>();
            this.PZRRSPL = new HashSet<ResultadosParciales>();
            this.PZRRSTT = new HashSet<ResultadosTotales>();
        }

        public virtual int PZRPLAP_SEQ_NUMBER { get; set; }
        public virtual int PZRPLAP_APPL_SEQ_NUMBER { get; set; }
        public virtual string PZRPLAP_POLL_CODE { get; set; }
        public virtual string PZRPLAP_CALR_CODE { get; set; }
        public virtual int PZRPLAP_PIDM { get; set; }
        public virtual string PZRPLAP_DNCY_CODE { get; set; }
        public virtual string PZRPLAP_POST_CODE { get; set; }
        public virtual string PZRPLAP_PFLE_CODE { get; set; }
        public virtual string PZRPLAP_STATUS { get; set; }
        public virtual System.DateTime PZRPLAP_DATE { get; set; }
        public virtual string PZRPLAP_USER { get; set; }
        public virtual System.DateTime PZRPLAP_ACTIVITY_DATE { get; set; }
        public virtual string PZRPLAP_DATA_ORIGIN { get; set; }

        public virtual InstrumentoAplicable PZBAPPL { get; set; }
        public virtual AsignacionCargoPersona PZRASPE { get; set; }
        public virtual ICollection<Respuesta> PZRAWAP { get; set; }
        public virtual ICollection<ResultadosParciales> PZRRSPL { get; set; }
        public virtual ICollection<ResultadosTotales> PZRRSTT { get; set; }

 //       public virtual AsignacionInstrumentoPersona PZRPLAP { get; set; }
    }
} 