﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class AsignacionInstrumentoPregunta
    {
        public AsignacionInstrumentoPregunta()
        {
            this.PZRAWAP = new HashSet<Respuesta>();
        }

        public virtual int PZBPLQS_SEQ_NUMBER { get; set; }
        public virtual int PZBPLQS_APPL_SEQ_NUMBER { get; set; }
        public virtual string PZBPLQS_POLL_CODE { get; set; }
        public virtual string PZBPLQS_CALR_CODE { get; set; }
        public virtual string PZBPLQS_QSTN_CODE { get; set; }
        public virtual string PZBPLQS_AREA_CODE { get; set; }
        public virtual string PZBPLQS_TYAW_CODE { get; set; }
        public virtual string PZBPLQS_CRIT_CODE { get; set; }
        public virtual Nullable<short> PZBPLQS_MIN_VALUE { get; set; }
        public virtual Nullable<short> PZBPLQS_MAX_VALUE { get; set; }
        public virtual Nullable<short> PZBPLQS_MIN_OVAL { get; set; }
        public virtual Nullable<short> PZBPLQS_MAX_OVAL { get; set; }
        public virtual short PZBPLQS_ORDER { get; set; }
        public virtual string PZBPLQS_STATUS { get; set; }
        public virtual string PZBPLQS_USER { get; set; }
        public virtual System.DateTime PZBPLQS_ACTIVITY_DATE { get; set; }
        public virtual string PZBPLQS_DATA_ORIGIN { get; set; }

        public virtual InstrumentoAplicable PZBAPPL { get; set; }
        public virtual Area PZBAREA { get; set; }
        public virtual Criterio PZBCRIT { get; set; }
        public virtual ICollection<Respuesta> PZRAWAP { get; set; }
        public virtual Pregunta PZBQSTN { get; set; }
        public virtual TipoRespuesta PZVTYAW { get; set; }
    }
}