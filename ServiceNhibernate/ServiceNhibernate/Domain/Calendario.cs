﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Calendario
    {
        public Calendario()
        {
            this.PZBAPPL = new HashSet<InstrumentoAplicable>();
            this.PZBPHSE = new HashSet<Fase>();
        }

        public virtual string PZBCALR_CODE { get; set; }
        public virtual string PZBCALR_NAME { get; set; }
        public virtual int PZBCALR_SRL { get; set; }
        public virtual System.DateTime PZBCALR_INIT_DATE { get; set; }
        public virtual System.DateTime PZBCALR_END_DATE { get; set; }
        public virtual string PZBCALR_TERM { get; set; }
        public virtual string PZBCALR_USER { get; set; }
        public virtual System.DateTime PZBCALR_ACTIVITY_DATE { get; set; }
        public virtual string PZBCALR_DATA_ORIGIN { get; set; }

        public virtual ICollection<InstrumentoAplicable> PZBAPPL { get; set; }
        public virtual ICollection<Fase> PZBPHSE { get; set; }
    }
}