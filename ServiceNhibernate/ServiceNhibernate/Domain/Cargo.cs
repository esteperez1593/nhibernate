﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Cargo
    {
        public Cargo()
        {
            this.PZRDYPT = new HashSet<AsignacionCargoDependencia>();
            this.PZVPOST1 = new HashSet<Cargo>();
        }

        public virtual string PZVPOST_CODE { get; set; }
        public virtual string PZVPOST_NAME { get; set; }
        public virtual string PZVPOST_DESCRIPTION { get; set; }
        public virtual string PZVPOST_CODE_SUP { get; set; }
        public virtual string PZVPOST_USER { get; set; }
        public virtual System.DateTime PZVPOST_ACTIVITY_DATE { get; set; }
        public virtual string PZVPOST_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionCargoDependencia> PZRDYPT { get; set; }
        public virtual ICollection<Cargo> PZVPOST1 { get; set; }
        public virtual Cargo PZVPOST2 { get; set; }
    }
}
