﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Criterio
    {

        public Criterio()
        {
            this.PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public virtual string PZBCRIT_CODE { get; set; }
        public virtual string PZBCRIT_DESCRIPTION { get; set; }
        public virtual string PZBCRIT_USER { get; set; }
        public virtual System.DateTime PZBCRIT_ACTIVITY_DATE { get; set; }
        public virtual string PZBCRIT_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionInstrumentoPregunta> PZBPLQS { get; set; }


    }
}