﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class DefinicionPermisos
    {

        public DefinicionPermisos()
        {
            this.PZVPERM = new HashSet<Permiso>();
        }

        public virtual string PZVRGHT_CODE { get; set; }
        public virtual string PZVRGHT_DESCRIPTION { get; set; }
        public virtual int PZVRGHT_GRADE { get; set; }
        public virtual string PZVRGHT_USER { get; set; }
        public virtual System.DateTime PZVRGHT_ACTIVITY_DATE { get; set; }
        public virtual string PZVRGHT_DATA_ORIGIN { get; set; }

        public virtual ICollection<Permiso> PZVPERM { get; set; }

   

    }
}