﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ServiceNhibernate.Domain
{

    
    public class Dependencia
    {

        public Dependencia()
        {
            this.PZRDYPT = new HashSet<AsignacionCargoDependencia>();
            this.PZVDNCY1 = new HashSet<Dependencia>();
        }

        public virtual string PZVDNCY_CODE { get; set; }
        public virtual string PZVDNCY_NAME { get; set; }
        public virtual string PZVDNCY_CODE_SUP { get; set; }
        public virtual string PZVDNCY_SITE { get; set; }
        public virtual int PZVDNCY_LEVEL { get; set; }
        public virtual string PZVDNCY_KEY { get; set; }
        public virtual string PZVDNCY_USER { get; set; }
        public virtual System.DateTime PZVDNCY_ACTIVITY_DATE { get; set; }
        public virtual string PZVDNCY_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionCargoDependencia> PZRDYPT { get; set; }
        public virtual ICollection<Dependencia> PZVDNCY1 { get; set; }
        public virtual Dependencia PZVDNCY2 { get; set; }



    }
}