﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Fase
    {
        public Fase()
        {
            this.PZRMSSG = new HashSet<Mensaje>();
        }

        public virtual string PZBPHSE_CODE { get; set; }
        public virtual string PZBPHSE_NAME { get; set; }
        public virtual string PZBPHSE_CALR_CODE { get; set; }
        public virtual System.DateTime PZBPHSE_OPEN_DATE { get; set; }
        public virtual System.DateTime PZBPHSE_CLOSE_DATE { get; set; }
        public virtual Nullable<System.DateTime> PZBPHSE_OPEXTENSION_DATE { get; set; }
        public virtual Nullable<System.DateTime> PZBPHSE_CLEXTENSION_DATE { get; set; }
        public virtual string PZBPHSE_USER { get; set; }
        public virtual System.DateTime PZBPHSE_ACTIVITY_DATE { get; set; }
        public virtual string PZBPHSE_DATA_ORIGIN { get; set; }

        public virtual Calendario PZBCALR { get; set; }
        public virtual ICollection<Mensaje> PZRMSSG { get; set; }

    }
}