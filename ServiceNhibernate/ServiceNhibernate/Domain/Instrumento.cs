﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Instrumento
    {
        public Instrumento()
        {
            this.PZBAPPL = new HashSet<InstrumentoAplicable>();
        }

        public Instrumento(string pZBPOLL_CODE, string pZBPOLL_NAME, string pZBPOLL_USER, DateTime pZBPOLL_ACTIVITY_DATE, string pZBPOLL_DATA_ORIGIN)
        {
            PZBPOLL_CODE = pZBPOLL_CODE;
            PZBPOLL_NAME = pZBPOLL_NAME;
            PZBPOLL_USER = pZBPOLL_USER;
            PZBPOLL_ACTIVITY_DATE = pZBPOLL_ACTIVITY_DATE;
            PZBPOLL_DATA_ORIGIN = pZBPOLL_DATA_ORIGIN;
        }

        public Instrumento(string pZBPOLL_CODE, string pZBPOLL_NAME, string pZBPOLL_USER, DateTime pZBPOLL_ACTIVITY_DATE, string pZBPOLL_DATA_ORIGIN, ICollection<InstrumentoAplicable> pZBAPPL)
        {
            PZBPOLL_CODE = pZBPOLL_CODE;
            PZBPOLL_NAME = pZBPOLL_NAME;
            PZBPOLL_USER = pZBPOLL_USER;
            PZBPOLL_ACTIVITY_DATE = pZBPOLL_ACTIVITY_DATE;
            PZBPOLL_DATA_ORIGIN = pZBPOLL_DATA_ORIGIN;
            PZBAPPL = pZBAPPL;
        }

        public virtual string PZBPOLL_CODE { get; set; }
        public virtual string PZBPOLL_NAME { get; set; }
        public virtual string PZBPOLL_USER { get; set; }
        public virtual  System.DateTime PZBPOLL_ACTIVITY_DATE { get; set; }
        public virtual string PZBPOLL_DATA_ORIGIN { get; set; }

        public virtual ICollection<InstrumentoAplicable> PZBAPPL { get; set; }

    }
}