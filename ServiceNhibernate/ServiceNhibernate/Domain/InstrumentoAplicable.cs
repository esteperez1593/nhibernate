﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class InstrumentoAplicable
    {

        public InstrumentoAplicable()
        {
            this.PZBAPRS = new HashSet<ResultadoAplicable>();
            this.PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
            this.PZRPLAP = new HashSet<AsignacionInstrumentoPersona>();
        }

        public InstrumentoAplicable(Instrumento pZBPOLL)
        {
            PZBPOLL = pZBPOLL;
        }

        public InstrumentoAplicable(Calendario pZBCALR)
        {
            PZBCALR = pZBCALR;
        }

        public InstrumentoAplicable(Calendario pZBCALR, Instrumento pZBPOLL)
        {
            PZBCALR = pZBCALR;
            PZBPOLL = pZBPOLL;
        }

        public InstrumentoAplicable(string pZBAPPL_POLL_CODE, string pZBAPPL_CALR_CODE)
        {
            PZBAPPL_POLL_CODE = pZBAPPL_POLL_CODE;
            PZBAPPL_CALR_CODE = pZBAPPL_CALR_CODE;
        }

        public virtual int PZBAPPL_SEQ_NUMBER { get; set; }
        public virtual string PZBAPPL_POLL_CODE { get; set; }
        public virtual string PZBAPPL_CALR_CODE { get; set; }
        public virtual string PZBAPPL_PFLE_CODE { get; set; }
        public virtual int PZBAPPL_PRCNT { get; set; }
        public virtual string PZBAPPL_USER { get; set; }
        public virtual System.DateTime PZBAPPL_ACTIVITY_DATE { get; set; }
        public virtual string PZBAPPL_DATA_ORIGIN { get; set; }

        public virtual ICollection<ResultadoAplicable> PZBAPRS { get; set; }
        public virtual ICollection<AsignacionInstrumentoPregunta> PZBPLQS { get; set; }
        public virtual ICollection<AsignacionInstrumentoPersona> PZRPLAP { get; set; }
        public virtual Calendario PZBCALR { get; set; }
        public virtual Instrumento PZBPOLL { get; set; }
          public virtual Perfil PZVPFLE { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as InstrumentoAplicable;

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return this.PZBAPPL_SEQ_NUMBER == other.PZBAPPL_SEQ_NUMBER &&
                   this.PZBAPPL_POLL_CODE == other.PZBAPPL_POLL_CODE &&
                   this.PZBAPPL_CALR_CODE == other.PZBAPPL_CALR_CODE;
                
        }


         public override int GetHashCode()
         {
             unchecked
             {
                 int hash = GetType().GetHashCode();
                 hash = (hash * 31) ^ PZBAPPL_SEQ_NUMBER.GetHashCode();
                 hash = (hash * 31) ^ PZBPOLL.PZBPOLL_CODE.GetHashCode();
                 hash = (hash * 31) ^ PZBCALR.PZBCALR_CODE.GetHashCode();
              //   hash = (hash * 31) ^ PZVPFLE.PZVPFLE_CODE.GetHashCode();

                return hash;
             }


         }

    }
}