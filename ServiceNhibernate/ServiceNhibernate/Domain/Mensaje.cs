﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Mensaje
    {


        public virtual int PZRMSSG_ID { get; set; }
        public virtual string PZRMSSG_DESCRIPTION { get; set; }
        public virtual string PZRMSSG_PHSE_CODE { get; set; }
        public virtual string PZRMSSG_USER { get; set; }
        public virtual System.DateTime PZRMSSG_ACTIVITY_DATE { get; set; }
        public virtual string PZRMSSG_DATA_ORIGIN { get; set; }

        public virtual Fase PZBPHSE { get; set; }


    }
}