﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Perfil
    {
        public Perfil()
        {
            this.PZBAPPL = new HashSet<InstrumentoAplicable>();
            this.PZRASPE = new HashSet<AsignacionCargoPersona>();
            this.PZVPERM = new HashSet<Permiso>();
        }

        public virtual string PZVPFLE_CODE { get; set; }
        public virtual string PZVPFLE_NAME { get; set; }
        public virtual string PZVPFLE_USER { get; set; }
        public virtual System.DateTime PZVPFLE_ACTIVITY_DATE { get; set; }
        public virtual string PZVPFLE_DATA_ORIGIN { get; set; }

        public virtual ICollection<InstrumentoAplicable> PZBAPPL { get; set; }
        public virtual ICollection<AsignacionCargoPersona> PZRASPE { get; set; }
        public virtual ICollection<Permiso> PZVPERM { get; set; }

        //public virtual Perfil PZVPFLE { get; set; }

      
        }
}