﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Permiso
    {


        public virtual string PZVPERM_CODE { get; set; }
        public virtual string PZVPERM_POST_CODE { get; set; }
        public virtual string PZVPERM_DNCY_CODE { get; set; }
        public virtual string PZVPERM_PFLE_CODE { get; set; }
        public virtual Nullable<int> PZVPERM_PIDM { get; set; }
        public virtual string PZVPERM_RGHT_CODE { get; set; }
        public virtual string PZVPERM_USER { get; set; }
        public virtual System.DateTime PZVPERM_ACTIVITY_DATE { get; set; }
        public virtual string PZVPERM_DATA_ORIGIN { get; set; }

        public virtual AsignacionCargoPersona PZRASPE { get; set; }
        public virtual Perfil PZVPFLE { get; set; }
        public virtual DefinicionPermisos PZVRGHT { get; set; }

    }
}