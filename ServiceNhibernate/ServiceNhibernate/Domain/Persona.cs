﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Persona
    {
        public Persona()
        {
            this.PZRASPE = new HashSet<AsignacionCargoPersona>();
        }

        public virtual int PZBPRSO_PIDM { get; set; }
        public virtual string PZBPRSO_PAYSHEET { get; set; }
        public virtual string PZBPRSO_ACTIVE { get; set; }
        public virtual string PZBPRSO_SITE { get; set; }
        public virtual string PZBPRSO_USER { get; set; }
        public virtual System.DateTime PZBPRSO_ACTIVITY_DATE { get; set; }
        public virtual string PZBPRSO_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionCargoPersona> PZRASPE { get; set; }



    }
}
