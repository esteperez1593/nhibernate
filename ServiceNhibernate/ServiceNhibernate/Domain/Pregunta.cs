﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Pregunta
    {
        public Pregunta()
        {
            this.PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public virtual string PZBQSTN_CODE { get; set; }
        public virtual string PZBQSTN_NAME { get; set; }
        public virtual string PZBQSTN_DESCRIPTION { get; set; }
        public virtual string PZBQSTN_USER { get; set; }
        public virtual System.DateTime PZBQSTN_ACTIVITY_DATE { get; set; }
        public virtual string PZBQSTN_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionInstrumentoPregunta> PZBPLQS { get; set; }

   
    }
}