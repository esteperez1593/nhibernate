﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class Respuesta
    {

        public virtual int PZRAWAP_SEQ_NUMBER { get; set; }
        public virtual int PZRAWAP_PLAP_SEQ_NUMBER { get; set; }
        public virtual int PZRAWAP_PLQS_SEQ_NUMBER { get; set; }
        public virtual string PZRAWAP_DESCRIPTION { get; set; }
        public virtual Nullable<short> PZRAWAP_PERC_IMP { get; set; }
        public virtual short PZRAWAP_VALUE { get; set; }
        public virtual string PZRAWAP_USER { get; set; }
        public virtual System.DateTime PZRAWAP_ACTIVITY_DATE { get; set; }
        public virtual string PZRAWAP_DATA_ORIGIN { get; set; }

        public virtual AsignacionInstrumentoPregunta PZBPLQS { get; set; }
        public virtual AsignacionInstrumentoPersona PZRPLAP { get; set; }

    }
}