﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class ResultadoAplicable
    {
        public ResultadoAplicable()
        {
            this.PZRRSPL = new HashSet<ResultadosParciales>();
        }
        public virtual int PZBAPRS_SEQ_NUMBER { get; set; }
        public virtual int PZBAPRS_APPL_SEQ_NUMBER { get; set; }
        public virtual string PZBAPRS_POLL_CODE { get; set; }
        public virtual string PZBAPRS_CALR_CODE { get; set; }
        public virtual string PZBAPRS_TYRS_CODE { get; set; }
        public virtual int PZBAPRS_ORDER { get; set; }
        public virtual string PZBAPRS_USER { get; set; }
        public virtual System.DateTime PZBAPRS_ACTIVITY_DATE { get; set; }
        public virtual string PZBAPRS_DATA_ORIGIN { get; set; }

        public virtual InstrumentoAplicable PZBAPPL { get; set; }
        public virtual ICollection<ResultadosParciales> PZRRSPL { get; set; }
        public virtual TipoResultado PZVTYRS { get; set; }


    }
}