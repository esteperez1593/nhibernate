﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class ResultadosParciales
    {

        public virtual int PZRRSPL_SEQ_NUMBER { get; set; }
        public virtual int PZRRSPL_PLAP_SEQ_NUMBER { get; set; }
        public virtual int PZRRSPL_APRS_SEQ_NUMBER { get; set; }
        public virtual Nullable<short> PZRRSPL_NUMB_VALUE { get; set; }
        public virtual string PZRRSPL_STR_VALUE { get; set; }
        public virtual string PZRRSPL_USER { get; set; }
        public virtual System.DateTime PZRRSPL_ACTIVITY_DATE { get; set; }
        public virtual string PZRRSPL_DATA_ORIGIN { get; set; }

        public virtual ResultadoAplicable PZBAPRS { get; set; }
        public virtual AsignacionInstrumentoPersona PZRPLAP { get; set; }
   


    }
}