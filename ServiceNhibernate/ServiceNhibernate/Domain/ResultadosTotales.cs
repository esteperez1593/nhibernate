﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class ResultadosTotales
    {

        public virtual int PZRRSTT_ID { get; set; }
        public virtual int PZRRSTT_PLAP_SEQ_NUMBER { get; set; }
        public virtual string PZRRSTT_DESCRIPTION { get; set; }
        public virtual Nullable<int> PZRRSTT_RESULT_VALUE { get; set; }
        public virtual string PZRRSTT_USER { get; set; }
        public virtual System.DateTime PZRRSTT_ACTIVITY_DATE { get; set; }
        public virtual string PZRRSTT_DATA_ORIGIN { get; set; }

        public virtual AsignacionInstrumentoPersona PZRPLAP { get; set; }
   

    }
}