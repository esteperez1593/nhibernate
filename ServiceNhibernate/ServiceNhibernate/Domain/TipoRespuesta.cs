﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class TipoRespuesta
    {

        public TipoRespuesta()
        {
            this.PZBPLQS = new HashSet<AsignacionInstrumentoPregunta>();
        }

        public virtual string PZVTYAW_CODE { get; set; }
        public virtual string PZVTYAW_DESCRIPTION { get; set; }
        public virtual string PZVTYAW_USER { get; set; }
        public virtual System.DateTime PZVTYAW_ACTIVITY_DATE { get; set; }
        public virtual string PZVTYAW_DATA_ORIGIN { get; set; }

        public virtual ICollection<AsignacionInstrumentoPregunta> PZBPLQS { get; set; }

    }
}