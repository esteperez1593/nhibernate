﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{
    public class TipoResultado
    {

        public TipoResultado()
        {
            this.PZBAPRS = new HashSet<ResultadoAplicable>();
        }
 

        public virtual string PZVTYRS_CODE { get; set; }
        public virtual string PZVTYRS_DESCRIPTION { get; set; }
        public virtual string PZVTYRS_USER { get; set; }
        public virtual System.DateTime PZVTYRS_ACTIVITY_DATE { get; set; }
        public virtual string PZVTYRS_DATA_ORIGIN { get; set; }

        public virtual ICollection<ResultadoAplicable> PZBAPRS { get; set; }
     
    }
}