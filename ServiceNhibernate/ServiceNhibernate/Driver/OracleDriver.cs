﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess;
using NHibernate.Driver;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace ServiceNhibernate.Driver
{
    public class OracleDriver : DriverBase
    {
        public override IDbCommand CreateCommand()
        {
            return new OracleCommand();
        }
        public override IDbConnection CreateConnection()
        {
            return new OracleConnection();
        }
        public override string NamedPrefix
        {
            get { return string.Empty; }
        }
        public override bool UseNamedPrefixInParameter
        {
            get { return false; }
        }
        public override bool UseNamedPrefixInSql
        {
            get { return false; }
        }
    }
}