﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAreaRepository
    {
        List<Area> GetPZBAREA();
        Area Add(Area _area);
        void Update(Area _area);
        void Remove(Area _area);
        Area GetById(decimal id);
        Area GetByName(String name);
    }
}