﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionCargoDependenciaRepository
    {
        List<AsignacionCargoDependencia> GetAll();
        AsignacionCargoDependencia Add(AsignacionCargoDependencia calendario);
        void Update(AsignacionCargoDependencia calendario);
        void Remove(AsignacionCargoDependencia calendario);
        AsignacionCargoDependencia GetById(decimal id);
        AsignacionCargoDependencia GetByName(String name);
        List<AsignacionCargoPersona> GetEvaluados(String idcar, String iddep);

    }
}