﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionCargoPersonaRepository
    {
        AsignacionCargoPersona Add(AsignacionCargoPersona parametro);
        void Update(AsignacionCargoPersona parametro);
        void Remove(AsignacionCargoPersona parametro);
        AsignacionCargoPersona GetById(decimal parametro);
        AsignacionCargoPersona GetByName(String parametro);

        List<AsignacionCargoPersona> GetAll();

        List<AsignacionCargoPersona> GetPZRASPEId(int idper);

    }
}