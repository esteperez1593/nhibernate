﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionInstrumentoPersonaRepository
    {
        AsignacionInstrumentoPersona Add(AsignacionInstrumentoPersona cargo);

        List<AsignacionInstrumentoPersona> GetAll();
        List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPen(String id);
        List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPenDep(String id);
        List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String id);
        List<AsignacionInstrumentoPersona> GetPZRPLAPIdC(String id, String idi);
        List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String ide, String idi);
        void Update(AsignacionInstrumentoPersona cargo);
        void Remove(AsignacionInstrumentoPersona cargo);
        AsignacionInstrumentoPersona GetById(decimal cargo);
        AsignacionInstrumentoPersona GetByName(String cargo);

    }
}