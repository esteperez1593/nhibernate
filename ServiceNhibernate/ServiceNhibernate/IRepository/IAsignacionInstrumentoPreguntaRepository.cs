﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IAsignacionInstrumentoPreguntaRepository
    {
        List<AsignacionInstrumentoPregunta> GetAll();
        AsignacionInstrumentoPregunta Add(AsignacionInstrumentoPersona cargo);
        void Update(AsignacionInstrumentoPersona cargo);
        void Remove(AsignacionInstrumentoPersona cargo);
        AsignacionInstrumentoPersona GetById(decimal cargo);
        AsignacionInstrumentoPersona GetByName(String cargo);

    }
}