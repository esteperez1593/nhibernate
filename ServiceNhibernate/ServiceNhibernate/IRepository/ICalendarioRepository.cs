﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ICalendarioRepository
    {
        List<Calendario> GetPZBCALR();
        Calendario Add(Calendario calendario);
        void Update(Calendario calendario);
        void Remove(Calendario calendario);
        Calendario GetById(decimal id);
        Calendario GetByName(String name);

    }
}