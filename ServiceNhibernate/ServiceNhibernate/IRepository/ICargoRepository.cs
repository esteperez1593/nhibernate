﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Domain
{

    interface ICargoRepository
    {
        Cargo Add(Cargo cargo);
        void Update(Cargo cargo);
        void Remove(Cargo cargo);
        Cargo GetById(decimal cargo);
        Cargo GetByName(String cargo);

    }
}
