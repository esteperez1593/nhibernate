﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ICriterioRepository
    {
        List<Criterio> GetPZBCRIT();
        Criterio Add(Criterio cargo);
        void Update(Criterio cargo);
        void Remove(Criterio cargo);
        Criterio GetById(decimal cargo);
        Criterio GetByName(String cargo);

    }
}