﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IDefinicionPermisosRepository
    {
        DefinicionPermisos Add(DefinicionPermisos cargo);
        void Update(DefinicionPermisos cargo);
        void Remove(DefinicionPermisos cargo);
        DefinicionPermisos GetById(decimal cargo);
        DefinicionPermisos GetByName(String cargo);

    }
}