﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IDependenciaRepository
    {
        Dependencia Add();
        void Update(Dependencia cargo);
        void Remove(Dependencia cargo);
        Dependencia GetById(decimal cargo);
        Dependencia GetByName(String cargo);

    }
}