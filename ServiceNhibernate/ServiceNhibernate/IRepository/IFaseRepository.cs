﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IFaseRepository
    {
        Fase Add(Fase cargo);
        void Update(Fase cargo);
        void Remove(Fase cargo);
        Fase GetById(decimal cargo);
        Fase GetByName(String cargo);

    }
}