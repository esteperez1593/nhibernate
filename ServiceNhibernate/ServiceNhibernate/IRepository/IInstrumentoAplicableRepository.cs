﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IInstrumentoAplicableRepository
    {
        List<InstrumentoAplicable> GetPZBAPPL();
        InstrumentoAplicable Add(InstrumentoAplicable _instrumento);
        void Update(InstrumentoAplicable _instrumento);
        void Remove(InstrumentoAplicable _instrumento);
        InstrumentoAplicable GetById(decimal id);
        InstrumentoAplicable GetByName(String name);

        List<InstrumentoAplicable> GetPZBAPPL2();
    }
}