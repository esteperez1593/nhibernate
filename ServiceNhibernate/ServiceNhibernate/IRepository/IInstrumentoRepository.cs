﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IInstrumentoRepository
    {
        List<InstrumentoAplicable> GetPZBPOLLP(int pidm, String status);
        List<Instrumento> GetPZBPOLL();
        Instrumento Add(Instrumento parametro);
        void Update(Instrumento parametro);
        void Remove(Instrumento parametro);
        Instrumento GetById(decimal parametro);
        Instrumento GetByName(String parametro);

    }
}