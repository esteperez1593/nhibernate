﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IMensajeRepository
    {
        Mensaje Add(Mensaje parametro);
        void Update(Mensaje parametro);
        void Remove(Mensaje parametro);
        Mensaje GetById(decimal parametro);
        Mensaje GetByName(String parametro);

    }
}