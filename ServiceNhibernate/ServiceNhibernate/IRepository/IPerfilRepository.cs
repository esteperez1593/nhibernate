﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPerfilRepository
    {
        List<Perfil> GetPZVPFLE();
        Perfil Add(Perfil _area);
        void Update(Perfil _area);
        void Remove(Perfil _area);
        Perfil GetById(decimal id);
        Perfil GetByName(String name);
    }
}