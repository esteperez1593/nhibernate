﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPermisoRepository
    {

        List<Domain.DefinicionPermisos> GetPZVPERM(int pidm);
    }
}