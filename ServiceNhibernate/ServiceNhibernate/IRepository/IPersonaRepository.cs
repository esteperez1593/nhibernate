﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPersonaRepository
    {
        Persona Add(Persona parametro);
        void Update(Persona parametro);
        void Remove(Persona parametro);
        Persona GetById(decimal parametro);
        Persona GetByName(String parametro);

    }
}