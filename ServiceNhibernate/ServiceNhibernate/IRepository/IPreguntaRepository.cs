﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IPreguntaRepository
    {
        List<Domain.Pregunta> GetPZBQSTN();
        Pregunta Add(Pregunta parametro);
        void Update(Pregunta parametro);
        void Remove(Pregunta parametro);
        Pregunta GetById(decimal parametro);
        Pregunta GetByName(String parametro);

    }
}