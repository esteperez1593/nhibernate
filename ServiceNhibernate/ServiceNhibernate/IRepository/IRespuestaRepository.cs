﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IRespuestaRepository
    {
        List<Domain.Respuesta> GetPZRAWAP(int id, string idins, string idcal, string _plap);
        Respuesta Add(Respuesta parametro);
        void Update(Respuesta parametro);
        void Remove(Respuesta parametro);
        Respuesta GetById(decimal parametro);
        Respuesta GetByName(String parametro);

    }
}