﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadoAplicableRepository
    {
         List<ResultadosParciales> GetPZBAPRS(string _appl, string _poll,
                                                  string _calr, string _pidm,
                                                  string _perfil, string _status,
                                                  int _plap, string _type);

        ResultadoAplicable Add(ResultadoAplicable parametro);
        void Update(ResultadoAplicable parametro);
        void Remove(ResultadoAplicable parametro);
        ResultadoAplicable GetById(decimal parametro);
        ResultadoAplicable GetByName(String parametro);
    }
}

