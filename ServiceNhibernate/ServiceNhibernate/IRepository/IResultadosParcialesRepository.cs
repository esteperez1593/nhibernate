﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadosParcialesRepository
    {

        ResultadosParciales Add(ResultadosParciales parametro);
        void Update(ResultadosParciales parametro);
        void Remove(ResultadosParciales parametro);
        ResultadosParciales GetById(decimal parametro);
        ResultadosParciales GetByName(String parametro);

    }
}