﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IResultadosTotalesRepository
    {
            ResultadosTotales Add(ResultadosTotales parametro);
            void Update(ResultadosTotales parametro);
            void Remove(ResultadosTotales parametro);
            ResultadosTotales GetById(decimal parametro);
            ResultadosTotales GetByName(String parametro);

        }
    }
