﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ITipoRespuestaRepository
    {
        List<TipoRespuesta> GetPZVTYAWs();
        TipoRespuesta Add(TipoRespuesta parametro);
        void Update(TipoRespuesta parametro);
        void Remove(TipoRespuesta parametro);
        TipoRespuesta GetById(decimal parametro);
        TipoRespuesta GetByName(String parametro);

    }
}