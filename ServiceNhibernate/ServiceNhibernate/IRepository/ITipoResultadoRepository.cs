﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface ITipoResultadoRepository
    {
        TipoResultado Add(TipoResultado parametro);
        void Update(TipoResultado parametro);
        void Remove(TipoResultado parametro);
        TipoResultado GetById(decimal parametro);
        TipoResultado GetByName(String parametro);
        List<TipoResultado> GetPZVTYRS();

    }
}