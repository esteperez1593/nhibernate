﻿using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.IRepository
{
    interface IValidatorRepository
    {

        String validarStatusPoll(string _pidm, string _poll, string _calr, int id);

        String validarComponentesInstrumentos(string parametro);
    }
}