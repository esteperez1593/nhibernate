﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class AreaMap : ClassMap<Area>
    {
        public AreaMap()

        {

            Id(x => x.PZBAREA_CODE);

            Map(x => x.PZBAREA_DESCRIPTION);

            Map(x => x.PZBAREA_USER);

            Map(x => x.PZBAREA_DATA_ORIGIN);

            Map(x => x.PZBAREA_ACTIVITY_DATE);

            HasMany(x => x.PZBPLQS)
            .KeyColumn("PZBPLQS_SEQ_NUMBER").Cascade.All().Inverse()
            ;
          
            Table("PZBAREA");

        }
    }


}