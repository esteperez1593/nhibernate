﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class AsignacionCargoDependenciaMap : ClassMap<AsignacionCargoDependencia>
    {
        public AsignacionCargoDependenciaMap()

        {

            this.CompositeId()
            .KeyReference(x => x.PZVPOST, "PZVPOST_CODE")
            .KeyReference(x => x.PZVDNCY, "PZVDNCY_CODE");

            this.Map(x => x.PZRDYPT_USER);

            this.Map(x => x.PZRDYPT_DATA_ORIGIN);

            this.Map(x => x.PZRDYPT_ACTIVITY_DATE);

            this.Map(x => x.PZRDYPT_POST_CODE);

            this.Map(x => x.PZRDYPT_DNCY_CODE);

            this.HasOne(x => x.PZVPOST);

            this.HasOne(x => x.PZVDNCY);

            this.HasMany(x => x.PZRASPE)
                 .KeyColumn("PZRASPE_POST_CODE")
                 .KeyColumn("PZRASPE_DNCY_CODE")
                 .KeyColumn("PZRASPE_PFLE_CODE")
                 .KeyColumn("PZRASPE_PIDM").Cascade.All().Inverse();

            HasOne(x => x.PZVDNCY).ForeignKey("PZVDNCY_CODE");

            HasOne(x => x.PZVPOST).ForeignKey("PZVPOST_CODE");
                
       
            Table("PZRDYPT");

    }


   


    }
}