﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class AsignacionCargoPersonaMap : ClassMap<AsignacionCargoPersona>
    {
        public AsignacionCargoPersonaMap()

        {
            this.CompositeId()
            .KeyReference(x => x.PZRDYPT, "PZRDYPT_POST_CODE", "PZRDYPT_DNCY_CODE")
            .KeyReference(x => x.PZVPFLE, "PZVPFLE_CODE")
            .KeyReference(x => x.PZBPRSO, "PZBPRSO_PIDM");


            this.Map(x => x.PZRASPE_START_DATE);

            this.Map(x => x.PZRASPE_END_DATE);

            this.Map(x => x.PZRASPE_USER);

            this.Map(x => x.PZRASPE_DATA_ORIGIN);

            this.Map(x => x.PZRASPE_ACTIVITY_DATE);

            this.Map(x => x.PZRASPE_POST_CODE);

            this.Map(x => x.PZRASPE_DNCY_CODE);

            this.Map(x => x.PZRASPE_PFLE_CODE);

            this.Map(x => x.PZRASPE_PIDM);

            this.HasOne(x => x.PZRDYPT);

            this.HasOne(x => x.PZVPFLE);

            this.HasOne(x => x.PZBPRSO);

            this.HasMany(x => x.PZRPLAP)
                .KeyColumn("PZRPLAP_SEQ_NUMBER").Cascade.All().Inverse();

            this.HasMany(x => x.PZVPERM)
                .KeyColumn("PZVPERM_CODE").Cascade.All().Inverse();
         

          
            Table("PZRASPE");

        }
    }
}