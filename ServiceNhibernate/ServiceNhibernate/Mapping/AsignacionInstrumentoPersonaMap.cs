﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class AsignacionInstrumentoPersonaMap : ClassMap<AsignacionInstrumentoPersona>
    {
        public AsignacionInstrumentoPersonaMap()
        {

            Id(x => x.PZRPLAP_SEQ_NUMBER);

            Map(x => x.PZRPLAP_STATUS);

            Map(x => x.PZRPLAP_APPL_SEQ_NUMBER);

            Map(x => x.PZRPLAP_USER);

            Map(x => x.PZRPLAP_DATA_ORIGIN);

            Map(x => x.PZRPLAP_ACTIVITY_DATE);

            Map(x => x.PZRPLAP_DATE);

            Map(x => x.PZRPLAP_POLL_CODE);

            Map(x => x.PZRPLAP_POST_CODE);

            Map(x => x.PZRPLAP_DNCY_CODE);

            Map(x => x.PZRPLAP_PFLE_CODE);

            Map(x => x.PZRPLAP_PIDM);

            Map(x => x.PZRPLAP_CALR_CODE);

            HasOne(x => x.PZBAPPL);
                /*.ForeignKey("PZBAPPL_APPL_SEQ_NUMBER", "PZBAPPL_POLL_CODE", "PZBAPPL_CALR_CODE");
                */
            HasOne(x => x.PZRASPE);

            HasMany(x => x.PZRAWAP)
                .KeyColumn("PZRAWAP_SEQ_NUMBER").Cascade.All().Inverse(); 

            HasMany(x => x.PZRRSPL)
                .KeyColumn("PZRRSPL_SEQ_NUMBER").Cascade.All().Inverse(); ;

            HasMany(x => x.PZRRSTT)
                .KeyColumn("PZRRSTT_ID").Cascade.All().Inverse(); 


            Table("PZRPLAP");

        }
    }
}