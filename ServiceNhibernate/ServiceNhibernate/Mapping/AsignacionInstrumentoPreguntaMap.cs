﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class AsignacionInstrumentoPreguntaMap : ClassMap<AsignacionInstrumentoPregunta>
    {
        public AsignacionInstrumentoPreguntaMap()

        {

            this.Id(x => x.PZBPLQS_SEQ_NUMBER);

            this.Map(x => x.PZBPLQS_CALR_CODE);

            this.Map(x => x.PZBPLQS_POLL_CODE);

            this.Map(x => x.PZBPLQS_APPL_SEQ_NUMBER);

            this.Map(x => x.PZBPLQS_AREA_CODE);

            this.Map(x => x.PZBPLQS_CRIT_CODE);

            this.Map(x => x.PZBPLQS_QSTN_CODE);

            this.Map(x => x.PZBPLQS_TYAW_CODE);

            this.Map(x => x.PZBPLQS_MAX_OVAL);

            this.Map(x => x.PZBPLQS_MIN_OVAL);

            this.Map(x => x.PZBPLQS_MAX_VALUE);

            this.Map(x => x.PZBPLQS_MIN_VALUE);

            this.Map(x => x.PZBPLQS_ORDER);

            this.Map(x => x.PZBPLQS_STATUS);

            this.Map(x => x.PZBPLQS_USER);

            this.Map(x => x.PZBPLQS_DATA_ORIGIN);

            this.Map(x => x.PZBPLQS_ACTIVITY_DATE);

            this.HasOne(x => x.PZBAPPL);

            this.HasOne(x => x.PZBAREA);

            this.HasOne(x => x.PZBCRIT);

            this.HasOne(x => x.PZBQSTN);

            this.HasOne(x => x.PZVTYAW);

            this.HasMany(x => x.PZRAWAP)
                .KeyColumn("PZRAWAP_SEQ_NUMBER").Cascade.All().Inverse();


            Table("PZBPLQS");

        }
    }
}
