﻿using ServiceNhibernate.Domain;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class CalendarioMap : ClassMap<Calendario>
    {
        public CalendarioMap()

        {


            Id(x => x.PZBCALR_CODE);

            Map(x => x.PZBCALR_NAME);

            Map(x => x.PZBCALR_SRL);

            Map(x => x.PZBCALR_INIT_DATE);

            Map(x => x.PZBCALR_END_DATE);

            Map(x => x.PZBCALR_TERM);

            Map(x => x.PZBCALR_USER);

            Map(x => x.PZBCALR_ACTIVITY_DATE);

            Map(x => x.PZBCALR_DATA_ORIGIN);



            HasMany(x => x.PZBPHSE)
            .KeyColumn("PZBPHSE_CODE")
             .Cascade.All().Inverse();


            HasMany(x => x.PZBAPPL)
                .KeyColumn("PZBAPPL_SEQ_NUMBER")
                .KeyColumn("PZBAPPL_POLL_CODE")
                .KeyColumn("PZBAPPL_CALR_CODE")
             .Cascade.All().Inverse();
         
            Table("PZBCALR");
       
        }
    }
}