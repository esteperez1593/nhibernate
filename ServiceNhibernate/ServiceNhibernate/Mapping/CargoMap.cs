﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class CargoMap : ClassMap<Cargo>
    {
        public CargoMap()

        {

            Id(x => x.PZVPOST_CODE);

            Map(x => x.PZVPOST_NAME);

            Map(x => x.PZVPOST_DESCRIPTION);

            Map(x => x.PZVPOST_USER);

            Map(x => x.PZVPOST_DATA_ORIGIN);

            Map(x => x.PZVPOST_ACTIVITY_DATE);

            Map(x => x.PZVPOST_CODE_SUP).Column("PZVPOST_CODE_SUP");

                
            HasMany(x => x.PZRDYPT)
            .KeyColumn("PZVPOST_CODE")
            .KeyColumn("PZVDNCY_CODE")
            .Cascade.All().Inverse()
            ;

            HasMany(x => x.PZVPOST1)
                .KeyColumn("PZVPOST_CODE").Cascade.All().Inverse();

            HasOne(x => x.PZVPOST2);


            Table("PZVPOST");

         
    }
    }
}