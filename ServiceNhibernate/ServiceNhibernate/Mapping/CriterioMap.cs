﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class CriterioMap : ClassMap<Criterio>
    {
        public CriterioMap()

        {

            Id(x => x.PZBCRIT_CODE);

            Map(x => x.PZBCRIT_USER);

            Map(x => x.PZBCRIT_DESCRIPTION);

            Map(x => x.PZBCRIT_DATA_ORIGIN);

            Map(x => x.PZBCRIT_ACTIVITY_DATE);

            HasMany(x => x.PZBPLQS)
           .KeyColumn("PZBPLQS_SEQ_NUMBER").Cascade.All().Inverse();


            //    Map(x => x.PZVPOST_DATA_ORIGIN)            
            Table("PZBCRIT");

        }
    }
}