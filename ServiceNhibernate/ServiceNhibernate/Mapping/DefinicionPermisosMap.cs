﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{

    class DefinicionPermisosMap : ClassMap<DefinicionPermisos>
    {
        public DefinicionPermisosMap()

        {

            Id(x => x.PZVRGHT_CODE);

            Map(x => x.PZVRGHT_DESCRIPTION);

            Map(x => x.PZVRGHT_GRADE);

            Map(x => x.PZVRGHT_USER);

            Map(x => x.PZVRGHT_ACTIVITY_DATE);

            Map(x => x.PZVRGHT_DATA_ORIGIN);

            HasMany(x => x.PZVPERM)
                .KeyColumn("PZVPERM_CODE").Cascade.All().Inverse();


            Table("PZVRGHT");

        }
    }

}