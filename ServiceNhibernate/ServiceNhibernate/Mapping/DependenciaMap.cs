﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class DependenciaMap : ClassMap<Dependencia>
    {
        public DependenciaMap()

        {

            Id(x => x.PZVDNCY_CODE);

            Map(x => x.PZVDNCY_CODE_SUP).Column("PZVDNCY_CODE_SUP");

            Map(x => x.PZVDNCY_KEY);

            Map(x => x.PZVDNCY_LEVEL);

            Map(x => x.PZVDNCY_NAME);

            Map(x => x.PZVDNCY_SITE);

            Map(x => x.PZVDNCY_USER);

            Map(x => x.PZVDNCY_ACTIVITY_DATE);

            Map(x => x.PZVDNCY_DATA_ORIGIN);

            HasMany(x => x.PZVDNCY1)
                .KeyColumn("PZVDNCY_CODE").Cascade.All().Inverse();

            HasMany(x => x.PZRDYPT)
                 .KeyColumn("PZVPOST_CODE")
                 .KeyColumn("PZVDNCY_CODE").Cascade.All().Inverse();

            HasOne(x => x.PZVDNCY2);



            Table("PZVDNCY");

        }
    }
}