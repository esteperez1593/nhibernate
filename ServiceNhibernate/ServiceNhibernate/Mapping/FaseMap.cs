﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class FaseMap : ClassMap<Fase>
    {
        public FaseMap()

        {

            Id(x => x.PZBPHSE_CODE);

            Map(x => x.PZBPHSE_NAME);

            Map(x => x.PZBPHSE_USER);

            Map(x => x.PZBPHSE_CALR_CODE);

            Map(x => x.PZBPHSE_OPEN_DATE);

            Map(x => x.PZBPHSE_CLOSE_DATE);

            Map(x => x.PZBPHSE_OPEXTENSION_DATE);

            Map(x => x.PZBPHSE_CLEXTENSION_DATE);

            Map(x => x.PZBPHSE_ACTIVITY_DATE);

            Map(x => x.PZBPHSE_DATA_ORIGIN);

            HasMany(x => x.PZRMSSG)
                .KeyColumn("PZRMSSG_ID").Cascade.All().Inverse();

            HasOne(x => x.PZBCALR);

            //    Map(x => x.PZVPOST_DATA_ORIGIN)            
            Table("PZBPHSE");

        }
    }
}