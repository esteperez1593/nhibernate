﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class InstrumentoAplicableMap : ClassMap<InstrumentoAplicable>
    {
        public InstrumentoAplicableMap()

        {
            this.CompositeId()
            .KeyReference(x => x.PZBAPPL_SEQ_NUMBER)
            .KeyReference(x => x.PZBPOLL, "PZBPOLL_CODE")
            .KeyReference(x => x.PZBCALR, "PZBCALR_CODE");

            this.Map(x => x.PZBAPPL_POLL_CODE);

            this.Map(x => x.PZBAPPL_CALR_CODE);

            this.Map(x => x.PZBAPPL_PFLE_CODE);

            this.Map(x => x.PZBAPPL_PRCNT);

            this.Map(x => x.PZBAPPL_USER);

            this.Map(x => x.PZBAPPL_ACTIVITY_DATE);

            this.Map(x => x.PZBAPPL_DATA_ORIGIN);

            this.HasOne(x => x.PZBCALR).ForeignKey("PZBCALR_CODE");

            this.HasOne(x => x.PZVPFLE).ForeignKey("PZVPFLE_CODE");

            this.HasOne(x => x.PZBPOLL).ForeignKey("PZBPOLL_CODE");

            this.HasMany(x => x.PZRPLAP)
                .KeyColumn("PZRPLAP_SEQ_NUMBER").Cascade.All().Inverse();

            this.HasMany(x => x.PZBPLQS)
                .KeyColumn("PZBPLQS_SEQ_NUMBER").Cascade.All().Inverse();

            this.HasMany(x => x.PZBAPRS)
                .KeyColumn("PZBAPRS_SEQ_NUMBER").Cascade.All().Inverse();

            HasOne(x => x.PZBCALR).ForeignKey("PZBCALR_CODE");

          //  HasOne(x => x.PZVPFLE).ForeignKey("PZVPFLE_CODE");

         


            Table("PZBAPPL");

        }
    }
}