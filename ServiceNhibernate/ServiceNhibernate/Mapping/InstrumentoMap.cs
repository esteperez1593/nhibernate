﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class InstrumentoMap : ClassMap<Instrumento>
    {
        public InstrumentoMap()

        {

            Id(x => x.PZBPOLL_CODE);

            Map(x => x.PZBPOLL_NAME);

            Map(x => x.PZBPOLL_USER);

            Map(x => x.PZBPOLL_ACTIVITY_DATE);

            Map(x => x.PZBPOLL_DATA_ORIGIN);

            HasMany(x => x.PZBAPPL)
                .KeyColumn("PZBAPPL_SEQ_NUMBER")
                .KeyColumn("PZBAPPL_POLL_CODE")
                .KeyColumn("PZBAPPL_CALR_CODE").Cascade.All().Inverse(); 



            Table("PZBPOLL");

        }
    }
}