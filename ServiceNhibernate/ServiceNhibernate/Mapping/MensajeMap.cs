﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class MensajeMap : ClassMap<Mensaje>
    {
        public MensajeMap()

        {

            Id(x => x.PZRMSSG_ID);

            Map(x => x.PZRMSSG_PHSE_CODE);

            Map(x => x.PZRMSSG_USER);

            Map(x => x.PZRMSSG_DESCRIPTION);

            Map(x => x.PZRMSSG_DATA_ORIGIN);

            Map(x => x.PZRMSSG_ACTIVITY_DATE);

            HasOne(x => x.PZBPHSE);

            //    Map(x => x.PZVPOST_DATA_ORIGIN)            
            Table("PZRMSSG");

        }
    }
}