﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class PerfilMap : ClassMap<Perfil>
    {
        public PerfilMap()

        {


            Id(x => x.PZVPFLE_CODE);

            Map(x => x.PZVPFLE_NAME);

            Map(x => x.PZVPFLE_USER);

            Map(x => x.PZVPFLE_ACTIVITY_DATE);

            Map(x => x.PZVPFLE_DATA_ORIGIN);



        /*    HasMany(x => x.PZBAPPL)
                .KeyColumn("PZBAPPL_SEQ_NUMBER")
                .KeyColumn("PZBPOLL_CODE")
                .KeyColumn("PZBCALR_CODE").Cascade.All().Inverse();
                */
            this.HasMany(x => x.PZRASPE)
                 .KeyColumn("PZRASPE_POST_CODE")
                 .KeyColumn("PZRASPE_DNCY_CODE")
                 .KeyColumn("PZRASPE_PFLE_CODE")
                 .KeyColumn("PZRASPE_PIDM").Cascade.All().Inverse();

            this.HasMany(x => x.PZVPERM)
                .KeyColumn("PZVPERM_CODE").Cascade.All().Inverse();

            Table("PZVPFLE");

        }
    }
}