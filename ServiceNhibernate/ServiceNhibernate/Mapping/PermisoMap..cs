﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class PermisoMap : ClassMap<Permiso>
    {
        public PermisoMap()

        {

            Id(x => x.PZVPERM_CODE);

            Map(x => x.PZVPERM_POST_CODE);

            Map(x => x.PZVPERM_DNCY_CODE);

            Map(x => x.PZVPERM_PFLE_CODE);

            Map(x => x.PZVPERM_PIDM);

            Map(x => x.PZVPERM_RGHT_CODE);

            Map(x => x.PZVPERM_USER);

            Map(x => x.PZVPERM_DATA_ORIGIN);

            Map(x => x.PZVPERM_ACTIVITY_DATE);

            HasOne(x => x.PZRASPE);

            HasOne(x => x.PZVPFLE);

            HasOne(x => x.PZVRGHT);


            Table("PZVPERM");

        }
    }
}