﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class PersonaMap : ClassMap<Persona>
    {
        public PersonaMap()

        {

            Id(x => x.PZBPRSO_PIDM);

            Map(x => x.PZBPRSO_SITE);

            Map(x => x.PZBPRSO_ACTIVE);

            Map(x => x.PZBPRSO_PAYSHEET);

            Map(x => x.PZBPRSO_USER);

            Map(x => x.PZBPRSO_DATA_ORIGIN);

            Map(x => x.PZBPRSO_ACTIVITY_DATE);

            HasMany(x => x.PZRASPE)
                 .KeyColumn("PZRASPE_POST_CODE")
                 .KeyColumn("PZRASPE_DNCY_CODE")
                 .KeyColumn("PZRASPE_PFLE_CODE")
                 .KeyColumn("PZRASPE_PIDM").Cascade.All().Inverse(); 
         
            Table("PZBPRSO");

        }
    }
}