﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class PreguntaMap : ClassMap<Pregunta>
    {
        public PreguntaMap()

        {

            Id(x => x.PZBQSTN_CODE);

            Map(x => x.PZBQSTN_USER);

            Map(x => x.PZBQSTN_DESCRIPTION);

            Map(x => x.PZBQSTN_NAME);

            Map(x => x.PZBQSTN_DATA_ORIGIN);

            Map(x => x.PZBQSTN_ACTIVITY_DATE);

            HasMany(x => x.PZBPLQS)
                .KeyColumn("PZBPLQS_SEQ_NUMBER").Cascade.All().Inverse();

            Table("PZBQSTN");

        }
    }
}