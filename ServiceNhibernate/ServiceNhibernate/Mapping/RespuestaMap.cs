﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class RespuestaMap : ClassMap<Respuesta>
    {
        public RespuestaMap()

        {

            Id(x => x.PZRAWAP_SEQ_NUMBER);

            Map(x => x.PZRAWAP_PLQS_SEQ_NUMBER);

            Map(x => x.PZRAWAP_PLAP_SEQ_NUMBER);

            Map(x => x.PZRAWAP_PERC_IMP);

            Map(x => x.PZRAWAP_DESCRIPTION);

            Map(x => x.PZRAWAP_VALUE);

            Map(x => x.PZRAWAP_USER);

            Map(x => x.PZRAWAP_DATA_ORIGIN);

            Map(x => x.PZRAWAP_ACTIVITY_DATE);
           
            HasOne(x => x.PZBPLQS).ForeignKey("PZBPLQS_SEQ_NUMBER");

            HasOne(x => x.PZRPLAP).ForeignKey("PZRPLAP_SEQ_NUMBER");

        

            Table("PZRAWAP");

    }
  }
}