﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class ResultadoAplicableMap : ClassMap<ResultadoAplicable>
    {
        public ResultadoAplicableMap()

        {


            Id(x => x.PZBAPRS_SEQ_NUMBER);

            Map(x => x.PZBAPRS_APPL_SEQ_NUMBER);

            Map(x => x.PZBAPRS_POLL_CODE);

            Map(x => x.PZBAPRS_CALR_CODE);

            Map(x => x.PZBAPRS_TYRS_CODE);

            Map(x => x.PZBAPRS_ORDER);

            Map(x => x.PZBAPRS_USER);

            Map(x => x.PZBAPRS_ACTIVITY_DATE);

            Map(x => x.PZBAPRS_DATA_ORIGIN);

            HasOne(x => x.PZBAPPL);

            HasOne(x => x.PZVTYRS);         

            HasMany(x => x.PZRRSPL)
                .KeyColumn("PZRRSPL_SEQ_NUMBER")
                .Cascade.All().Inverse();

 



            Table("PZBAPRS");

        }
    }
}