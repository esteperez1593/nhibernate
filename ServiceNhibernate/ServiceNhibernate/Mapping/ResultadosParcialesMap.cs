﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class ResultadosParcialesMap : ClassMap<ResultadosParciales>
    {
        public ResultadosParcialesMap()

        {

            Id(x => x.PZRRSPL_SEQ_NUMBER);

            Map(x => x.PZRRSPL_PLAP_SEQ_NUMBER);

            Map(x => x.PZRRSPL_APRS_SEQ_NUMBER);

            Map(x => x.PZRRSPL_NUMB_VALUE);

            Map(x => x.PZRRSPL_STR_VALUE);

            Map(x => x.PZRRSPL_USER);

            Map(x => x.PZRRSPL_ACTIVITY_DATE);

            Map(x => x.PZRRSPL_DATA_ORIGIN);

            HasOne(x => x.PZBAPRS);

            HasOne(x => x.PZRPLAP);

  
            Table("PZRRSPL");

        }
    }
}