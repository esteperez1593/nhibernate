﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class ResultadosTotalesMap : ClassMap<ResultadosTotales>
    {
        public ResultadosTotalesMap()

        {

            Id(x => x.PZRRSTT_ID);

            Map(x => x.PZRRSTT_PLAP_SEQ_NUMBER);

            Map(x => x.PZRRSTT_RESULT_VALUE);

            Map(x => x.PZRRSTT_DESCRIPTION);

            Map(x => x.PZRRSTT_USER);

            Map(x => x.PZRRSTT_DATA_ORIGIN);

            Map(x => x.PZRRSTT_ACTIVITY_DATE);

            HasOne(x => x.PZRPLAP);
           
            Table("PZRRSTT");
           
    }
}
}