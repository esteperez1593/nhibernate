﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class TipoRespuestaMap : ClassMap<TipoRespuesta>
    {
        public TipoRespuestaMap()

        {

            Id(x => x.PZVTYAW_CODE);

            Map(x => x.PZVTYAW_USER);

            Map(x => x.PZVTYAW_DESCRIPTION);

            Map(x => x.PZVTYAW_DATA_ORIGIN);

            Map(x => x.PZVTYAW_ACTIVITY_DATE);

            HasMany(x => x.PZBPLQS).KeyColumn("PZBPLQS_SEQ_NUMBER").Cascade.All().Inverse();

            Table("PZVTYAW");

        }
    }
}