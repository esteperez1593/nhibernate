﻿using FluentNHibernate.Mapping;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Mapping
{
    class TipoResultadoMap : ClassMap<TipoResultado>
    {
        public TipoResultadoMap()
        {
            Id(x => x.PZVTYRS_CODE);

            Map(x => x.PZVTYRS_DESCRIPTION);
            Map(x => x.PZVTYRS_USER);
            Map(x => x.PZVTYRS_ACTIVITY_DATE);
            Map(x => x.PZVTYRS_DATA_ORIGIN);

            HasMany(x => x.PZBAPRS)
               .KeyColumn("PZBAPRS_SEQ_NUMBER").Cascade.All().Inverse();

            Table("PZVTYRS");
    }
    }
}