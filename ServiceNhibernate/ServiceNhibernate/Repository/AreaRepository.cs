﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;
using NHibernate.Transform;

namespace ServiceNhibernate.Repository
{
    class AreaRepository : IAreaRepository
    {

        public List<Area> GetPZBAREA()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                List<Domain.Area> result = new List<Domain.Area>();
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var respuesta = session.
                         CreateSQLQuery(@"Select * from PZBAREA")
                         .AddEntity("PZBAREA",typeof(Domain.Area))
                         .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                         .List<Area>();

                        foreach (var X in respuesta)
                        {

                            //Datos del Area
                            Domain.Area _area = new Domain.Area();
                            //  _area.PZBAREA_ID = X.PZBAREA_ID;
                            _area.PZBAREA_CODE = X.PZBAREA_CODE;
                            _area.PZBAREA_DESCRIPTION = X.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_USER = X.PZBAREA_USER;
                            _area.PZBAREA_DATA_ORIGIN = X.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = X.PZBAREA_ACTIVITY_DATE;

                            //_area._PZBAREA_POLL_CODE = X.PZBAREA_POLL_CODE;
                            result.Add(_area);
                        }


                        return result;

                    }
                    catch (Exception e) { }


                }

                return result;

            }

        }
        public Area Add(Area _area)
        {
            throw new NotImplementedException();
        }

        public Area GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public Area GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(Area _area)
        {
            throw new NotImplementedException();
        }

        public void Update(Area _area)
        {
            throw new NotImplementedException();
        }
    }
}