﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;
using NHibernate.Transform;
using NHibernate.Criterion;
using System.Web.Http;

namespace ServiceNhibernate.Repository
{
    class AsignacionCargoDependenciaRepository : IAsignacionCargoDependenciaRepository
    {
        public AsignacionCargoDependencia Add(AsignacionCargoDependencia calendario)
        {
            throw new NotImplementedException();
        }

       
        public List<AsignacionCargoDependencia> GetAll() 
        {
            List<Domain.AsignacionCargoDependencia> re = new List<Domain.AsignacionCargoDependencia>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using ( ITransaction transaction = session.BeginTransaction())
                {



                    var respuesta = session.
                        CreateSQLQuery(@"select *
                                                from  
                                                     PZRDYPT cardep, 
                                                     PZVDNCY dep,
                                                     PZVPOST car
                                                where cardep.pzrdypt_dncy_code = dep.pzvdncy_code
                                                      and cardep.pzrdypt_post_code = car.pzvpost_code
                                                      and car.pzvpost_code='RECTOR'                
                                                ")
                                                /*.AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddJoin("PZVPOST", "PZRDYPT.PZVPOST")
                                                .AddJoin("PZVDNCY", "PZRDYPT.PZVDNCY")*/
                                                
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionCargoDependencia>();
                    
                    foreach (var result in respuesta)
                    {

                        Domain.AsignacionCargoDependencia cd = new Domain.AsignacionCargoDependencia();

                        //Datos de la Ternaria   
                        cd.PZRDYPT_POST_CODE = result.PZRDYPT_POST_CODE;
                        cd.PZRDYPT_DNCY_CODE = result.PZRDYPT_DNCY_CODE;
                        // cd.PZRDYPT_ID = x.PZRDYPT_ID;
                        cd.PZRDYPT_USER = result.PZRDYPT_USER;
                        cd.PZRDYPT_DATA_ORIGIN = result.PZRDYPT_DATA_ORIGIN;
                        cd.PZRDYPT_ACTIVITY_DATE = result.PZRDYPT_ACTIVITY_DATE;

                        
                        //DEPENDENCIA
                        Domain.Dependencia dep = new Domain.Dependencia();
                        //dep.PZVDNCY_ID = x.PZVDNCY.PZVDNCY_ID;
                        dep.PZVDNCY_KEY = result.PZVDNCY.PZVDNCY_KEY;
                        dep.PZVDNCY_SITE = result.PZVDNCY.PZVDNCY_SITE;
                        dep.PZVDNCY_LEVEL = result.PZVDNCY.PZVDNCY_LEVEL;
                        dep.PZVDNCY_NAME = result.PZVDNCY.PZVDNCY_NAME;
                        dep.PZVDNCY_ACTIVITY_DATE = result.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
                        dep.PZVDNCY_CODE_SUP = result.PZVDNCY.PZVDNCY_CODE_SUP;
                        dep.PZVDNCY_DATA_ORIGIN = result.PZVDNCY.PZVDNCY_DATA_ORIGIN;
                        dep.PZVDNCY_USER = result.PZVDNCY.PZVDNCY_USER;
                        cd.PZVDNCY = dep;
                        

                       /* //Departamento Evaluador
                        Domain.Dependencia depEval = new Domain.Dependencia();
                        // depEval.PZVDNCY_ID = x.PZVDNCY.PZVDNCY2.PZVDNCY_ID;
                        depEval.PZVDNCY_KEY = result.PZVDNCY.PZVDNCY2.PZVDNCY_KEY;
                        depEval.PZVDNCY_SITE = result.PZVDNCY.PZVDNCY2.PZVDNCY_SITE;
                        depEval.PZVDNCY_LEVEL = result.PZVDNCY.PZVDNCY2.PZVDNCY_LEVEL;
                        depEval.PZVDNCY_NAME = result.PZVDNCY.PZVDNCY2.PZVDNCY_NAME;
                        depEval.PZVDNCY_ACTIVITY_DATE = result.PZVDNCY.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;

                        dep.PZVDNCY2 = depEval;*/

                        cd.PZVDNCY = dep;

                        //Cargo
                        Domain.Cargo item = new Domain.Cargo();

                        //item.PZVPOST_ID = x.PZVPOST.PZVPOST_ID;
                        item.PZVPOST_CODE = result.PZVPOST.PZVPOST_CODE;
                        item.PZVPOST_NAME = result.PZVPOST.PZVPOST_NAME;
                        item.PZVPOST_CODE_SUP = result.PZVPOST.PZVPOST_CODE_SUP;
                        item.PZVPOST_USER = result.PZVPOST.PZVPOST_USER;
                        item.PZVPOST_DESCRIPTION = result.PZVPOST.PZVPOST_DESCRIPTION;
                        item.PZVPOST_ACTIVITY_DATE = result.PZVPOST.PZVPOST_ACTIVITY_DATE;
                        item.PZVPOST_DATA_ORIGIN = result.PZVPOST.PZVPOST_DATA_ORIGIN;
                        
                        // Cargo Supervisor
                        /*
                        Domain.Cargo itemSup = new Domain.Cargo();

                        //itemSup.PZVPOST_ID = x.PZVPOST.PZVPOST2.PZVPOST_ID;
                        itemSup.PZVPOST_CODE = result.PZVPOST.PZVPOST2.PZVPOST_CODE;
                        itemSup.PZVPOST_NAME = result.PZVPOST.PZVPOST2.PZVPOST_NAME;
                        itemSup.PZVPOST_CODE_SUP = result.PZVPOST.PZVPOST_CODE_SUP;
                        itemSup.PZVPOST_USER = result.PZVPOST.PZVPOST2.PZVPOST_USER;
                        itemSup.PZVPOST_DESCRIPTION = result.PZVPOST.PZVPOST2.PZVPOST_DESCRIPTION;
                        itemSup.PZVPOST_ACTIVITY_DATE = result.PZVPOST.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                        itemSup.PZVPOST_DATA_ORIGIN = result.PZVPOST.PZVPOST2.PZVPOST_DATA_ORIGIN;

                        item.PZVPOST2 = itemSup;*/

                        cd.PZVPOST = item;
                        
                        re.Add(cd);
                        

                    }

                    return (re);
                      transaction.Commit();
                }

            }
            return re;
        }

        public List<AsignacionCargoPersona> GetEvaluados(String idcar, String iddep) {

            List<Domain.AsignacionCargoPersona> result = new List<Domain.AsignacionCargoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var cardep = session.
                        CreateSQLQuery(@"select *
                                                from PZVPOST CAR ,
                                                     PZVDNCY DEP,
                                                     PZRDYPT CARDEP ,
                                                     PZVDNCY SUP,
                                                     PZVPOST rev,
                                                     pzraspe aspe,
                                                     pzvpfle pfle,
                                                     pzbprso prso
                                                where cardep.pzrdypt_post_code=car.pzvpost_code and 
                                                      car.pzvpost_code_sup = rev.pzvpost_code and
                                                      cardep.PZRDYPT_DNCY_CODE = dep.pzvdncy_CODE and 
                                                      cardep.PZRDYPT_DNCY_CODE = aspe.pzraspe_dncy_code and
                                                      cardep.pzrdypt_post_code = aspe.pzraspe_post_code and
                                                      pfle.pzvpfle_code = aspe.pzraspe_pfle_code and
                                                      prso.pzbprso_pidm = aspe.pzraspe_pidm and
                                                      sup.pzvdncy_CODE = '" + iddep + "' and " +
                                                      "rev.pzvpost_code = '" + idcar + "'")
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZRASPE",typeof(Domain.AsignacionCargoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionCargoPersona>();
                    foreach (var x in cardep)
                    {


                        Domain.AsignacionCargoPersona ppcd = new Domain.AsignacionCargoPersona();
                        //   ppcd.PZRASPE_ID = x;
                        ppcd.PZRASPE_DNCY_CODE = x.PZRASPE_DNCY_CODE;
                        ppcd.PZRASPE_POST_CODE = x.PZRASPE_POST_CODE;
                        ppcd.PZRASPE_PIDM = x.PZRASPE_PIDM;
                        ppcd.PZRASPE_PFLE_CODE = x.PZRASPE_PFLE_CODE;
                        ppcd.PZRASPE_END_DATE = x.PZRASPE_END_DATE;
                        ppcd.PZRASPE_START_DATE = x.PZRASPE_START_DATE;
                        ppcd.PZRASPE_DATA_ORIGIN = x.PZRASPE_DATA_ORIGIN;
                        ppcd.PZRASPE_USER = x.PZRASPE_USER;

                        Domain.AsignacionCargoDependencia cd = new Domain.AsignacionCargoDependencia();

                        //Datos de la Ternaria   
                        cd.PZRDYPT_POST_CODE = x.PZRDYPT.PZRDYPT_POST_CODE;
                        cd.PZRDYPT_DNCY_CODE = x.PZRDYPT.PZRDYPT_DNCY_CODE;
                        cd.PZRDYPT_USER = x.PZRDYPT.PZRDYPT_USER;
                        cd.PZRDYPT_DATA_ORIGIN = x.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                        cd.PZRDYPT_ACTIVITY_DATE = x.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                        ppcd.PZRDYPT = cd;

                        //DEPENDENCIA
                        Domain.Dependencia dep = new Domain.Dependencia();
                        //dep.PZVDNCY_ID = x.PZVDNCY.PZVDNCY_ID;
                        dep.PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                        dep.PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                        dep.PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                        dep.PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                        dep.PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;
                        dep.PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                        dep.PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;

                        ppcd.PZRDYPT.PZVDNCY = dep;

                        Domain.Persona per = new Domain.Persona();
                        per.PZBPRSO_PIDM = x.PZBPRSO.PZBPRSO_PIDM;
                        per.PZBPRSO_SITE = x.PZBPRSO.PZBPRSO_SITE;
                        per.PZBPRSO_USER = x.PZBPRSO.PZBPRSO_USER;
                        per.PZBPRSO_PAYSHEET = x.PZBPRSO.PZBPRSO_PAYSHEET;
                        per.PZBPRSO_DATA_ORIGIN = x.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                        per.PZBPRSO_ACTIVE = x.PZBPRSO.PZBPRSO_ACTIVE;
                        //per.PZBPRSO_PZVPOST = x.PZBPRSO.PZBPRSO_PZCRCOD;

                        ppcd.PZBPRSO = per;


                        //Departamento Evaluador
                        Domain.Dependencia depEval = new Domain.Dependencia();
                        //depEval.PZVDNCY_ID = x.PZVDNCY.PZVDNCY2.PZVDNCY_ID;
                        depEval.PZVDNCY_CODE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_CODE;
                        depEval.PZVDNCY_KEY = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_KEY;
                        depEval.PZVDNCY_SITE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_SITE;
                        depEval.PZVDNCY_LEVEL = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_LEVEL;
                        depEval.PZVDNCY_NAME = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_NAME;
                        depEval.PZVDNCY_USER = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_USER;
                        depEval.PZVDNCY_ACTIVITY_DATE = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_ACTIVITY_DATE;
                        depEval.PZVDNCY_DATA_ORIGIN = x.PZRDYPT.PZVDNCY.PZVDNCY2.PZVDNCY_DATA_ORIGIN;

                        ppcd.PZRDYPT.PZVDNCY.PZVDNCY2 = depEval;
                        dep.PZVDNCY2 = depEval;

                        cd.PZVDNCY = dep;

                        //Cargo
                        Domain.Cargo item = new Domain.Cargo();

                        //item.PZVPOST_ID = x.PZVPOST.PZVPOST_ID;
                        item.PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST_NAME;
                        item.PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                        item.PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST_USER;
                        item.PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                        item.PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                        item.PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST_DATA_ORIGIN;

                        ppcd.PZRDYPT.PZVPOST = item;
                        // Cargo Supervisor

                        Domain.Cargo itemSup = new Domain.Cargo();

                        //itemSup.PZVPOST_ID = x.PZVPOST.PZVPOST2.PZVPOST_ID;
                        itemSup.PZVPOST_CODE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE;
                        itemSup.PZVPOST_NAME = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_NAME;
                        itemSup.PZVPOST_CODE_SUP = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_CODE_SUP;
                        itemSup.PZVPOST_USER = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_USER;
                        itemSup.PZVPOST_DESCRIPTION = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DESCRIPTION;
                        itemSup.PZVPOST_ACTIVITY_DATE = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_ACTIVITY_DATE;
                        itemSup.PZVPOST_DATA_ORIGIN = x.PZRDYPT.PZVPOST.PZVPOST2.PZVPOST_DATA_ORIGIN;

                        ppcd.PZRDYPT.PZVPOST.PZVPOST2 = itemSup;
                        item.PZVPOST2 = itemSup;

                        cd.PZVPOST = item;

                        result.Add(ppcd);

                    }

                    return (result);

                }
            }
         }


        public AsignacionCargoDependencia GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public AsignacionCargoDependencia GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(AsignacionCargoDependencia calendario)
        {
            throw new NotImplementedException();
        }

        public void Update(AsignacionCargoDependencia calendario)
        {
            throw new NotImplementedException();
        }
    }

}
