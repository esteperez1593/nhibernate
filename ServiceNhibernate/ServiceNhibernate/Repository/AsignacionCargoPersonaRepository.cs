﻿using NHibernate;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    public class AsignacionCargoPersonaRepository : IAsignacionCargoPersonaRepository
    {
        public AsignacionCargoPersona Add(AsignacionCargoPersona parametro)
        {
            throw new NotImplementedException();
        }


        public List<AsignacionCargoPersona> GetPZRASPEId(int idper)
        {
            List<Domain.AsignacionCargoPersona> re = new List<Domain.AsignacionCargoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var respuesta = session.
                        CreateSQLQuery(@"SELECT * 
                                                FROM PZBPRSO, 
                                                     PZVPOST,
                                                     PZVPFLE,
                                                     PZRASPE,
                                                     PZRDYPT,
                                                     PZVDNCY
                                                WHERE PZRASPE.PZRASPE_PFLE_CODE = PZVPFLE.PZVPFLE_code
                                                AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM 
                                                AND pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                and PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                and PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                  AND PZBPRSO.PZBPRSO_PIDM = '" + idper + "'")

                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionCargoPersona>();

                    foreach (var result in respuesta)
                    {
                        Domain.AsignacionCargoPersona cp = new AsignacionCargoPersona();

                        cp.PZRASPE_ACTIVITY_DATE = result.PZRASPE_ACTIVITY_DATE;
                        cp.PZRASPE_DATA_ORIGIN = result.PZRASPE_DATA_ORIGIN;
                        cp.PZRASPE_DNCY_CODE = result.PZRASPE_DNCY_CODE;
                        cp.PZRASPE_POST_CODE = result.PZRASPE_POST_CODE;
                        cp.PZRASPE_PFLE_CODE = result.PZRASPE_PFLE_CODE;
                        cp.PZRASPE_START_DATE = result.PZRASPE_START_DATE;
                        cp.PZRASPE_END_DATE = result.PZRASPE_END_DATE;
                        cp.PZRASPE_PIDM = result.PZRASPE_PIDM;
                        cp.PZRASPE_USER = result.PZRASPE_USER;

                        Domain.AsignacionCargoDependencia cd = new AsignacionCargoDependencia();
                        cd.PZRDYPT_POST_CODE = result.PZRDYPT.PZRDYPT_POST_CODE;
                        cd.PZRDYPT_DNCY_CODE = result.PZRDYPT.PZRDYPT_DNCY_CODE;
                        cd.PZRDYPT_USER = result.PZRDYPT.PZRDYPT_USER;
                        cd.PZRDYPT_DATA_ORIGIN = result.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                        cd.PZRDYPT_ACTIVITY_DATE = result.PZRDYPT.PZRDYPT_ACTIVITY_DATE;


                        Domain.Cargo _cargo = new Cargo();
                        _cargo.PZVPOST_CODE = result.PZRDYPT.PZVPOST.PZVPOST_CODE;
                        _cargo.PZVPOST_NAME = result.PZRDYPT.PZVPOST.PZVPOST_NAME;
                        _cargo.PZVPOST_USER = result.PZRDYPT.PZVPOST.PZVPOST_USER;
                        _cargo.PZVPOST_CODE_SUP = result.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                        _cargo.PZVPOST_DESCRIPTION = result.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                        _cargo.PZVPOST_DATA_ORIGIN = result.PZRDYPT.PZVPOST.PZVPOST_DATA_ORIGIN;
                        _cargo.PZVPOST_ACTIVITY_DATE = result.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;

                        Domain.Dependencia _dependencia = new Dependencia();
                        _dependencia.PZVDNCY_CODE = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                        _dependencia.PZVDNCY_KEY = result.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                        _dependencia.PZVDNCY_NAME = result.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                        _dependencia.PZVDNCY_SITE = result.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                        _dependencia.PZVDNCY_USER = result.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                        _dependencia.PZVDNCY_LEVEL = result.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                        _dependencia.PZVDNCY_CODE_SUP = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                        _dependencia.PZVDNCY_DATA_ORIGIN = result.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;
                        _dependencia.PZVDNCY_ACTIVITY_DATE = result.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;

                        cd.PZVDNCY = _dependencia;
                        cd.PZVPOST = _cargo;
                        cp.PZRDYPT = cd;
                        re.Add(cp);


                    }

                    return (re);
                    transaction.Commit();
                }

            }
            return re;
        }



        public List<AsignacionCargoPersona> GetAll()
         {
            List<Domain.AsignacionCargoPersona> re = new List<Domain.AsignacionCargoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var respuesta = session.
                        CreateSQLQuery(@"select *
                                                from  
                                                     PZRDYPT cardep, 
                                                     PZVDNCY dep,
                                                     PZVPOST car,
                                                     PZRASPE aspe,
                                                     PZBPRSO prso,
                                                     PZVPFLE pfle
                                                where cardep.pzrdypt_dncy_code = dep.pzvdncy_code
                                                      and cardep.pzrdypt_post_code = car.pzvpost_code
                                                      and aspe.pzraspe_dncy_code = cardep.pzrdypt_dncy_code
                                                      and aspe.pzraspe_post_code = cardep. pzrdypt_post_code
                                                      and aspe.pzraspe_pfle_code = pfle.pzvpfle_code
                                                      and aspe.pzraspe_pidm = prso.pzbprso_pidm
                                                      and prso.pzbprso_pidm= '272588'
                                                ")

                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionCargoPersona>();

                    foreach (var result in respuesta)
                    {
                        Domain.AsignacionCargoPersona cp = new AsignacionCargoPersona();

                        cp.PZRASPE_ACTIVITY_DATE = result.PZRASPE_ACTIVITY_DATE;
                        cp.PZRASPE_DATA_ORIGIN = result.PZRASPE_DATA_ORIGIN;
                        cp.PZRASPE_DNCY_CODE = result.PZRASPE_DNCY_CODE;
                        cp.PZRASPE_POST_CODE = result.PZRASPE_POST_CODE;
                        cp.PZRASPE_PFLE_CODE = result.PZRASPE_PFLE_CODE;
                        cp.PZRASPE_START_DATE = result.PZRASPE_START_DATE;
                        cp.PZRASPE_END_DATE = result.PZRASPE_END_DATE;
                        cp.PZRASPE_PIDM = result.PZRASPE_PIDM;
                        cp.PZRASPE_USER = result.PZRASPE_USER;

                        Domain.AsignacionCargoDependencia cd = new AsignacionCargoDependencia();
                        cd.PZRDYPT_POST_CODE = result.PZRDYPT.PZRDYPT_POST_CODE;
                        cd.PZRDYPT_DNCY_CODE = result.PZRDYPT.PZRDYPT_DNCY_CODE;
                        cd.PZRDYPT_USER = result.PZRDYPT.PZRDYPT_USER;
                        cd.PZRDYPT_DATA_ORIGIN = result.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                        cd.PZRDYPT_ACTIVITY_DATE = result.PZRDYPT.PZRDYPT_ACTIVITY_DATE;


                        Domain.Cargo _cargo = new Cargo();
                        _cargo.PZVPOST_CODE = result.PZRDYPT.PZVPOST.PZVPOST_CODE;
                        _cargo.PZVPOST_NAME = result.PZRDYPT.PZVPOST.PZVPOST_NAME;
                        _cargo.PZVPOST_USER = result.PZRDYPT.PZVPOST.PZVPOST_USER;
                        _cargo.PZVPOST_CODE_SUP = result.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                        _cargo.PZVPOST_DESCRIPTION = result.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                        _cargo.PZVPOST_DATA_ORIGIN = result.PZRDYPT.PZVPOST.PZVPOST_DATA_ORIGIN;
                        _cargo.PZVPOST_ACTIVITY_DATE = result.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;

                        Domain.Dependencia _dependencia = new Dependencia();
                        _dependencia.PZVDNCY_CODE = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                        _dependencia.PZVDNCY_KEY  = result.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                        _dependencia.PZVDNCY_NAME = result.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                        _dependencia.PZVDNCY_SITE = result.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                        _dependencia.PZVDNCY_USER = result.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                        _dependencia.PZVDNCY_LEVEL = result.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                        _dependencia.PZVDNCY_CODE_SUP = result.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                        _dependencia.PZVDNCY_DATA_ORIGIN = result.PZRDYPT.PZVDNCY.PZVDNCY_DATA_ORIGIN;
                        _dependencia.PZVDNCY_ACTIVITY_DATE = result.PZRDYPT.PZVDNCY.PZVDNCY_ACTIVITY_DATE;

                        cd.PZVDNCY = _dependencia;
                        cd.PZVPOST = _cargo;
                        cp.PZRDYPT = cd;
                        re.Add(cp);


                    }

                    return (re);
                    transaction.Commit();
                }

            }
            return re;
        }

        public AsignacionCargoPersona GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public AsignacionCargoPersona GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(AsignacionCargoPersona parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(AsignacionCargoPersona parametro)
        {
            throw new NotImplementedException();
        }
    }
}