﻿using NHibernate;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    class AsignacionInstrumentoPersonaRepository : IAsignacionInstrumentoPersonaRepository
    {
      

        public List<AsignacionInstrumentoPersona> GetAll()
        {

            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@" SELECT *
                                                FROM PZRPLAP
                                                WHERE PZRPLAP_SEQ_NUMBER=172
                                                ")
                                                .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();


                    foreach (var result in es)
                    {
                        Domain.AsignacionInstrumentoPersona item = new Domain.AsignacionInstrumentoPersona();
                        
                        item.PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                        item.PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                        item.PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                        item.PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                        item.PZRPLAP_POLL_CODE = result.PZRPLAP_POLL_CODE;
                        item.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                        item.PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                        item.PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                        item.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                        item.PZRPLAP_USER = result.PZRPLAP_USER;
                        item.PZRPLAP_DATE = result.PZRPLAP_DATE;
                        item.PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                        item.PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;

                        re.Add(item);
                    }
                    

                    return (re);
                    transaction.Commit();
                }

            }
            return re;
        }

        public List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPen(String id)
        {


            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@"  SELECT *
                                                   FROM 
                                                        --INSTRUMENTO
                                                        PZRPLAP,
                                                        PZBPOLL,
                                                        PZBAPPL,
                                                        PZBCALR,
                                                        PZVPFLE,
                                                        --EVALUADO
                                                        PZVPOST,
                                                        PZRDYPT,
                                                        PZVDNCY,
                                                        PZRASPE,
                                                        PZBPRSO,
                                                        --EVALUADOR
                                                        PZVPOST POST1,
                                                        PZVDNCY DNCY1,
                                                        PZRDYPT DYPT1,
                                                        PZRASPE ASPE1,
                                                        PZBPRSO PRSO1
                                                   WHERE PZRPLAP.PZRPLAP_STATUS='P'     
                                                     and pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                     AND pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                     AND pzvdncy.pzvdncy_code = pzrdypt.pzrdypt_dncy_code
                                                     AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM
                                                     AND PZVDNCY.PZVDNCY_CODE_sup = DNCY1.PZVDNCY_CODE
                                                     AND PZVPOST.PZVPOST_CODE_sup = POST1.PZVPOST_CODE 
                                                     AND POST1.PZVPOST_CODE = dypt1.pzrdypt_post_code 
                                                     AND dypt1.pzrdypt_dncy_code = DNCY1.PZVDNCY_CODE
                                                     AND aspe1.pzraspe_post_code = dypt1.pzrdypt_post_code
                                                     AND aspe1.pzraspe_dncy_code = dypt1.pzrdypt_dncy_code
                                                     and aspe1.pzraspe_pidm= prso1.pzbprso_pidm
                                                     and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                     and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                     AND pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                     and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                     and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                     and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                     and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                     and pzbappl.pzbappl_calr_code = pzrplap.pzrplap_calr_code
                                                     and pzbappl.pzbappl_pfle_code = pzvpfle.pzvpfle_code
                                                     and pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code 
                                                     and prso1.pzbprso_pidm= '" + id + "' ")
                                                    .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                    .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                    .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                    .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                    .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                    .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                    .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                    .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                    .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                    .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();


                    foreach (var result in es)
                    {
                        

                            Domain.AsignacionInstrumentoPersona item = new Domain.AsignacionInstrumentoPersona();
                            item.PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                            item.PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                            item.PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                            item.PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                            item.PZRPLAP_POLL_CODE = result.PZRPLAP_POLL_CODE;
                            item.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                            item.PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                            item.PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                            item.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                            item.PZRPLAP_USER = result.PZRPLAP_USER;
                            item.PZRPLAP_DATE = result.PZRPLAP_DATE;
                            item.PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                            item.PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;


                            //Datos Instrumento aplicado
                            Domain.InstrumentoAplicable _insApl = new InstrumentoAplicable();
                            _insApl.PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                            _insApl.PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                            _insApl.PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                            _insApl.PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                            _insApl.PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                            _insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                            _insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                            _insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                            item.PZBAPPL = _insApl;

                            //Datos Instrumento
                            Domain.Instrumento ins = new Domain.Instrumento();
                            ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                            ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                            ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                            ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                            item.PZBAPPL.PZBPOLL = ins;

                            //Datos Calendario
                            Domain.Calendario cal = new Domain.Calendario();
                            cal.PZBCALR_CODE = result.PZBAPPL.PZBCALR.PZBCALR_CODE;
                            cal.PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                            cal.PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                            cal.PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                            cal.PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                            cal.PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                            cal.PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                            cal.PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                            item.PZBAPPL.PZBCALR = cal;



                            // Datos PER_PER
                            Domain.AsignacionCargoPersona ppcd = new Domain.AsignacionCargoPersona();
                            ppcd.PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                            ppcd.PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                            ppcd.PZRASPE_DNCY_CODE = result.PZRASPE.PZRASPE_DNCY_CODE;
                            ppcd.PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                            ppcd.PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                            ppcd.PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;
                            item.PZRASPE = ppcd;

                            //Datos Perfil
                            Domain.Perfil per = new Domain.Perfil();
                            per.PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                            per.PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                            per.PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                            item.PZRASPE.PZVPFLE = per;

                            //Datos Persona
                            Domain.Persona per2 = new Domain.Persona();
                            per2.PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                            per2.PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                            per2.PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                            per2.PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                            per2.PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                            per2.PZBPRSO_ACTIVITY_DATE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVITY_DATE;
                            per2.PZBPRSO_DATA_ORIGIN = result.PZRASPE.PZBPRSO.PZBPRSO_DATA_ORIGIN;
                            item.PZRASPE.PZBPRSO = per2;

                            //Datos Cardep
                            Domain.AsignacionCargoDependencia carDep = new Domain.AsignacionCargoDependencia();
                            //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                            carDep.PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                            carDep.PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                            carDep.PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                            carDep.PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                            carDep.PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                            item.PZRASPE.PZRDYPT = carDep;

                            //Datos Cargo
                            Domain.Cargo car = new Domain.Cargo();
                            //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                            car.PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE;
                            car.PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                            car.PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                            car.PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                            car.PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                            car.PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                            item.PZRASPE.PZRDYPT.PZVPOST = car;

                            //Datos Departamento
                            Domain.Dependencia dep = new Domain.Dependencia();
                            //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                            dep.PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                            dep.PZVDNCY_CODE_SUP = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                            dep.PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                            dep.PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                            dep.PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                            dep.PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                            dep.PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                            item.PZRASPE.PZRDYPT.PZVDNCY = dep;

                        re.Add(item);
                        

                
                        transaction.Commit();
                    }

                }
                return re;
            }
        }

        public List<AsignacionInstrumentoPersona> GetPZRPLAPEvaPenDep(String id) {

            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@" SELECT  *
                                                   FROM                    
                                                        --INSTRUMENTO
                                                        PZRPLAP,
                                                        PZBPOLL,
                                                        PZBAPPL,
                                                        PZBCALR,
                                                        PZVPFLE,
                                                        --EVALUADO
                                                        PZVPOST,
                                                        PZRDYPT,
                                                        PZVDNCY,
                                                        PZRASPE,
                                                        PZBPRSO,
                                                        --EVALUADOR
                                                        PZVPOST POST1,
                                                        PZVDNCY DNCY1,
                                                        PZRDYPT DYPT1,
                                                        PZRASPE ASPE1,
                                                        PZBPRSO PRSO1
                                                   WHERE PZRPLAP.PZRPLAP_STATUS IN ('P','T')     
                                                     and pzvpost.pzvpost_code = pzrdypt.pzrdypt_post_code
                                                     AND pzrdypt.pzrdypt_post_code = pzraspe.pzraspe_post_code
                                                     AND pzvdncy.pzvdncy_code = pzrdypt.pzrdypt_dncy_code
                                                     AND pzrdypt.pzrdypt_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzraspe.pzraspe_pidm = PZBPRSO.PZBPRSO_PIDM
                                                     AND PZVDNCY.PZVDNCY_CODE_sup = DNCY1.PZVDNCY_CODE
                                                     AND PZVPOST.PZVPOST_CODE_sup = POST1.PZVPOST_CODE 
                                                     AND POST1.PZVPOST_CODE = dypt1.pzrdypt_post_code 
                                                     AND dypt1.pzrdypt_dncy_code = DNCY1.PZVDNCY_CODE
                                                     AND aspe1.pzraspe_post_code = dypt1.pzrdypt_post_code
                                                     AND aspe1.pzraspe_dncy_code = dypt1.pzrdypt_dncy_code
                                                     and aspe1.pzraspe_pidm= prso1.pzbprso_pidm
                                                     and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                     AND pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                     and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                     AND pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                     and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                     and pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                     and pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                     and pzbappl.pzbappl_calr_code = pzbcalr.pzbcalr_code
                                                     and pzbappl.pzbappl_calr_code = pzrplap.pzrplap_calr_code
                                                     and pzbappl.pzbappl_pfle_code = pzvpfle.pzvpfle_code
                                                     and pzbappl.pzbappl_pfle_code = pzrplap.pzrplap_pfle_code 
                                                     and prso1.pzbprso_pidm= '"+id+"' ")
                                                    .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                    .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                    .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                    .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                    .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                    .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                    .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                    .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                    .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                    .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();
                    foreach (var result in es)
                    {
                        Domain.AsignacionInstrumentoPersona item = new Domain.AsignacionInstrumentoPersona();
                        item.PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                        item.PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                        item.PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                        item.PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                        item.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                        item.PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                        item.PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                        item.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                        item.PZRPLAP_USER = result.PZRPLAP_USER;
                        item.PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                        item.PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;

                    


                        //DATOS instrumento aplicable
                        Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                        _insApl.PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                        _insApl.PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                        _insApl.PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                        _insApl.PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                        _insApl.PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                        _insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                        _insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                        _insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                        item.PZBAPPL = _insApl;

                        //Datos Instrumento
                        Domain.Instrumento ins = new Domain.Instrumento();
                        ins.PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                        ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                        ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                        item.PZBAPPL.PZBPOLL = ins;



                          re.Add(item);


                    }

                    return (re);
                    transaction.Commit();
                }

            }
            return re;
        }

        public List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String id) {

            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@" SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_appl_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_appl_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm 
                                                         and pzbprso.pzbprso_pidm= '" + id + "' ")

                                                .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZBAREA", typeof(Domain.Area))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();


                    foreach (var result in es)
                    {
                        //Datos ICPD
                        Domain.AsignacionInstrumentoPersona item = new Domain.AsignacionInstrumentoPersona();
                        item.PZRPLAP_CALR_CODE = result.PZRPLAP_CALR_CODE;
                        item.PZRPLAP_POST_CODE = result.PZRPLAP_POST_CODE;
                        item.PZRPLAP_DNCY_CODE = result.PZRPLAP_DNCY_CODE;
                        item.PZRPLAP_PFLE_CODE = result.PZRPLAP_PFLE_CODE;
                        item.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                        item.PZRPLAP_DATA_ORIGIN = result.PZRPLAP_DATA_ORIGIN;
                        item.PZRPLAP_ACTIVITY_DATE = result.PZRPLAP_ACTIVITY_DATE;
                        item.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                        item.PZRPLAP_USER = result.PZRPLAP_USER;
                        item.PZRPLAP_APPL_SEQ_NUMBER = result.PZRPLAP_APPL_SEQ_NUMBER;
                        item.PZRPLAP_SEQ_NUMBER = result.PZRPLAP_SEQ_NUMBER;


                        //Datos Instrumento aplicado
                        Domain.InstrumentoAplicable _insApl = new InstrumentoAplicable();
                        _insApl.PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                        _insApl.PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                        _insApl.PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                        _insApl.PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                        _insApl.PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                        _insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                        _insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                        _insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                        item.PZBAPPL = _insApl;

                        //Datos Instrumento
                        Domain.Instrumento ins = new Domain.Instrumento();
                        //ins.PZBPOLL_ID = result.PZBCALR.PZBPOLL.PZBPOLL_ID;
                        // ins._PZBPOLL_CODE = result.PZBCALR.PZBAPPL.;
                        //ins.PZBPRSO_ID = result.PZVICOD.PZBPOLL.PZBPRSO_ID;
                        ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                        ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                        item.PZBAPPL.PZBPOLL = ins;

                        //Datos Calendario
                        Domain.Calendario cal = new Domain.Calendario();
                        // cal.PZBCALR_ID = result.PZBCALR.PZBCALR_ID;
                        cal.PZBCALR_NAME = result.PZBAPPL.PZBCALR.PZBCALR_NAME;
                        cal.PZBCALR_USER = result.PZBAPPL.PZBCALR.PZBCALR_USER;
                        cal.PZBCALR_TERM = result.PZBAPPL.PZBCALR.PZBCALR_TERM;
                        cal.PZBCALR_END_DATE = result.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                        cal.PZBCALR_INIT_DATE = result.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                        cal.PZBCALR_DATA_ORIGIN = result.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                        cal.PZBCALR_ACTIVITY_DATE = result.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;
                        item.PZBAPPL.PZBCALR = cal;



                        // Datos PER_PER
                        Domain.AsignacionCargoPersona ppcd = new Domain.AsignacionCargoPersona();
                        ppcd.PZRASPE_PIDM = result.PZRASPE.PZRASPE_PIDM;
                        ppcd.PZRASPE_PFLE_CODE = result.PZRASPE.PZRASPE_PFLE_CODE;
                        ppcd.PZRASPE_DNCY_CODE = result.PZRASPE.PZRASPE_DNCY_CODE;
                        //ppcd.PZRASPE_ID = result.PZRASPE.PZRASPE_ID;
                        ppcd.PZRASPE_POST_CODE = result.PZRASPE.PZRASPE_POST_CODE;
                        ppcd.PZRASPE_END_DATE = result.PZRASPE.PZRASPE_END_DATE;
                        ppcd.PZRASPE_START_DATE = result.PZRASPE.PZRASPE_START_DATE;
                        item.PZRASPE = ppcd;

                        //Datos Perfil
                        Domain.Perfil per = new Domain.Perfil();
                        //per.PZVPFLE_ID = result.PZRASPE.PZVPFLE.PZVPFLE_ID;
                        per.PZVPFLE_CODE = result.PZRASPE.PZVPFLE.PZVPFLE_CODE;
                        per.PZVPFLE_NAME = result.PZRASPE.PZVPFLE.PZVPFLE_NAME;
                        per.PZVPFLE_ACTIVITY_DATE = result.PZRASPE.PZVPFLE.PZVPFLE_ACTIVITY_DATE;
                        item.PZRASPE.PZVPFLE = per;

                        //Datos Persona
                        Domain.Persona per2 = new Domain.Persona();
                        // per2.PZBPRSO_ID = result.PZRASPE.PZBPRSO.PZBPRSO_ID;
                        per2.PZBPRSO_PIDM = result.PZRASPE.PZBPRSO.PZBPRSO_PIDM;
                        per2.PZBPRSO_SITE = result.PZRASPE.PZBPRSO.PZBPRSO_SITE;
                        //per2.PZBPRSO_NAME = result.PZRASPE.PZBPRSO.PZBPRSO_NAME;
                        per2.PZBPRSO_USER = result.PZRASPE.PZBPRSO.PZBPRSO_USER;
                        per2.PZBPRSO_ACTIVE = result.PZRASPE.PZBPRSO.PZBPRSO_ACTIVE;
                        per2.PZBPRSO_PAYSHEET = result.PZRASPE.PZBPRSO.PZBPRSO_PAYSHEET;
                        //per2.PZBPRSO_PZVPOST = result.PZRASPE.PZBPRSO.PZBPRSO_PZCRCOD;
                        item.PZRASPE.PZBPRSO = per2;

                        //Datos Cardep
                        Domain.AsignacionCargoDependencia carDep = new Domain.AsignacionCargoDependencia();
                        //carDep.PZRDYPT_ID = result.PZRASPE.PZRDYPT.PZRDYPT_ID;
                        carDep.PZRDYPT_DNCY_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_DNCY_CODE;
                        carDep.PZRDYPT_POST_CODE = result.PZRASPE.PZRDYPT.PZRDYPT_POST_CODE;
                        carDep.PZRDYPT_USER = result.PZRASPE.PZRDYPT.PZRDYPT_USER;
                        carDep.PZRDYPT_DATA_ORIGIN = result.PZRASPE.PZRDYPT.PZRDYPT_DATA_ORIGIN;
                        carDep.PZRDYPT_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZRDYPT_ACTIVITY_DATE;
                        item.PZRASPE.PZRDYPT = carDep;

                        //Datos Cargo
                        Domain.Cargo car = new Domain.Cargo();
                        //car.PZVPOST_ID = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ID;
                        car.PZVPOST_CODE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE;
                        car.PZVPOST_NAME = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_NAME;
                        car.PZVPOST_CODE_SUP = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_CODE_SUP;
                        car.PZVPOST_USER = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_USER;
                        car.PZVPOST_DESCRIPTION = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_DESCRIPTION;
                        car.PZVPOST_ACTIVITY_DATE = result.PZRASPE.PZRDYPT.PZVPOST.PZVPOST_ACTIVITY_DATE;
                        item.PZRASPE.PZRDYPT.PZVPOST = car;

                        //Datos Departamento
                        Domain.Dependencia dep = new Domain.Dependencia();
                        //dep.PZVDNCY_ID = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_ID;
                        dep.PZVDNCY_CODE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE;
                        dep.PZVDNCY_CODE_SUP = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_CODE_SUP;
                        dep.PZVDNCY_KEY = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_KEY;
                        dep.PZVDNCY_SITE = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_SITE;
                        dep.PZVDNCY_LEVEL = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_LEVEL;
                        dep.PZVDNCY_NAME = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_NAME;
                        dep.PZVDNCY_USER = result.PZRASPE.PZRDYPT.PZVDNCY.PZVDNCY_USER;
                        item.PZRASPE.PZRDYPT.PZVDNCY = dep;

                        return (re);
                        transaction.Commit();
                    }

                }
                return re;
            }
        }
        public List<AsignacionInstrumentoPersona> GetPZRPLAPIdC(String id, String idi)
        {


            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@" SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_appl_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_appl_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         and pzbprso.pzbprso_pidm='" + id + "' and PZBPOLL.PZBPOLL_code='" + idi + "'")

                                                .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZBAREA", typeof(Domain.Area))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();
                    foreach (var result in es)
                    {
                        Domain.AsignacionInstrumentoPersona i_p = new Domain.AsignacionInstrumentoPersona();

                        i_p.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                        i_p.PZRPLAP_STATUS = result.PZRPLAP_STATUS;


                        Domain.AsignacionInstrumentoPregunta i_c = new Domain.AsignacionInstrumentoPregunta();
                        
                        

                        //Datos de I_c
                        foreach (var r in result.PZBAPPL.PZBPLQS)
                        {



                            i_c.PZBPLQS_SEQ_NUMBER = r.PZBPLQS_SEQ_NUMBER;
                            i_c.PZBPLQS_APPL_SEQ_NUMBER = r.PZBPLQS_APPL_SEQ_NUMBER;
                            i_c.PZBPLQS_USER = r.PZBPLQS_USER;
                            i_c.PZBPLQS_DATA_ORIGIN = r.PZBPLQS_DATA_ORIGIN;
                            i_c.PZBPLQS_ACTIVITY_DATE = r.PZBPLQS_ACTIVITY_DATE;
                            i_c.PZBPLQS_QSTN_CODE = r.PZBPLQS_QSTN_CODE;
                            i_c.PZBPLQS_POLL_CODE = r.PZBPLQS_POLL_CODE;
                            i_c.PZBPLQS_AREA_CODE = r.PZBPLQS_AREA_CODE;
                            i_c.PZBPLQS_CRIT_CODE = r.PZBPLQS_CRIT_CODE;
                            i_c.PZBPLQS_MAX_OVAL = r.PZBPLQS_MAX_OVAL;
                            i_c.PZBPLQS_MAX_VALUE = r.PZBPLQS_MAX_VALUE;
                            i_c.PZBPLQS_MIN_OVAL = r.PZBPLQS_MIN_OVAL;
                            i_c.PZBPLQS_MIN_VALUE = r.PZBPLQS_MIN_VALUE;
                            i_c.PZBPLQS_ORDER = r.PZBPLQS_ORDER;
                            i_c.PZBPLQS_STATUS = r.PZBPLQS_STATUS;
                            

                            //Datos de la pregunta
                            Domain.Pregunta pre = new Domain.Pregunta();
                            pre.PZBQSTN_CODE = r.PZBQSTN.PZBQSTN_CODE;
                            pre.PZBQSTN_USER = r.PZBQSTN.PZBQSTN_USER;
                            pre.PZBQSTN_DESCRIPTION = r.PZBQSTN.PZBQSTN_DESCRIPTION;
                            pre.PZBQSTN_ACTIVITY_DATE = r.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                            pre.PZBQSTN_DATA_ORIGIN = r.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                            pre.PZBQSTN_NAME = r.PZBQSTN.PZBQSTN_NAME;
                            i_c.PZBQSTN = pre;



                            //DATOS DEL criterio de Evaluacion
                            Domain.Criterio _cri = new Domain.Criterio();
                            _cri.PZBCRIT_CODE = r.PZBCRIT.PZBCRIT_CODE;
                            _cri.PZBCRIT_USER = r.PZBCRIT.PZBCRIT_USER;
                            _cri.PZBCRIT_DESCRIPTION = r.PZBCRIT.PZBCRIT_DESCRIPTION;
                            _cri.PZBCRIT_DATA_ORIGIN = r.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                            _cri.PZBCRIT_ACTIVITY_DATE = r.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                            i_c.PZBCRIT = _cri;

                            //Datos del area a la cual pertenece la pregunta
                            Domain.Area _area = new Domain.Area();
                            _area.PZBAREA_CODE = r.PZBAREA.PZBAREA_CODE;
                            _area.PZBAREA_USER = r.PZBAREA.PZBAREA_USER;
                            _area.PZBAREA_DESCRIPTION = r.PZBAREA.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_DATA_ORIGIN = r.PZBAREA.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = r.PZBAREA.PZBAREA_ACTIVITY_DATE;
                            i_c.PZBAREA = _area;

                        }



                        //DATOS instrumento aplicable
                        Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                        _insApl.PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                        _insApl.PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                        _insApl.PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                        _insApl.PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                        _insApl.PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                        _insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                        _insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                        _insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                        i_c.PZBAPPL = _insApl;

                        //Datos Instrumento
                        Domain.Instrumento ins = new Domain.Instrumento();
                        ins.PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                        ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                        ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                        i_c.PZBAPPL.PZBPOLL = ins;
                        i_p.PZBAPPL.PZBPLQS.Add(i_c);

                       re.Add(i_p);


                    }

                    return (re);
                    transaction.Commit();
                }

            }
            return re;

        }

        public List<AsignacionInstrumentoPersona> GetPZRPLAPIdI(String ide, String idi) {


            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {



                    var es = session.
                        CreateSQLQuery(@"  SELECT *
                                                 FROM --INSTRUMETNO   
                                                      PZBPOLL,pzbappl,
                                                      pzbplqs,pzbqstn,
                                                      pzrplap,PZBCALR,
                                                      PZRASPE,PZBPRSO,
                                                      PZVPFLE,pzrdypt,
                                                      pzvpost,pzvdncy,
                                                      pzbarea,pzbcrit

                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_appl_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_appl_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         PZBPOLL.PZBPOLL_code='" + idi + "' and " +
                                                         "pzbprso.pzbprso_pidm='" + ide + "' ")

                                                .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZBAREA", typeof(Domain.Area))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<AsignacionInstrumentoPersona>();
                    foreach (var result in es)
                    {
                        Domain.AsignacionInstrumentoPersona i_p = new Domain.AsignacionInstrumentoPersona();

                        i_p.PZRPLAP_PIDM = result.PZRPLAP_PIDM;
                        i_p.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                        Domain.AsignacionInstrumentoPregunta i_c = new Domain.AsignacionInstrumentoPregunta();
                        //Datos de I_c
                        foreach (var r in result.PZBAPPL.PZBPLQS)
                        {

                            //Datos de I_c


                            i_c.PZBPLQS_SEQ_NUMBER = r.PZBPLQS_SEQ_NUMBER;
                            i_c.PZBPLQS_APPL_SEQ_NUMBER = r.PZBPLQS_APPL_SEQ_NUMBER;
                            i_c.PZBPLQS_USER = r.PZBPLQS_USER;
                            i_c.PZBPLQS_DATA_ORIGIN = r.PZBPLQS_DATA_ORIGIN;
                            i_c.PZBPLQS_ACTIVITY_DATE = r.PZBPLQS_ACTIVITY_DATE;
                            i_c.PZBPLQS_QSTN_CODE = r.PZBPLQS_QSTN_CODE;
                            i_c.PZBPLQS_POLL_CODE = r.PZBPLQS_POLL_CODE;
                            i_c.PZBPLQS_AREA_CODE = r.PZBPLQS_AREA_CODE;
                            i_c.PZBPLQS_CRIT_CODE = r.PZBPLQS_CRIT_CODE;
                            i_c.PZBPLQS_MAX_OVAL = r.PZBPLQS_MAX_OVAL;
                            i_c.PZBPLQS_MAX_VALUE = r.PZBPLQS_MAX_VALUE;
                            i_c.PZBPLQS_MIN_OVAL = r.PZBPLQS_MIN_OVAL;
                            i_c.PZBPLQS_MIN_VALUE = r.PZBPLQS_MIN_VALUE;
                            i_c.PZBPLQS_ORDER = r.PZBPLQS_ORDER;
                            i_c.PZBPLQS_STATUS = r.PZBPLQS_STATUS;


                            //Datos de la pregunta
                            Domain.Pregunta pre = new Domain.Pregunta();
                            pre.PZBQSTN_CODE = r.PZBQSTN.PZBQSTN_CODE;
                            pre.PZBQSTN_USER = r.PZBQSTN.PZBQSTN_USER;
                            pre.PZBQSTN_DESCRIPTION = r.PZBQSTN.PZBQSTN_DESCRIPTION;
                            pre.PZBQSTN_ACTIVITY_DATE = r.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                            pre.PZBQSTN_DATA_ORIGIN = r.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                            pre.PZBQSTN_NAME = r.PZBQSTN.PZBQSTN_NAME;
                            i_c.PZBQSTN = pre;


                            //DATOS DEL criterio de Evaluacion
                            Domain.Criterio _cri = new Domain.Criterio();
                            _cri.PZBCRIT_CODE = r.PZBCRIT.PZBCRIT_CODE;
                            _cri.PZBCRIT_USER = r.PZBCRIT.PZBCRIT_USER;
                            _cri.PZBCRIT_DESCRIPTION = r.PZBCRIT.PZBCRIT_DESCRIPTION;
                            _cri.PZBCRIT_DATA_ORIGIN = r.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                            _cri.PZBCRIT_ACTIVITY_DATE = r.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                            i_c.PZBCRIT = _cri;

                            //Datos del area a la cual pertenece la pregunta
                            Domain.Area _area = new Domain.Area();
                            _area.PZBAREA_CODE = r.PZBAREA.PZBAREA_CODE;
                            _area.PZBAREA_USER = r.PZBAREA.PZBAREA_USER;
                            _area.PZBAREA_DESCRIPTION = r.PZBAREA.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_DATA_ORIGIN = r.PZBAREA.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = r.PZBAREA.PZBAREA_ACTIVITY_DATE;
                            i_c.PZBAREA = _area;


                            //DATOS instrumento aplicable
                            Domain.InstrumentoAplicable __insApl = new Domain.InstrumentoAplicable();
                            __insApl.PZBAPPL_CALR_CODE = r.PZBAPPL.PZBAPPL_CALR_CODE;
                            __insApl.PZBAPPL_PFLE_CODE = r.PZBAPPL.PZBAPPL_PFLE_CODE;
                            __insApl.PZBAPPL_PRCNT = r.PZBAPPL.PZBAPPL_PRCNT;
                            __insApl.PZBAPPL_POLL_CODE = r.PZBAPPL.PZBAPPL_POLL_CODE;
                            __insApl.PZBAPPL_SEQ_NUMBER = r.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                            __insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                            __insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                            __insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                            i_c.PZBAPPL = __insApl;

                            //Datos Instrumento
                            Domain.Instrumento _ins = new Domain.Instrumento();
                            _ins.PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                            _ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                            _ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                            _ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                            _ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                            i_c.PZBAPPL.PZBPOLL = _ins;
                            i_p.PZBAPPL.PZBPLQS.Add(i_c);



                            re.Add(i_p);


                        }



                        //DATOS instrumento aplicable
                        Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                        _insApl.PZBAPPL_CALR_CODE = result.PZBAPPL.PZBAPPL_CALR_CODE;
                        _insApl.PZBAPPL_PFLE_CODE = result.PZBAPPL.PZBAPPL_PFLE_CODE;
                        _insApl.PZBAPPL_PRCNT = result.PZBAPPL.PZBAPPL_PRCNT;
                        _insApl.PZBAPPL_POLL_CODE = result.PZBAPPL.PZBAPPL_POLL_CODE;
                        _insApl.PZBAPPL_SEQ_NUMBER = result.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                        _insApl.PZBAPPL_USER = result.PZBAPPL.PZBAPPL_USER;
                        _insApl.PZBAPPL_DATA_ORIGIN = result.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                        _insApl.PZBAPPL_ACTIVITY_DATE = result.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                        i_p.PZBAPPL = _insApl;

                        //Datos Instrumento
                        Domain.Instrumento ins = new Domain.Instrumento();
                        ins.PZBPOLL_CODE = result.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                        ins.PZBPOLL_NAME = result.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                        ins.PZBPOLL_USER = result.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = result.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = result.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                        i_p.PZBAPPL.PZBPOLL = ins;



                     re.Add(i_p);


                    }

                    return (re);
                    transaction.Commit();
                }

            }
            return re;

        }




        public AsignacionInstrumentoPersona GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public AsignacionInstrumentoPersona GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(AsignacionInstrumentoPersona calendario)
        {
            throw new NotImplementedException();
        }

        public void Update(AsignacionInstrumentoPersona calendario)
        {
            throw new NotImplementedException();
        }

        public AsignacionInstrumentoPersona Add(AsignacionInstrumentoPersona cargo)
        {
            throw new NotImplementedException();
        }

    
    }
}



