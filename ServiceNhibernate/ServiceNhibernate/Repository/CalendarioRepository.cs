﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class CalendarioRepository : ICalendarioRepository
    {
        public Calendario Add(Calendario calendario)
        {
            throw new NotImplementedException();
        }

        public Calendario GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public Calendario GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public List<Calendario> GetPZBCALR()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                List<Domain.Calendario> result = new List<Domain.Calendario>();

                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var res = session.
                         CreateSQLQuery(@"Select * from PZBCALR")
                         .AddEntity("PZBCALR", typeof(Domain.Calendario))
                         .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                         .List<Calendario>();

                        foreach (var X in res)
                        {

                            //Datos del Calendario
                            Domain.Calendario cal = new Domain.Calendario();
                            //  cal.PZBCALR_ID = X.PZBCALR_ID;
                            cal.PZBCALR_CODE = X.PZBCALR_CODE;
                            cal.PZBCALR_NAME = X.PZBCALR_NAME;
                            cal.PZBCALR_USER = X.PZBCALR_USER;
                            cal.PZBCALR_TERM = X.PZBCALR_TERM;
                            cal.PZBCALR_SRL = X.PZBCALR_SRL;
                            cal.PZBCALR_END_DATE = X.PZBCALR_END_DATE;
                            cal.PZBCALR_INIT_DATE = X.PZBCALR_INIT_DATE;
                            cal.PZBCALR_DATA_ORIGIN = X.PZBCALR_DATA_ORIGIN;
                            cal.PZBCALR_ACTIVITY_DATE = X.PZBCALR_ACTIVITY_DATE;
                            //cal._PZBCALR_POLL_CODE = X.PZBCALR_POLL_CODE;
                            result.Add(cal);

                        }

                    }
                    catch (Exception e)
                    {

                    }

                    return (result);

                }
            }
        }

        public void Remove(Calendario calendario)
        {
            throw new NotImplementedException();
        }

        public void Update(Calendario calendario)
        {
            throw new NotImplementedException();
        }
    }
}