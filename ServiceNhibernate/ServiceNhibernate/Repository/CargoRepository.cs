﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    class CargoRepository : ICargoRepository
    {
        public Cargo Add(Cargo cargo)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var respuesta = session.
                        CreateSQLQuery(@"Select * from pzvpost where pzvpost_code='RECTOR'")
                        .SetResultTransformer(Transformers.AliasToBean(typeof(Cargo)))
                        .List<Cargo>();

                    foreach (var result in respuesta)
                    {

                        Domain.Cargo item = new Domain.Cargo();

                   //     item.PZVPOST_ID = result.PZVPOST_ID;
                        item.PZVPOST_CODE = result.PZVPOST_CODE;
                        item.PZVPOST_NAME = result.PZVPOST_NAME;
                        item.PZVPOST_CODE_SUP = result.PZVPOST_CODE_SUP;
                        item.PZVPOST_USER = result.PZVPOST_USER;
                        item.PZVPOST_DESCRIPTION = result.PZVPOST_DESCRIPTION;
                        item.PZVPOST_ACTIVITY_DATE = result.PZVPOST_ACTIVITY_DATE;
                        item.PZVPOST_DATA_ORIGIN = result.PZVPOST_DATA_ORIGIN;



                        return item;
                        //   session.Save();
                        transaction.Commit();

                    }
                }

            }
            return cargo;
        }
        public void Update(Cargo product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.
               BeginTransaction())
                {
                    session.Update(product);
                    transaction.Commit();
                }
            }
        }
        public void Remove(Cargo product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.
               BeginTransaction())
                {
                    session.Delete(product);
                    transaction.Commit();
                }
            }
        }
        public Cargo GetById(decimal productId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return session.Get<Cargo>(productId);
            }
        }
        public Cargo GetByName(string name)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                Cargo product = session
                .CreateCriteria(typeof(Cargo))
                .Add(Restrictions.Eq("PZVPOST_NOMBRE", name))
                                        .UniqueResult<Cargo>();
                return product;
            }
        }
        public System.Collections.Generic.ICollection<Cargo>
       GetByCategory(string description)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var products = session
                .CreateCriteria(typeof(Cargo))
            .Add(Restrictions.Eq("PZVPOST_DESCRIPTION", description))
            .List<Cargo>();
                return products;
            }
        }

    }
}