﻿/*  [HttpGet]
        [ResponseType(typeof(List<Models.Respuesta>))]
        [Route("api/PZRAWAP/GetPZRAWAP/{id}/{idins}/{idcal}/{_plap}")]
        public IHttpActionResult GetPZRAWAP(int id, string idins, string idcal, string _plap)
        {
            List<Models.Respuesta> re = new List<Models.Respuesta>();
            try
            {

                // db.PZRAWAP.Include(tr => tr.PZBPLQS);




                db.PZRAWAP.Include(appl => appl.PZBPLQS.PZBAPPL)
                          .Include(poll => poll.PZBPLQS.PZBAPPL.PZBPOLL)
                          .Include(area => area.PZBPLQS.PZBAREA)
                          .Include(crit => crit.PZBPLQS.PZBCRIT)
                          .Include(qstn => qstn.PZBPLQS.PZBQSTN)
                          .Include(tyaw => tyaw.PZBPLQS.PZVTYAW)
                          .Include(bpls => bpls.PZBPLQS);

                var es = db.PZRAWAP.SqlQuery(@"SELECT * FROM PZBPOLL,pzbappl,pzbplqs,pzbqstn,
                                                             pzrplap,PZBCALR,PZRASPE,PZBPRSO,
                                                             PZVPFLE,pzrdypt,pzvpost,pzvdncy,
                                                             pzbarea,pzbcrit,pzrawap
                                                 WHERE  PZBPOLL.PZBPOLL_CODE = pzbappl.pzbappl_poll_code
                                                         AND pzbappl.pzbappl_poll_code = pzbplqs.pzbplqs_poll_code
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzbappl.pzbappl_seq_number = pzbplqs.pzbplqs_appl_seq_number
                                                         and pzbappl.pzbappl_seq_number = pzrplap.pzrplap_appl_seq_number  
                                                         and pzbcalr.pzbcalr_code = pzbappl.pzbappl_calr_code
                                                         and pzbappl.pzbappl_calr_code = pzbplqs.pzbplqs_calr_code
                                                         and pzvpfle.pzvpfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzbqstn.pzbqstn_code = pzbplqs.pzbplqs_qstn_code
                                                         and pzbcrit.pzbcrit_code = pzbplqs.pzbplqs_crit_code
                                                         and pzbarea.pzbarea_code = pzbplqs.pzbplqs_area_code
                                                         and pzbplqs.pzbplqs_seq_number = pzrawap.pzrawap_plqs_seq_number
                                                         and pzrplap.pzrplap_seq_number = pzrawap.pzrawap_plap_seq_number
                                                         and pzrplap.pzrplap_poll_code = pzbappl.pzbappl_poll_code
                                                         and pzrplap.pzrplap_calr_code = pzbappl.pzbappl_calr_code
                                                         and pzrplap.pzrplap_pfle_code = pzbappl.pzbappl_pfle_code
                                                         and pzrplap.pzrplap_pfle_code = pzraspe.pzraspe_pfle_code
                                                         and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code
                                                         and pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                         and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                         and pzrdypt.pzrdypt_post_code =  pzvpost.pzvpost_code
                                                         and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code
                                                         and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                         and pzrdypt.pzrdypt_dncy_code =  pzvdncy.pzvdncy_code
                                                         and pzrplap.pzrplap_pidm = pzraspe.pzraspe_pidm
                                                         and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm
                                                         AND PZBPOLL.PZBPOLL_code='" + idins + "' " +
                                                         "and pzbprso.pzbprso_pidm = '" + id + "' " +
                                                         "and pzbcalr.pzbcalr_code ='" + idcal + "' " +
                                                         " ORDER BY PZBPLQS.PZBPLQS_ORDER , pzbplqs.pzbplqs_area_code "
                                                         ).ToList();



                foreach (var x in es)
                {
                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = (int)x.PZRAWAP_SEQ_NUMBER;
                    resp._PZRAWAP_ACTIVITY_DATE = x.PZRAWAP_ACTIVITY_DATE;
                    resp._PZRAWAP_DESCRIPTION = x.PZRAWAP_DESCRIPTION;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = x.PZRAWAP_PLAP_SEQ_NUMBER;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = x.PZRAWAP_PLQS_SEQ_NUMBER;
                    resp._PZRAWAP_PERC_IMP = x.PZRAWAP_PERC_IMP;
                    resp._PZRAWAP_USER = x.PZRAWAP_USER;
                    resp._PZRAWAP_VALUE = x.PZRAWAP_VALUE;
                    resp._PZRAWAP_DATA_ORIGIN = x.PZRAWAP_DATA_ORIGIN;



                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();
                    _insPre._PZBPLQS_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x.PZBPLQS.PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x.PZBPLQS.PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x.PZBPLQS.PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x.PZBPLQS.PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x.PZBPLQS.PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x.PZBPLQS.PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x.PZBPLQS.PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x.PZBPLQS.PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x.PZBPLQS.PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x.PZBPLQS.PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x.PZBPLQS.PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x.PZBPLQS.PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x.PZBPLQS.PZBPLQS_STATUS;


                    Models.InstrumentoAplicable apl = new Models.InstrumentoAplicable();
                    apl._PZBAPPL_USER = x.PZBPLQS.PZBAPPL.PZBAPPL_USER;
                    apl._PZBAPPL_PRCNT = x.PZBPLQS.PZBAPPL.PZBAPPL_PRCNT;
                    apl._PZBAPPL_SEQ_NUMBER = x.PZBPLQS.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                    apl._PZBAPPL_POLL_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_POLL_CODE;
                    apl._PZBAPPL_PFLE_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_PFLE_CODE;
                    apl._PZBAPPL_CALR_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_CALR_CODE;
                    _insPre._PZBAPPL = apl;

                    Models.Instrumento ins = new Models.Instrumento();
                    ins._PZBPOLL_NAME = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                    ins._PZBPOLL_CODE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                    ins._PZBPOLL_USER = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                    ins._PZBPOLL_DATA_ORIGIN = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                    ins._PZBPOLL_ACTIVITY_DATE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                    _insPre._PZBAPPL._PZBPOLL = ins;

                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = x.PZBPLQS.PZBCRIT.PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = x.PZBPLQS.PZBCRIT.PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = x.PZBPLQS.PZBCRIT.PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = x.PZBPLQS.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = x.PZBPLQS.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                    _insPre._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x.PZBPLQS.PZBAREA.PZBAREA_CODE;
                    _area._PZBAREA_USER = x.PZBPLQS.PZBAREA.PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = x.PZBPLQS.PZBAREA.PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = x.PZBPLQS.PZBAREA.PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = x.PZBPLQS.PZBAREA.PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;

                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = x.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                    item._PZBQSTN_NAME = x.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                    item._PZBQSTN_USER = x.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = x.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = x.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = x.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                    _insPre._PZBQSTN = item;

                    resp._PZBPLQS = _insPre;

                
                    re.Add(resp);

                }


               
          

            }
            catch (Exception e)
            {

                return Ok("Respuestas no encontradas");

            }

            //Consulta las preguntas que no tienen respuesta
            List<Models.AsignacionInstrumentoPregunta> obj = _f.NoAnswerQstn(id, idins, idcal,_plap);

            int lon = obj.Count();

            if (lon > 0)
            {
                foreach (var x in obj)
                {

                    Models.Respuesta resp = new Models.Respuesta();
                    resp._PZRAWAP_SEQ_NUMBER = 0;
                    resp._PZRAWAP_ACTIVITY_DATE = DateTime.Today;
                    resp._PZRAWAP_DESCRIPTION = null;
                    resp._PZRAWAP_PLAP_SEQ_NUMBER = 0;
                    resp._PZRAWAP_PLQS_SEQ_NUMBER = 0;
                    resp._PZRAWAP_PERC_IMP = 0;
                    resp._PZRAWAP_USER = null;
                    resp._PZRAWAP_VALUE = 0;
                    resp._PZRAWAP_DATA_ORIGIN = null;

                    Models.AsignacionInstrumentoPregunta _insPre = new Models.AsignacionInstrumentoPregunta();

                    _insPre._PZBPLQS_SEQ_NUMBER = x._PZBPLQS_SEQ_NUMBER;
                    _insPre._PZBPLQS_USER = x._PZBPLQS_USER;
                    _insPre._PZBPLQS_DATA_ORIGIN = x._PZBPLQS_DATA_ORIGIN;
                    _insPre._PZBPLQS_ACTIVITY_DATE = x._PZBPLQS_ACTIVITY_DATE;
                    _insPre._PZBPLQS_QSTN_CODE = x._PZBPLQS_QSTN_CODE;
                    _insPre._PZBPLQS_POLL_CODE = x._PZBPLQS_POLL_CODE;
                    _insPre._PZBPLQS_CALR_CODE = x._PZBPLQS_CALR_CODE;
                    _insPre._PZBPLQS_APPL_SEQ_NUMBER = x._PZBPLQS_APPL_SEQ_NUMBER;
                    _insPre._PZBPLQS_AREA_CODE = x._PZBPLQS_AREA_CODE;
                    _insPre._PZBPLQS_CRIT_CODE = x._PZBPLQS_CRIT_CODE;
                    _insPre._PZBPLQS_TYAW_CODE = x._PZBPLQS_TYAW_CODE;
                    _insPre._PZBPLQS_MAX_OVAL = x._PZBPLQS_MAX_OVAL;
                    _insPre._PZBPLQS_MAX_VALUE = x._PZBPLQS_MAX_VALUE;
                    _insPre._PZBPLQS_MIN_OVAL = x._PZBPLQS_MIN_OVAL;
                    _insPre._PZBPLQS_MIN_VALUE = x._PZBPLQS_MIN_VALUE;
                    _insPre._PZBPLQS_ORDER = x._PZBPLQS_ORDER;
                    _insPre._PZBPLQS_STATUS = x._PZBPLQS_STATUS;
                    resp._PZBPLQS = _insPre;

                    //DATOS DEL criterio de Evaluacion
                    Models.Criterio _cri = new Models.Criterio();
                    _cri._PZBCRIT_CODE = x._PZBCRIT._PZBCRIT_CODE;
                    _cri._PZBCRIT_USER = x._PZBCRIT._PZBCRIT_USER;
                    _cri._PZBCRIT_DESCRIPTION = x._PZBCRIT._PZBCRIT_DESCRIPTION;
                    _cri._PZBCRIT_DATA_ORIGIN = x._PZBCRIT._PZBCRIT_DATA_ORIGIN;
                    _cri._PZBCRIT_ACTIVITY_DATE = x._PZBCRIT._PZBCRIT_ACTIVITY_DATE;
                    _insPre._PZBCRIT = _cri;
                    resp._PZBPLQS._PZBCRIT = _cri;

                    //Datos del area a la cual pertenece la pregunta
                    Models.Area _area = new Models.Area();
                    _area._PZBAREA_CODE = x._PZBAREA._PZBAREA_CODE;
                    _area._PZBAREA_USER = x._PZBAREA._PZBAREA_USER;
                    _area._PZBAREA_DESCRIPTION = x._PZBAREA._PZBAREA_DESCRIPTION;
                    _area._PZBAREA_DATA_ORIGIN = x._PZBAREA._PZBAREA_DATA_ORIGIN;
                    _area._PZBAREA_ACTIVITY_DATE = x._PZBAREA._PZBAREA_ACTIVITY_DATE;
                    _insPre._PZBAREA = _area;
                    resp._PZBPLQS._PZBAREA = _area;

                    Models.Pregunta item = new Models.Pregunta();

                    item._PZBQSTN_CODE = x._PZBQSTN._PZBQSTN_CODE;
                    item._PZBQSTN_NAME = x._PZBQSTN._PZBQSTN_NAME;
                    item._PZBQSTN_USER = x._PZBQSTN._PZBQSTN_USER;
                    item._PZBQSTN_DESCRIPTION = x._PZBQSTN._PZBQSTN_DESCRIPTION;
                    item._PZBQSTN_DATA_ORIGIN = x._PZBQSTN._PZBQSTN_DATA_ORIGIN;
                    item._PZBQSTN_ACTIVITY_DATE = x._PZBQSTN._PZBQSTN_ACTIVITY_DATE;
                    _insPre._PZBQSTN = item;
                    resp._PZBPLQS._PZBQSTN = item;

                    re.Add(resp);

                }
            }

            return Ok(re);


        }
*/