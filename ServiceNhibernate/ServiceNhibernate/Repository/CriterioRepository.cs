﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class CriterioRepository : ICriterioRepository
    {

        public List<Criterio> GetPZBCRIT(){

            using (ISession session = NHibernateHelper.OpenSession())
            {
                List<Domain.Criterio> result = new List<Domain.Criterio>();

                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var cri = session.
                         CreateSQLQuery(@"Select * from PZBCRIT")
                         .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                         .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                         .List<Criterio>();


                        foreach (var x in cri)
                        {

                            Domain.Criterio res = new Domain.Criterio();
                            res.PZBCRIT_CODE = x.PZBCRIT_CODE;
                            res.PZBCRIT_USER = x.PZBCRIT_USER;
                            res.PZBCRIT_DESCRIPTION = x.PZBCRIT_DESCRIPTION;
                            res.PZBCRIT_DATA_ORIGIN = x.PZBCRIT_DATA_ORIGIN;
                            res.PZBCRIT_ACTIVITY_DATE = x.PZBCRIT_ACTIVITY_DATE;
                            result.Add(res);

                        }
                        return result;

                    }

                    catch (Exception e) {
                        
                    }

                }
                return result;
            }

        }

        public Criterio Add(Criterio cargo)
        {
            throw new NotImplementedException();
        }

        public Criterio GetById(decimal cargo)
        {
            throw new NotImplementedException();
        }

        public Criterio GetByName(string cargo)
        {
            throw new NotImplementedException();
        }


        public void Remove(Criterio cargo)
        {
            throw new NotImplementedException();
        }

        public void Update(Criterio cargo)
        {
            throw new NotImplementedException();
        }
    }
}