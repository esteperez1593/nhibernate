﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    class DependenciaRepository : IDependenciaRepository
    {
        public Dependencia Add()
        {
            
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var respuesta = session.
                        CreateSQLQuery(@"Select * from PZVDNCY ")
                        .SetResultTransformer(Transformers.AliasToBean(typeof(Dependencia)))
                        .List<Dependencia>();

                    foreach (var result in respuesta)
                    {

                        Domain.Dependencia item = new Domain.Dependencia();

                        //     item.PZVDNCY_ID = result.PZVDNCY_ID;
                        item.PZVDNCY_CODE = result.PZVDNCY_CODE;
                        item.PZVDNCY_NAME = result.PZVDNCY_NAME;
                        item.PZVDNCY_CODE_SUP = result.PZVDNCY_CODE_SUP;
                        item.PZVDNCY_USER = result.PZVDNCY_USER;
                        item.PZVDNCY_ACTIVITY_DATE = result.PZVDNCY_ACTIVITY_DATE;
                        item.PZVDNCY_DATA_ORIGIN = result.PZVDNCY_DATA_ORIGIN;



                        return item;
                        //   session.Save();
                        transaction.Commit();

                    }
                }

            }
            return null;
        }
        public void Update(Dependencia product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.
               BeginTransaction())
                {
                    session.Update(product);
                    transaction.Commit();
                }
            }
        }
        public void Remove(Dependencia product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.
               BeginTransaction())
                {
                    session.Delete(product);
                    transaction.Commit();
                }
            }
        }
        public Dependencia GetById(decimal productId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return session.Get<Dependencia>(productId);
            }
        }
        public Dependencia GetByName(string name)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                Dependencia product = session
                .CreateCriteria(typeof(Dependencia))
                .Add(Restrictions.Eq("PZVDNCY_NOMBRE", name))
                                        .UniqueResult<Dependencia>();
                return product;
            }
        }
        public System.Collections.Generic.ICollection<Dependencia>
       GetByCategory(string description)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var products = session
                .CreateCriteria(typeof(Dependencia))
            .Add(Restrictions.Eq("PZVDNCY_DESCRIPTION", description))
            .List<Dependencia>();
                return products;
            }
        }


    }
}