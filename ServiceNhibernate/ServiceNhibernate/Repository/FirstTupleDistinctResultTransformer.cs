﻿using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    public class FirstTupleDistinctResultTransformer : DistinctRootEntityResultTransformer
    {
        public new object TransformTuple(object[] tuple, string[] aliases)
        {
            return tuple[0];
        }
    }
}