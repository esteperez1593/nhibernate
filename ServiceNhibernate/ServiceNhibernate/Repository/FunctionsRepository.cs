﻿using NHibernate;
using NHibernate.Transform;
using ServiceNhibernate.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceNhibernate.Repository
{
    public class FunctionsRepository
    {


        /// <summary>
        ///  
        /// Calcula el indice de desempeño para un usuario en funcion del 
        /// valor porcentual que representa para el perfil que tiene asignado
        ///  
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <param name="_term"></param>
        /// <returns></returns>

        public decimal GetIndPerc(string _perfil, string _calendario, string _instrumento, string _pidm, string _term)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {




                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_COMP('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _term + "') FROM DUAL").ToString();


                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }

        }

        /// <summary>
        /// 
        /// Obtiene el porcentaje asignado a un instrumento para un
        /// oerfil dado
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_term"></param>
        /// <returns></returns>

        public decimal GetPercProf(string _perfil, string _calendario, string _instrumento, string _term)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {




                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_PERC_EVAL('" + _perfil + "','" + _term + "','" + _instrumento + "','" + _calendario + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return 0;

                    }

                }
            }

        }

        public decimal GetPercPoll(String profile_code,
                                     String term_code,
                                     String poll_code,
                                     String calr_code,
                                     String pidm)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_PERC_POLL ('" + profile_code + "','" + term_code + "','" + poll_code + "','" + calr_code + "','" + pidm + "') from dual").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return 0;

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Obtiene la cantidad de preguntas a totalizar
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetCountItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {


                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_COUNT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return 0;

                    }

                }
            }

        }

        /// <summary>
        ///
        /// Obtiene el valor correspondiente al diferencial 
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetDifItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_DIF('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Obtiene la media de las respuestas de un Instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetMedItem(string _perfil, string _calendario, string _instrumento, string _pidm)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_MED('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL").ToString();


                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        ///
        /// Calcula el porcentaje de satisfacion en funcion de las respuestas registradas
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>
        public decimal GetPercObjT(string _instrumento, string _pidm, string _calr, string _pfle)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_TOTAL_OBJ_PERC ('" + _pidm + "','" + _instrumento + ",'" + _calr + "','" + _pfle + "') from dual ").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Calcula el valor porcentual para un objetivo aun no registrado
        /// 
        /// </summary>
        /// <param name="_resp_value"></param>
        /// <param name="_perc_value"></param>
        /// <returns></returns>

        public decimal GetPercObj(decimal _resp_value, decimal _perc_value)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_OBJ_PERC ('" + _resp_value + "','" + _perc_value + "') from dual").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /*
         * 
         * Instrumento de Extension
         * 
         */

        /// <summary>
        /// 
        /// Suma las horas correspondientes a los proyectos de extension
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetSumProyExt(string _perfil, string _calendario, string _instrumento, string _pidm)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SSELECT BZPKEVAL.F_GET_POLL_EXT('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "') FROM DUAL").ToString();


                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }

                }
            }

        }

        /// <summary>
        /// 
        /// Devuelve el monto actual de la UC 
        /// para el calculo de los instrumentos de extension
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_attr"></param>
        /// <returns></returns>

        public decimal GetUCAmount(string _term)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_AMOUNT_UC ('" + _term + "') from dual ").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }

                }
            }

        }

        /// <summary>
        ///  
        ///  Devuelve expresado en Unidades Credito el Valor correspondiente 
        ///  a los ingresos generados a la UCAB por concepto de asesoria y/o
        ///  Consultoria
        ///  
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>

        public decimal GetUCValue(string _term, string _amount)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_VALUE_UC_EXT('" + _term + "', '" + _amount + "') from dual ").ToString();


                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Calcula los puntos obtenidos en funcion del costo de la UC y 
        /// del monto ingresado por concepto de asesorias o consultorias
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_amount"></param>
        /// <returns></returns>

        public decimal GetUCPoints(string _term, string _amount)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval.F_GET_POINTS_UC_EXT ('" + _term + "','" + _amount + "') from dual ").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Calculo de la sumatoria correspondiente a un instrumento 
        /// por areas
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_pidm"></param>
        /// <returns></returns>

        public decimal GetSumAns(string _perfil, string _calendario, string _instrumento, string _pidm, string _area)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_RESULT_SUM('" + _perfil + "','" + _calendario + "','" + _instrumento + "','" + _pidm + "','" + _area + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /*
         * 
         * Instrumento de Investigadores
         * 
         */

        /// <summary>
        /// 
        /// Calcula los puntos por publicaciones
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        public decimal GetInvPub(string _pregunta, string _cantidad)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval. F_CALC_INVE_PUBL('" + _pregunta + "','" + _cantidad + "') from dual ").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /// <summary>
        /// 
        /// Calcula los puntos por comite
        /// 
        /// </summary>
        /// <param name="_pregunta"></param>
        /// <param name="_cantidad"></param>
        /// <returns></returns>
        public decimal GetInvCom(string _pregunta, string _cantidad)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select bzpkeval. F_CALC_INVE_CONS('" + _pregunta + "','" + _cantidad + "') from dual ").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }

        /*
         * 
         * General de funciones
         * 
         */

        /// <summary>
        ///  
        /// Obtiene el total de areas que componen un instrumento
        ///  
        /// </summary>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <returns></returns>

        public decimal GetAreaIns(string _calendario, string _instrumento)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_AREA_COUNT('" + _calendario + "','" + _instrumento + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }


        /// <summary>
        /// 
        /// Devuelve el total de preguntas que componen un Area
        /// 
        /// </summary>
        /// <param name="_term"></param>
        /// <param name="_calendario"></param>
        /// <param name="_instrumento"></param>
        /// <param name="_perfil"></param>
        /// <returns></returns>

        public decimal GetPregCont(string _term, string _calendario, string _instrumento, string _perfil)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_COUNT_QSTN('" + _term + "', '" + _calendario + "','" + _instrumento + "','" + _perfil + "') FROM DUAL").ToString();

                        foreach (decimal resp in res)
                        {
                            return resp;
                        }
                        return 0;
                    }

                    catch (Exception e)
                    {

                        return (0);

                    }
                }
            }
        }


        /// <summary>
        /// 
        /// Obtiene el codigo de los instrumentos
        /// 
        /// </summary>
        /// <param name="_instrumento"></param>
        /// <returns></returns>

        public String GetInsCod(string _instrumento)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_POLL_CODE('" + _instrumento + "') FROM DUAL").ToString();


                        string code = res;

                        return res;

                        return "No encontrado";
                    }

                    catch (Exception e)
                    {

                        return ("Error");

                    }
                }
            }
        }



        /// <summary>
        /// 
        /// Obtiene el codigo de un perfil
        /// 
        /// </summary>
        /// <param name="_perfil"></param>
        /// <returns></returns>
        public String GetPerCod(string _perfil)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_PFLE_CODE('" + _perfil + "') FROM DUAL").ToString();

                        String code;
                        code = res;

                        return code;
                    }

                    catch (Exception e)
                    {

                        return ("Error");

                    }
                }
            }
        }


        /// <summary>
        /// 
        /// Obtiene el codigo del calendario
        /// 
        /// </summary>
        /// <param name="_calendario"></param>
        /// <returns></returns>

        public String GetCalCod(string _calendario)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"SELECT BZPKEVAL.F_GET_CALR_CODE('" + _calendario + "') FROM DUAL").ToString();

                        string resp = res;

                        return resp;

                        return "No encontrado";
                    }

                    catch (Exception e)
                    {

                        return (("Error"));

                    }
                }
            }
        }



        public void PutPZRPLAP(int id, string user, string status)//Domain.AsignacionInstrumentoPersona _PZRPLAP)
        {


            List<Domain.AsignacionInstrumentoPersona> re = new List<Domain.AsignacionInstrumentoPersona>();
            int _pidm = 0;


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {

                        var res = session.CreateSQLQuery(@"select * FROM PZRPLAP WHERE pzrplap.PZRPLAP_SEQ_NUMBER = '" + id + "' ").List<Domain.AsignacionInstrumentoPersona>();

                        foreach (var result in res)
                        {
                            DateTime fecha = DateTime.Today;
                            _pidm = result.PZRPLAP_PIDM;
                            result.PZRPLAP_DATE = fecha;
                            result.PZRPLAP_ACTIVITY_DATE = fecha;
                            result.PZRPLAP_STATUS = status;
                            result.PZRPLAP_USER = user;

                            session.Save(result);



                        }
                        session.Close();
                        //  String respuest = GetNofifier(_pidm, "Se ha cargado su evaluación ", "Se ha modificado exitosamete su evaluacion ");
                    }
                    catch (Exception e)
                    {

                    }

                }
            }
        }




        /// <summary>
        /// Procedimiento que envia 1 correo a  1 Usuario
        /// 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifier(int _pidm, String _titulo, String _contenido)
        {


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var res = session.CreateSQLQuery(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Sistema de Evaluacion de Desempeño ', 'smtp2.ucab.edu.ve', 'Notificacion de Evaluacion', '" + _contenido + "') " +
                                                                        " FROM dual"
                                                                         ).List<string>();
                        if (res.Equals("T - Mail Sent"))
                        {

                            return ("Se ha enviado el correo");

                        }
                        else
                        {
                            return ("No se pudo enviar. Intente Nuevamente. ");
                        }

                    }

                    catch (Exception e)
                    {

                        return ("Error.");

                    }


                }
            }
        }
        public string NameUser(int _pidm)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var res = session.CreateSQLQuery(@"select f_format_name('" + _pidm + "', 'LFM') from dual").List<string>();

                    foreach (string rp in res)
                    {
                        return rp;
                    }

                    return "null";

                }
            }
        }

        public string NamePoll(string _code)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var res = session.CreateSQLQuery(@" select pzbpoll.pzbpoll_name
                                                     from pzbpoll where pzbpoll_code='" + _code + "'").List<string>();

                    foreach (string rp in res)
                    {
                        return rp;
                    }

                    return "null";

                }
            }
        }

        public string IdEvaluador(int _pidm, string _pfle, string _post,
                                  string _dncy, string _poll, string _calr)
        {

            string resp;
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var res = session.CreateSQLQuery(@" select prso.pzbprso_pidm

                                                                from pzrplap,
                                                                     pzraspe,pzbprso,
                                                                     pzrdypt,pzvpost,
                                                                     pzvdncy,pzvpfle,
                                                                --evaluador
                                                                     pzbprso prso,
                                                                     pzrdypt dypt,
                                                                     pzvpost post,
                                                                     pzvdncy dncy,
                                                                     pzraspe aspe

                                                                where pzrplap.pzrplap_pidm ='" + _pidm + "'" +
                                                               "and pzrplap_pfle_code='" + _pfle + "'" +
                                                               "and pzrplap_post_code = '" + _post + "'" +
                                                               " and pzrplap_dncy_code='" + _dncy + "'" +
                                                               " AND pzrplap_calr_code= '" + _calr + "'" +
                                                               " and pzrplap_poll_code= '" + _poll + "'" +
                                                               " AND pzraspe.pzraspe_pidm = pzrplap.pzrplap_pidm" +
                                                               " and pzraspe.pzraspe_pidm = pzbprso.pzbprso_pidm" +
                                                               " and pzraspe.pzraspe_pfle_code = pzrplap.pzrplap_pfle_code" +
                                                               " and pzraspe.pzraspe_pfle_code = pzvpfle.pzvpfle_code" +
                                                               " and pzraspe.pzraspe_post_code = pzrplap.pzrplap_post_code" +
                                                               " and pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code" +
                                                               " and pzrdypt.pzrdypt_post_code = pzvpost.pzvpost_code" +
                                                               " and pzvpost.pzvpost_code_sup = post.pzvpost_code  " +
                                                               " and post.pzvpost_code = dypt.pzrdypt_post_code" +
                                                               " and dypt.pzrdypt_post_code = aspe.pzraspe_post_code" +
                                                               " and aspe.pzraspe_pidm = prso.pzbprso_pidm" +
                                                               " and pzrplap.pzrplap_dncy_code = pzraspe.pzraspe_dncy_code" +
                                                               " and pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code" +
                                                               " and pzrdypt.pzrdypt_dncy_code = pzvdncy.pzvdncy_code" +
                                                               " and pzvdncy.pzvdncy_code_sup =  dncy.pzvdncy_code "
                                                                 ).List<string>();
                    foreach (string rp in res)
                    {
                        return rp;
                    }

                    return "null";

                }
            }
        }


        public List<AsignacionInstrumentoPregunta> NoAnswerQstn(int id, string idins, string idcal, string _plap)
        {

            List<Domain.AsignacionInstrumentoPregunta> result = new List<Domain.AsignacionInstrumentoPregunta>();


            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    var res = session.CreateSQLQuery(@"SELECT  DISTINCT PZBQSTN.*,PZBPLQS.* ,PZBCRIT.*,PZBAREA.*
                                              FROM PZBPOLL,PZBAPPL,
                                                   PZBPLQS,PZBQSTN,
                                                   PZRPLAP,PZBCALR,
                                                   PZRASPE,PZBPRSO,
                                                   PZVPFLE,PZRDYPT,
                                                   PZVPOST,PZVDNCY,
                                                   PZBAREA,PZBCRIT,PZRAWAP
                                                 WHERE  PZBPOLL.PZBPOLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE
                                                         AND PZBAPPL.PZBAPPL_POLL_CODE = PZBPLQS.PZBPLQS_POLL_CODE
                                                         AND PZRPLAP.PZRPLAP_POLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE
                                                         AND PZRPLAP.PZRPLAP_STATUS NOT IN ('F','C')
                                                         AND PZBAPPL.PZBAPPL_SEQ_NUMBER = PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER
                                                         AND PZBAPPL.PZBAPPL_SEQ_NUMBER = PZRPLAP.PZRPLAP_APPL_SEQ_NUMBER  
                                                         AND PZBCALR.PZBCALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                         AND PZBAPPL.PZBAPPL_CALR_CODE = PZBPLQS.PZBPLQS_CALR_CODE
                                                         AND PZVPFLE.PZVPFLE_CODE = PZBAPPL.PZBAPPL_PFLE_CODE
                                                         AND PZBQSTN.PZBQSTN_CODE = PZBPLQS.PZBPLQS_QSTN_CODE
                                                         AND PZBCRIT.PZBCRIT_CODE = PZBPLQS.PZBPLQS_CRIT_CODE
                                                         AND PZBAREA.PZBAREA_CODE = PZBPLQS.PZBPLQS_AREA_CODE
                                                         AND PZBPLQS.PZBPLQS_SEQ_NUMBER NOT IN(SELECT  PZRAWAP.PZRAWAP_PLQS_SEQ_NUMBER FROM PZRAWAP WHERE PZRAWAP.PZRAWAP_PLAP_SEQ_NUMBER = '" + _plap + "' )" +
                                                         "AND PZBPLQS.PZBPLQS_STATUS = 'E' " +
                                                         "AND PZRPLAP.PZRPLAP_SEQ_NUMBER = PZRAWAP.PZRAWAP_PLAP_SEQ_NUMBER " +
                                                         "AND PZRPLAP.PZRPLAP_POLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_CALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_PFLE_CODE = PZBAPPL.PZBAPPL_PFLE_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_PFLE_CODE = PZRASPE.PZRASPE_PFLE_CODE " +
                                                         "AND PZRASPE.PZRASPE_PFLE_CODE = PZVPFLE.PZVPFLE_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_POST_CODE = PZRASPE.PZRASPE_POST_CODE " +
                                                         "AND PZRASPE.PZRASPE_POST_CODE = PZRDYPT.PZRDYPT_POST_CODE " +
                                                         "AND PZRDYPT.PZRDYPT_POST_CODE =  PZVPOST.PZVPOST_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_DNCY_CODE = PZRASPE.PZRASPE_DNCY_CODE " +
                                                         "AND PZRASPE.PZRASPE_DNCY_CODE = PZRDYPT.PZRDYPT_DNCY_CODE " +
                                                         "AND PZRDYPT.PZRDYPT_DNCY_CODE =  PZVDNCY.PZVDNCY_CODE " +
                                                         "AND PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_PIDM " +
                                                         "AND PZRASPE.PZRASPE_PIDM = PZBPRSO.PZBPRSO_PIDM " +
                                                         "AND PZBPOLL.PZBPOLL_CODE='" + idins + "'" +
                                                         "and PZBPRSO.PZBPRSO_PIDM = '" + id + "'" +
                                                         "and PZBCALR.PZBCALR_CODE ='" + idcal + "'" +
                                                         "and PZRPLAP.PZRPLAP_SEQ_NUMBER='" + _plap + "' " +
                                                        "ORDER BY PZBPLQS.PZBPLQS_ORDER , pzbplqs.pzbplqs_area_code "

                                                                )
                                                                 .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                                 .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                                 .AddEntity("PZBAREA", typeof(Domain.Area))
                                                                 .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                                 .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                                    .List<AsignacionInstrumentoPregunta>();
                    foreach (var x in res)
                    {
                        Domain.AsignacionInstrumentoPregunta _insPre = new Domain.AsignacionInstrumentoPregunta();

                        _insPre.PZBPLQS_SEQ_NUMBER = x.PZBPLQS_SEQ_NUMBER;
                        _insPre.PZBPLQS_USER = x.PZBPLQS_USER;
                        _insPre.PZBPLQS_DATA_ORIGIN = x.PZBPLQS_DATA_ORIGIN;
                        _insPre.PZBPLQS_ACTIVITY_DATE = x.PZBPLQS_ACTIVITY_DATE;
                        _insPre.PZBPLQS_QSTN_CODE = x.PZBPLQS_QSTN_CODE;
                        _insPre.PZBPLQS_POLL_CODE = x.PZBPLQS_POLL_CODE;
                        _insPre.PZBPLQS_CALR_CODE = x.PZBPLQS_CALR_CODE;
                        _insPre.PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS_APPL_SEQ_NUMBER;
                        _insPre.PZBPLQS_AREA_CODE = x.PZBPLQS_AREA_CODE;
                        _insPre.PZBPLQS_CRIT_CODE = x.PZBPLQS_CRIT_CODE;
                        _insPre.PZBPLQS_TYAW_CODE = x.PZBPLQS_TYAW_CODE;
                        _insPre.PZBPLQS_MAX_OVAL = x.PZBPLQS_MAX_OVAL;
                        _insPre.PZBPLQS_MAX_VALUE = x.PZBPLQS_MAX_VALUE;
                        _insPre.PZBPLQS_MIN_OVAL = x.PZBPLQS_MIN_OVAL;
                        _insPre.PZBPLQS_MIN_VALUE = x.PZBPLQS_MIN_VALUE;
                        _insPre.PZBPLQS_ORDER = x.PZBPLQS_ORDER;
                        _insPre.PZBPLQS_STATUS = x.PZBPLQS_STATUS;


                        //DATOS DEL criterio de Evaluacion
                        Domain.Criterio _cri = new Domain.Criterio();
                        _cri.PZBCRIT_CODE = x.PZBCRIT.PZBCRIT_CODE;
                        _cri.PZBCRIT_USER = x.PZBCRIT.PZBCRIT_USER;
                        _cri.PZBCRIT_DESCRIPTION = x.PZBCRIT.PZBCRIT_DESCRIPTION;
                        _cri.PZBCRIT_DATA_ORIGIN = x.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                        _cri.PZBCRIT_ACTIVITY_DATE = x.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                        _insPre.PZBCRIT = _cri;

                        //Datos del area a la cual pertenece la pregunta
                        Domain.Area _area = new Domain.Area();
                        _area.PZBAREA_CODE = x.PZBAREA.PZBAREA_CODE;
                        _area.PZBAREA_USER = x.PZBAREA.PZBAREA_USER;
                        _area.PZBAREA_DESCRIPTION = x.PZBAREA.PZBAREA_DESCRIPTION;
                        _area.PZBAREA_DATA_ORIGIN = x.PZBAREA.PZBAREA_DATA_ORIGIN;
                        _area.PZBAREA_ACTIVITY_DATE = x.PZBAREA.PZBAREA_ACTIVITY_DATE;
                        _insPre.PZBAREA = _area;

                        Domain.Pregunta item = new Domain.Pregunta();

                        item.PZBQSTN_CODE = x.PZBQSTN.PZBQSTN_CODE;
                        item.PZBQSTN_NAME = x.PZBQSTN.PZBQSTN_NAME;
                        item.PZBQSTN_USER = x.PZBQSTN.PZBQSTN_USER;
                        item.PZBQSTN_DESCRIPTION = x.PZBQSTN.PZBQSTN_DESCRIPTION;
                        item.PZBQSTN_DATA_ORIGIN = x.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                        item.PZBQSTN_ACTIVITY_DATE = x.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                        _insPre.PZBQSTN = item;

                        result.Add(_insPre);
                    }
                    return result;
                }
            }
        }



        /// <summary>
        /// Enviar correo al evaluador
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_pfle"></param>
        /// <param name="_post"></param>
        /// <param name="_dncy"></param>
        /// <param name="_poll"></param>
        /// <param name="_calr"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifierE(int _pidm, string _pfle, string _post,
                                   string _dncy, string _poll, string _calr,
                                   String _titulo, String _contenido)
        {

            try
            {
                String pidm_evaluador = IdEvaluador(_pidm, _pfle, _post,
                                                    _dncy, _poll, _calr);

                using (ISession session = NHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {

                        var res = session.CreateSQLQuery(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + pidm_evaluador + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).List<String>();
                        if (res.Equals("T - Mail Sent"))
                        {

                            return ("Se ha enviado el correo");

                        }
                        else
                        {
                            return ("No se pudo enviar. Intente Nuevamente. ");
                        }

                    }
                }
            }

            catch (Exception e)
            {

                return ("Error.");

            }


        } 



        /// <summary>
        /// Procedimiento que envia correos a un grupo 
        /// </summary>
        /// <param name="_pidm"></param>
        /// <param name="_titulo"></param>
        /// <param name="_contenido"></param>
        /// <returns></returns>
        public string GetNofifierG(int[] _pidm, String _titulo, String _contenido)
        {

            try
            {
                for (int dimension = 1; dimension <= _pidm.Length; dimension++)
                {

                    using (ISession session = NHibernateHelper.OpenSession())
                    {
                        using (ITransaction transaction = session.BeginTransaction())
                        {

                            var res = session.CreateSQLQuery(@" SELECT baninst1.f_sendemail(
                                                                 baninst1.f_bus_email ('" + _pidm[dimension] + "',NULL,'A','Y'), null, 'evaluacionRRHH@ucab.edu.ve', 'Evaluación de Desempeño', 'smtp2.ucab.edu.ve', '" + _titulo + "', '" + _contenido + "') " +
                                                                " FROM dual"
                                                                 ).List<String>();
                        }

                      

                    }

                }
                return ("Enviados");
            }

            catch (Exception e)
            {

                return ("Error");

            }





        }



    }

}

