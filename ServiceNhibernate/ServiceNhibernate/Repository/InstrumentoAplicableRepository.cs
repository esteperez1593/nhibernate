﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;
using NHibernate.Transform;

namespace ServiceNhibernate.Repository
{
    class InstrumentoAplicableRepository : IInstrumentoAplicableRepository
    {

        public List<InstrumentoAplicable> GetPZBAPPL()
        {
            List<Domain.InstrumentoAplicable> result = new List<Domain.InstrumentoAplicable>();
            using (ISession session = NHibernateHelper.OpenSession())
            {

                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var res = session.
                         CreateSQLQuery(@"Select * from PZBAPPL")
                         .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                         .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                         .List<InstrumentoAplicable>();

                        foreach (var X in res)
                        {

                            //Datos del Instrumento Aplicable
                            Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                            _insApl.PZBAPPL_CALR_CODE = X.PZBAPPL_CALR_CODE;
                            _insApl.PZBAPPL_PFLE_CODE = X.PZBAPPL_PFLE_CODE;
                            _insApl.PZBAPPL_PRCNT = X.PZBAPPL_PRCNT;
                            _insApl.PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                            _insApl.PZBAPPL_SEQ_NUMBER = X.PZBAPPL_SEQ_NUMBER;
                            _insApl.PZBAPPL_USER = X.PZBAPPL_USER;
                            _insApl.PZBAPPL_DATA_ORIGIN = X.PZBAPPL_DATA_ORIGIN;
                            _insApl.PZBAPPL_ACTIVITY_DATE = X.PZBAPPL_ACTIVITY_DATE;
                            //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                            result.Add(_insApl);

                        }

                    }
                    catch (Exception e)
                    {

                    }

                    return result;
                }
            }
        }


        public List<InstrumentoAplicable> GetPZBAPPL2()
        {
            List<Domain.InstrumentoAplicable> result = new List<Domain.InstrumentoAplicable>();

            using (ISession session = NHibernateHelper.OpenSession())
            {

                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {


                        var res = session.
                                     CreateSQLQuery(@"Select *
                                                        from PZBAPPL,PZBCALR,PZBPOLL,PZVPFLE
                                                        where PZBAPPL.PZBAPPL_CALR_CODE = PZBCALR_CODE
                                                        and PZBAPPL.PZBAPPL_POLL_CODE = PZBPOLL_CODE
                                                        AND PZBAPPL.PZBAPPL_PFLE_CODE = PZVPFLE_CODE
                                                        AND PZBAPPL.PZBAPPL_SEQ_NUMBER=1")

                                         .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                         .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                         .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                         .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                         .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                         .List<InstrumentoAplicable>();

                        foreach (var X in res)
                        {

                           //Datos del Instrumento Aplicable
                            Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                            _insApl.PZBAPPL_CALR_CODE = X.PZBAPPL_CALR_CODE;
                            _insApl.PZBAPPL_PFLE_CODE = X.PZBAPPL_PFLE_CODE;
                            _insApl.PZBAPPL_PRCNT = X.PZBAPPL_PRCNT;
                            _insApl.PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                            _insApl.PZBAPPL_SEQ_NUMBER = X.PZBAPPL_SEQ_NUMBER;
                            _insApl.PZBAPPL_USER = X.PZBAPPL_USER;
                            _insApl.PZBAPPL_DATA_ORIGIN = X.PZBAPPL_DATA_ORIGIN;
                            _insApl.PZBAPPL_ACTIVITY_DATE = X.PZBAPPL_ACTIVITY_DATE;
                            //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;

                            Domain.Calendario _calr = new Domain.Calendario();
                            _calr.PZBCALR_CODE = X.PZBCALR.PZBCALR_CODE;
                            _calr.PZBCALR_NAME = X.PZBCALR.PZBCALR_NAME;
                            _calr.PZBCALR_SRL = X.PZBCALR.PZBCALR_SRL;
                            _calr.PZBCALR_TERM = X.PZBCALR.PZBCALR_TERM;
                            _calr.PZBCALR_USER = X.PZBCALR.PZBCALR_USER;
                            _calr.PZBCALR_INIT_DATE = X.PZBCALR.PZBCALR_INIT_DATE;
                            _calr.PZBCALR_END_DATE = X.PZBCALR.PZBCALR_END_DATE;
                            _calr.PZBCALR_DATA_ORIGIN = X.PZBCALR.PZBCALR_DATA_ORIGIN;
                            _calr.PZBCALR_ACTIVITY_DATE = X.PZBCALR.PZBCALR_ACTIVITY_DATE;
                            
                            Domain.Instrumento _poll = new Instrumento();
                            _poll.PZBPOLL_CODE = X.PZBPOLL.PZBPOLL_CODE;
                            _poll.PZBPOLL_NAME = X.PZBPOLL.PZBPOLL_NAME;
                            _poll.PZBPOLL_USER = X.PZBPOLL.PZBPOLL_USER;
                            _poll.PZBPOLL_DATA_ORIGIN = X.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                            _poll.PZBPOLL_ACTIVITY_DATE = X.PZBPOLL.PZBPOLL_ACTIVITY_DATE;

                            Domain.Perfil _pfle = new Perfil();
                            _pfle.PZVPFLE_CODE = X.PZVPFLE.PZVPFLE_CODE;
                            _pfle.PZVPFLE_NAME = X.PZVPFLE.PZVPFLE_NAME;
                            _pfle.PZVPFLE_USER = X.PZVPFLE.PZVPFLE_USER;
                            _pfle.PZVPFLE_DATA_ORIGIN = X.PZVPFLE.PZVPFLE_DATA_ORIGIN;
                            _pfle.PZVPFLE_ACTIVITY_DATE = X.PZVPFLE.PZVPFLE_ACTIVITY_DATE;



                            _insApl.PZVPFLE = _pfle;
                            _insApl.PZBCALR = _calr;
                            _insApl.PZBPOLL = _poll;


                             result.Add(_insApl);

                        }

                    }
                    catch (Exception e)
                    {

                    }

                    return result;
                }
            }
        }


        public InstrumentoAplicable Add(InstrumentoAplicable _instrumento)
        {
            throw new NotImplementedException();
        }

        public InstrumentoAplicable GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public InstrumentoAplicable GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(InstrumentoAplicable _instrumento)
        {
            throw new NotImplementedException();
        }

        public void Update(InstrumentoAplicable _instrumento)
        {
            throw new NotImplementedException();
        }
    }
}