﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;


namespace ServiceNhibernate.Repository
{
    class InstrumentoPreguntaRepository : IAsignacionInstrumentoPreguntaRepository
    { 
        public List<AsignacionInstrumentoPregunta> GetAllNADA()
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                List<Domain.AsignacionInstrumentoPregunta> result = new List<Domain.AsignacionInstrumentoPregunta>();
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var respuesta = session.
                         CreateSQLQuery(@"Select * from PZBPLQS")
                          .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                           .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                           .List<AsignacionInstrumentoPregunta>();


                        foreach (var X in respuesta)
                        {

                            Domain.AsignacionInstrumentoPregunta _item = new Domain.AsignacionInstrumentoPregunta();
                           
                            _item.PZBPLQS_APPL_SEQ_NUMBER = X.PZBPLQS_APPL_SEQ_NUMBER;
                            _item.PZBPLQS_SEQ_NUMBER = X.PZBPLQS_SEQ_NUMBER;
                            _item.PZBPLQS_DATA_ORIGIN = X.PZBPLQS_DATA_ORIGIN;
                            _item.PZBPLQS_MAX_VALUE = X.PZBPLQS_MAX_VALUE;
                            _item.PZBPLQS_MIN_VALUE = X.PZBPLQS_MIN_VALUE;
                            _item.PZBPLQS_POLL_CODE = X.PZBPLQS_POLL_CODE;
                            _item.PZBPLQS_QSTN_CODE = X.PZBPLQS_QSTN_CODE;
                            _item.PZBPLQS_TYAW_CODE = X.PZBPLQS_TYAW_CODE;
                            _item.PZBPLQS_CRIT_CODE = X.PZBPLQS_CRIT_CODE;
                            _item.PZBPLQS_CALR_CODE = X.PZBPLQS_CALR_CODE;
                            _item.PZBPLQS_AREA_CODE = X.PZBPLQS_AREA_CODE;
                            _item.PZBPLQS_MAX_OVAL = X.PZBPLQS_MAX_OVAL;
                            _item.PZBPLQS_MIN_OVAL = X.PZBPLQS_MIN_OVAL;
                            _item.PZBPLQS_STATUS = X.PZBPLQS_STATUS;
                            _item.PZBPLQS_USER = X.PZBPLQS_USER;

                            result.Add(_item);
                        }


                        return result;

                    }
                    catch (Exception e) { }


                }

                return result;

            }
        }

        public List<AsignacionInstrumentoPregunta> GetAll()
        {
             
            using (ISession session = NHibernateHelper.OpenSession())
            {
                List<Domain.AsignacionInstrumentoPregunta> result = new List<Domain.AsignacionInstrumentoPregunta>();
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var respuesta = session.
                         CreateSQLQuery(@"Select *
                                            from PZBPLQS,PZBAREA,PZBCRIT,PZBAPPL,PZBPOLL,PZBCALR,PZVPFLE
                                            WHERE 
                                            PZBPLQS_AREA_CODE = PZBAREA_CODE AND 
                                            PZBPLQS_CRIT_CODE = PZBCRIT_CODE AND
                                            PZBPLQS_APPL_SEQ_NUMBER =PZBAPPL_SEQ_NUMBER AND 
                                            PZBAPPL.PZBAPPL_CALR_CODE = PZBCALR_CODE
                                             and PZBAPPL.PZBAPPL_POLL_CODE = PZBPOLL_CODE
                                            AND PZBAPPL.PZBAPPL_PFLE_CODE = PZVPFLE_CODE")
                       
                         .AddEntity("PZBAREA", typeof(Domain.Area))
                         .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                         .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                         .AddEntity("PZBCALR", typeof(Domain.Calendario))
                         .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                         .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                         .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                           .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                           .List<AsignacionInstrumentoPregunta>();


                        foreach (var X in respuesta)
                        {

                            Domain.AsignacionInstrumentoPregunta _item = new Domain.AsignacionInstrumentoPregunta();

                            _item.PZBPLQS_APPL_SEQ_NUMBER = X.PZBPLQS_APPL_SEQ_NUMBER;
                            _item.PZBPLQS_SEQ_NUMBER = X.PZBPLQS_SEQ_NUMBER;
                            _item.PZBPLQS_DATA_ORIGIN = X.PZBPLQS_DATA_ORIGIN;
                            _item.PZBPLQS_MAX_VALUE = X.PZBPLQS_MAX_VALUE;
                            _item.PZBPLQS_MIN_VALUE = X.PZBPLQS_MIN_VALUE;
                            _item.PZBPLQS_POLL_CODE = X.PZBPLQS_POLL_CODE;
                            _item.PZBPLQS_QSTN_CODE = X.PZBPLQS_QSTN_CODE;
                            _item.PZBPLQS_TYAW_CODE = X.PZBPLQS_TYAW_CODE;
                            _item.PZBPLQS_CRIT_CODE = X.PZBPLQS_CRIT_CODE;
                            _item.PZBPLQS_CALR_CODE = X.PZBPLQS_CALR_CODE;
                            _item.PZBPLQS_AREA_CODE = X.PZBPLQS_AREA_CODE;
                            _item.PZBPLQS_MAX_OVAL = X.PZBPLQS_MAX_OVAL;
                            _item.PZBPLQS_MIN_OVAL = X.PZBPLQS_MIN_OVAL;
                            _item.PZBPLQS_STATUS = X.PZBPLQS_STATUS;
                            _item.PZBPLQS_USER = X.PZBPLQS_USER;


                            //DATOS DEL criterio de Evaluacion
                            Domain.Criterio _cri = new Domain.Criterio();
                            _cri.PZBCRIT_CODE = X.PZBCRIT.PZBCRIT_CODE;
                            _cri.PZBCRIT_USER = X.PZBCRIT.PZBCRIT_USER;
                            _cri.PZBCRIT_DESCRIPTION = X.PZBCRIT.PZBCRIT_DESCRIPTION;
                            _cri.PZBCRIT_DATA_ORIGIN = X.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                            _cri.PZBCRIT_ACTIVITY_DATE = X.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                            _item.PZBCRIT = _cri;

                            //Datos del area a la cual pertenece la pregunta
                            Domain.Area _area = new Domain.Area();
                            _area.PZBAREA_CODE = X.PZBAREA.PZBAREA_CODE;
                            _area.PZBAREA_USER = X.PZBAREA.PZBAREA_USER;
                            _area.PZBAREA_DESCRIPTION = X.PZBAREA.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_DATA_ORIGIN = X.PZBAREA.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = X.PZBAREA.PZBAREA_ACTIVITY_DATE;
                            _item.PZBAREA = _area;


                            //Datos del Instrumento Aplicable
                            Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                            _insApl.PZBAPPL_CALR_CODE = X.PZBAPPL.PZBAPPL_CALR_CODE;
                            _insApl.PZBAPPL_PFLE_CODE = X.PZBAPPL.PZBAPPL_PFLE_CODE;
                            _insApl.PZBAPPL_PRCNT = X.PZBAPPL.PZBAPPL_PRCNT;
                            _insApl.PZBAPPL_POLL_CODE = X.PZBAPPL.PZBAPPL_POLL_CODE;
                            _insApl.PZBAPPL_SEQ_NUMBER = X.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                            _insApl.PZBAPPL_USER = X.PZBAPPL.PZBAPPL_USER;
                            _insApl.PZBAPPL_DATA_ORIGIN = X.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                            _insApl.PZBAPPL_ACTIVITY_DATE = X.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                            //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;

                            Domain.Calendario _calr = new Domain.Calendario();
                            _calr.PZBCALR_CODE = X.PZBAPPL.PZBCALR.PZBCALR_CODE;
                            _calr.PZBCALR_NAME = X.PZBAPPL.PZBCALR.PZBCALR_NAME;
                            _calr.PZBCALR_SRL = X.PZBAPPL.PZBCALR.PZBCALR_SRL;
                            _calr.PZBCALR_TERM = X.PZBAPPL.PZBCALR.PZBCALR_TERM;
                            _calr.PZBCALR_USER = X.PZBAPPL.PZBCALR.PZBCALR_USER;
                            _calr.PZBCALR_INIT_DATE = X.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                            _calr.PZBCALR_END_DATE = X.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                            _calr.PZBCALR_DATA_ORIGIN = X.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                            _calr.PZBCALR_ACTIVITY_DATE = X.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;

                            Domain.Instrumento _poll = new Instrumento();
                            _poll.PZBPOLL_CODE = X.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                            _poll.PZBPOLL_NAME = X.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                            _poll.PZBPOLL_USER = X.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                            _poll.PZBPOLL_DATA_ORIGIN = X.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                            _poll.PZBPOLL_ACTIVITY_DATE = X.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                            _insApl.PZBCALR = _calr;
                            _insApl.PZBPOLL = _poll;


                            _item.PZBAPPL = _insApl;

                            result.Add(_item);
                        }


                        return result;

                    }
                    catch (Exception e) { }


                }

                return result;

            }
        }


      



        public AsignacionInstrumentoPregunta Add(AsignacionInstrumentoPersona cargo)
        {
            throw new NotImplementedException();
        }

        public AsignacionInstrumentoPersona GetById(decimal cargo)
        {
            throw new NotImplementedException();
        }

        public AsignacionInstrumentoPersona GetByName(string cargo)
        {
            throw new NotImplementedException();
        }

        public void Remove(AsignacionInstrumentoPersona cargo)
        {
            throw new NotImplementedException();
        }

        public void Update(AsignacionInstrumentoPersona cargo)
        {
            throw new NotImplementedException();
        }
    }
}