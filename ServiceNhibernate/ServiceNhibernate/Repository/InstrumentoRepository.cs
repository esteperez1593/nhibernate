﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    public class InstrumentoRepository : IInstrumentoRepository
    {


        public List<Instrumento> GetPZBPOLL()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    List<Domain.Instrumento> re = new List<Domain.Instrumento>();

                    var res = session.CreateSQLQuery(@"Select * from PZBPOLL")
                                                .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<Instrumento>();
                    foreach (var x in res)
                    {
                        Domain.Instrumento ins = new Domain.Instrumento();

                        // ins.PZBPOLL_ID = x.PZBPOLL_ID;
                        ins.PZBPOLL_NAME = x.PZBPOLL_NAME;
                        ins.PZBPOLL_CODE = x.PZBPOLL_CODE;
                        ins.PZBPOLL_USER = x.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = x.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = x.PZBPOLL_ACTIVITY_DATE;


                        re.Add(ins);


                    }
                    return (re);
                    transaction.Commit();
                }
            }
        }


        /// <summary>
        /// Dado el pidm del usuario muestra sus instrumentos 
        /// activos pendientes
        /// </summary>
        /// <param name="pidm"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<InstrumentoAplicable> GetPZBPOLLP(int pidm, string status)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    List<Domain.InstrumentoAplicable> re = new List<Domain.InstrumentoAplicable>();

                    var res = session.CreateSQLQuery(@"SELECT *
                                                FROM PZBPLQS,
                                                PZRPLAP,
                                                PZBCALR,
                                                PZVPFLE, 
                                                PZRASPE,
                                                PZBPRSO,
                                                PZRDYPT,
                                                PZVDNCY,
                                                PZVPOST,
                                                PZBAPPL,
                                                pzbpoll,
                                                PZBPHSE,
                                                PZVPOST PZVPOST1,
                                                PZVDNCY PZVDNCY1
                                                WHERE pzbappl.pzbappl_poll_code = pzbpoll.pzbpoll_code
                                                AND   pzbappl.pzbappl_pfle_code = PZRPLAP.PZRPLAP_PFLE_CODE
                                                AND   PZRPLAP.PZRPLAP_PFLE_code = pzraspe.pzraspe_pfle_code
                                                AND   pzraspe.pzraspe_pfle_code = PZVPFLE.PZVPFLE_code
                                                AND   pzrplap.pzrplap_appl_seq_number = pzbappl.pzbappl_seq_number
                                                and   pzbappl.pzbappl_calr_code = PZRPLAP.PZRPLAP_CALR_code 
                                                AND   pzbappl.pzbappl_calr_code = PZBCALR.PZBCALR_CODE
                                                AND pzbcalr.pzbcalr_code = pzbphse.pzbphse_calr_code
                                                    AND SYSDATE >= pzbphse.pzbphse_open_date
                                                    and sysdate <=pzbphse.pzbphse_close_date 
                                                AND   pzrplap.pzrplap_post_code = pzraspe.pzraspe_post_code
                                                AND   pzraspe.pzraspe_post_code = pzrdypt.pzrdypt_post_code
                                                AND   PZRDYPT.PZRDYPT_POST_CODE = PZVPOST.PZVPOST_CODE
                                                AND   PZVPOST.PZVPOST_CODE_sup = PZVPOST1.PZVPOST_CODE 
                                                AND   pzrplap.pzrplap_dncy_code= pzraspe.pzraspe_dncy_code
                                                AND   pzraspe.pzraspe_dncy_code = pzrdypt.pzrdypt_dncy_code
                                                AND   PZRDYPT.PZRDYPT_DNCY_CODE = PZVDNCY.PZVDNCY_CODE
                                                AND   PZVDNCY.PZVDNCY_CODE_sup = PZVDNCY1.PZVDNCY_CODE
                                                AND   PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_pidm 
                                                AND   PZRASPE.PZRASPE_pidm = pzbprso.pzbprso_pidm
                                                AND   pzbplqs.pzbplqs_appl_seq_number = pzbappl.pzbappl_seq_number
                                                AND   PZBPLQS.PZBPLQS_POLL_CODE = pzbappl.pzbappl_poll_code
                                                AND   PZBPLQS.PZBPLQS_CALR_CODE = pzbappl.pzbappl_calr_code
                                                AND PZRPLAP.PZRPLAP_STATUS = '" + status + "' " +
                                               "and pzrplap.pzrplap_pidm = '" + pidm + "'")


                                                .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                                .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                .AddEntity("PZBPHSE", typeof(Domain.Fase))
                                                .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                .List<InstrumentoAplicable>();
                    foreach (var x in res)
                    {
                        Domain.Instrumento ins = new Domain.Instrumento();

                        Domain.InstrumentoAplicable apl = new Domain.InstrumentoAplicable();
                        apl.PZBAPPL_USER = x.PZBAPPL_USER;
                        apl.PZBAPPL_PRCNT = x.PZBAPPL_PRCNT;
                        apl.PZBAPPL_SEQ_NUMBER = x.PZBAPPL_SEQ_NUMBER;
                        apl.PZBAPPL_POLL_CODE = x.PZBAPPL_POLL_CODE;
                        apl.PZBAPPL_PFLE_CODE = x.PZBAPPL_PFLE_CODE;
                        apl.PZBAPPL_CALR_CODE = x.PZBAPPL_CALR_CODE;
                        apl.PZBAPPL_ACTIVITY_DATE = x.PZBAPPL_ACTIVITY_DATE;
                        apl.PZBAPPL_DATA_ORIGIN = x.PZBAPPL_DATA_ORIGIN;


                        // ins.PZBPOLL_ID = x.PZBPOLL_ID;
                        ins.PZBPOLL_NAME = x.PZBPOLL.PZBPOLL_NAME;
                        ins.PZBPOLL_CODE = x.PZBPOLL.PZBPOLL_CODE;
                        ins.PZBPOLL_USER = x.PZBPOLL.PZBPOLL_USER;
                        ins.PZBPOLL_DATA_ORIGIN = x.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                        ins.PZBPOLL_ACTIVITY_DATE = x.PZBPOLL.PZBPOLL_ACTIVITY_DATE;

                        apl.PZBPOLL = ins;

                        Domain.Calendario dcal = new Domain.Calendario();
                        dcal.PZBCALR_CODE = x.PZBCALR.PZBCALR_CODE;
                        dcal.PZBCALR_NAME = x.PZBCALR.PZBCALR_NAME;
                        dcal.PZBCALR_USER = x.PZBCALR.PZBCALR_USER;
                        dcal.PZBCALR_TERM = x.PZBCALR.PZBCALR_TERM;
                        dcal.PZBCALR_SRL = x.PZBCALR.PZBCALR_SRL;
                        dcal.PZBCALR_END_DATE = x.PZBCALR.PZBCALR_END_DATE;
                        dcal.PZBCALR_INIT_DATE = x.PZBCALR.PZBCALR_INIT_DATE;
                        dcal.PZBCALR_DATA_ORIGIN = x.PZBCALR.PZBCALR_DATA_ORIGIN;
                        dcal.PZBCALR_ACTIVITY_DATE = x.PZBCALR.PZBCALR_ACTIVITY_DATE;
                        apl.PZBCALR = dcal;


                   /*     foreach (var p in x.PZBPLQS)
                        {
                            Domain.AsignacionInstrumentoPregunta _insPre = new Domain.AsignacionInstrumentoPregunta();
                            _insPre.PZBPLQS_SEQ_NUMBER = p.PZBPLQS_SEQ_NUMBER;
                            _insPre.PZBPLQS_USER = p.PZBPLQS_USER;
                            _insPre.PZBPLQS_DATA_ORIGIN = p.PZBPLQS_DATA_ORIGIN;
                            _insPre.PZBPLQS_ACTIVITY_DATE = p.PZBPLQS_ACTIVITY_DATE;
                            _insPre.PZBPLQS_QSTN_CODE = p.PZBPLQS_QSTN_CODE;
                            _insPre.PZBPLQS_POLL_CODE = p.PZBPLQS_POLL_CODE;
                            _insPre.PZBPLQS_CALR_CODE = p.PZBPLQS_CALR_CODE;
                            _insPre.PZBPLQS_APPL_SEQ_NUMBER = p.PZBPLQS_APPL_SEQ_NUMBER;
                            _insPre.PZBPLQS_AREA_CODE = p.PZBPLQS_AREA_CODE;
                            _insPre.PZBPLQS_CRIT_CODE = p.PZBPLQS_CRIT_CODE;
                            _insPre.PZBPLQS_TYAW_CODE = p.PZBPLQS_TYAW_CODE;
                            _insPre.PZBPLQS_MAX_OVAL = p.PZBPLQS_MAX_OVAL;
                            _insPre.PZBPLQS_MAX_VALUE = p.PZBPLQS_MAX_VALUE;
                            _insPre.PZBPLQS_MIN_OVAL = p.PZBPLQS_MIN_OVAL;
                            _insPre.PZBPLQS_MIN_VALUE = p.PZBPLQS_MIN_VALUE;
                            _insPre.PZBPLQS_ORDER = p.PZBPLQS_ORDER;
                            _insPre.PZBPLQS_STATUS = p.PZBPLQS_STATUS;
                            apl.PZBPLQS.Add(_insPre);
                        }


                        foreach (var e in x.PZRPLAP)
                        {

                            Domain.AsignacionInstrumentoPersona item = new Domain.AsignacionInstrumentoPersona();
                            item.PZRPLAP_SEQ_NUMBER = e.PZRPLAP_SEQ_NUMBER;
                            item.PZRPLAP_APPL_SEQ_NUMBER = e.PZRPLAP_APPL_SEQ_NUMBER;
                            item.PZRPLAP_CALR_CODE = e.PZRPLAP_CALR_CODE;
                            item.PZRPLAP_POST_CODE = e.PZRPLAP_POST_CODE;
                            item.PZRPLAP_DNCY_CODE = e.PZRPLAP_DNCY_CODE;
                            item.PZRPLAP_PFLE_CODE = e.PZRPLAP_PFLE_CODE;
                            item.PZRPLAP_POLL_CODE = e.PZRPLAP_POLL_CODE;
                            item.PZRPLAP_STATUS = e.PZRPLAP_STATUS;
                            item.PZRPLAP_DATA_ORIGIN = e.PZRPLAP_DATA_ORIGIN;
                            item.PZRPLAP_ACTIVITY_DATE = e.PZRPLAP_ACTIVITY_DATE;
                            item.PZRPLAP_PIDM = e.PZRPLAP_PIDM;
                            item.PZRPLAP_USER = e.PZRPLAP_USER;
                            item.PZRPLAP_DATE = e.PZRPLAP_DATE;
                            apl.PZRPLAP.Add(item);

                        }


                        foreach (var cal in x.PZBCALR.PZBPHSE)
                        {

                            Domain.Fase fas = new Domain.Fase();
                            fas.PZBPHSE_CODE = cal.PZBPHSE_CODE;
                            fas.PZBPHSE_NAME = cal.PZBPHSE_NAME;
                            fas.PZBPHSE_USER = cal.PZBPHSE_USER;
                            fas.PZBPHSE_OPEN_DATE = cal.PZBPHSE_OPEN_DATE;
                            fas.PZBPHSE_CLOSE_DATE = cal.PZBPHSE_CLOSE_DATE;
                            fas.PZBPHSE_CLEXTENSION_DATE = cal.PZBPHSE_CLEXTENSION_DATE;
                            fas.PZBPHSE_OPEXTENSION_DATE = cal.PZBPHSE_OPEXTENSION_DATE;
                            fas.PZBPHSE_DATA_ORIGIN = cal.PZBPHSE_DATA_ORIGIN;
                            fas.PZBPHSE_ACTIVITY_DATE = cal.PZBPHSE_ACTIVITY_DATE;
                            fas.PZBPHSE_CALR_CODE = cal.PZBPHSE_CALR_CODE;
                            apl.PZBCALR.PZBPHSE.Add(fas);

                        }*/


                        re.Add(apl);


                    }
                    return (re);
                    transaction.Commit();
                }
            }
        }

        public Instrumento Add(Instrumento parametro)
        {
            throw new NotImplementedException();
        }

        public Instrumento GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public Instrumento GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(Instrumento parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(Instrumento parametro)
        {
            throw new NotImplementedException();
        }
    }
}