﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class PerfilRepository : IPerfilRepository
    {

        public List<Perfil> GetPZVPFLE()
        {
            List<Domain.Perfil> re = new List<Domain.Perfil>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        var res = session.CreateSQLQuery(@"Select * from PZVPFLE")
                                            .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                            .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                            .List<Perfil>();

                        foreach (var result in res)
                        {
                            Domain.Perfil item = new Domain.Perfil();

                            //item.PZVPFLE_ID = result.PZVPFLE_ID;
                            item.PZVPFLE_CODE = result.PZVPFLE_CODE;
                            item.PZVPFLE_NAME = result.PZVPFLE_NAME;
                            item.PZVPFLE_ACTIVITY_DATE = result.PZVPFLE_ACTIVITY_DATE;
                            item.PZVPFLE_DATA_ORIGIN = result.PZVPFLE_DATA_ORIGIN;
                            item.PZVPFLE_USER = result.PZVPFLE_USER;


                            re.Add(item);
                        }
                        return re;

                    }
                    catch (Exception e)
                    {


                    }

                }
            }
            return re;
        }

        public Perfil Add(Perfil _area)
        {
            throw new NotImplementedException();
        }

        public Perfil GetById(decimal id)
        {
            throw new NotImplementedException();
        }

        public Perfil GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public void Remove(Perfil _area)
        {
            throw new NotImplementedException();
        }

        public void Update(Perfil _area)
        {
            throw new NotImplementedException();
        }
    }
}