﻿using NHibernate;
using NHibernate.Transform;
using ServiceNhibernate.Domain;
using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace ServiceNhibernate.Repository
{
    class PermisoRepository : IPermisoRepository
    {
        public List<DefinicionPermisos> GetPZVPERM(int pidm)
        {
            List<Domain.DefinicionPermisos> result = new List<Domain.DefinicionPermisos>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {

                        var res = session.CreateSQLQuery(@"select * 
                                                from PZVPERM,PZRASPE,PZVRGHT,PZBPRSO
                                                where pzvperm.pzvperm_post_code= pzraspe.pzraspe_post_code
                                                    and pzvperm.pzvperm_dncy_code = pzraspe.pzraspe_dncy_code
                                                    and pzvperm.pzvperm_rght_code = pzvrght.pzvrght_code
                                                    and pzvperm.pzvperm_pidm =pzraspe.pzraspe_pidm
                                                    and pzbprso.pzbprso_pidm= pzraspe.pzraspe_pidm
                                                    and pzbprso.pzbprso_pidm='" + pidm + "' ")
                                                    .AddEntity("PZVRGHT", typeof(Domain.DefinicionPermisos))
                                                   /*    .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                       .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                       .AddEntity("PZVPERM", typeof(Domain.Permiso))*/
                                                   .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                     .List<DefinicionPermisos>();

                        foreach (var x in res)
                        {
                            //Datos del Instrumento Aplicable
                            Domain.DefinicionPermisos _defpER = new Domain.DefinicionPermisos();
                            _defpER.PZVRGHT_CODE = x.PZVRGHT_CODE;
                            _defpER.PZVRGHT_USER = x.PZVRGHT_USER;
                            _defpER.PZVRGHT_GRADE = x.PZVRGHT_GRADE;
                            _defpER.PZVRGHT_DATA_ORIGIN = x.PZVRGHT_DATA_ORIGIN;
                            _defpER.PZVRGHT_DESCRIPTION = x.PZVRGHT_DESCRIPTION;
                            _defpER.PZVRGHT_ACTIVITY_DATE = x.PZVRGHT_ACTIVITY_DATE;
                            result.Add(_defpER);

                        }
                    }
                    catch (Exception e) { }

                    return result;


                }
            }
         }


     
    }
}