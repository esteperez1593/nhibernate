﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class PreguntaRepository : IPreguntaRepository
    {

        public List<Pregunta> GetPZBQSTN()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    List<Domain.Pregunta> re = new List<Domain.Pregunta>();

                    try
                    {

                        var res = session.CreateSQLQuery(@"Select * from PZBQSTN")
                                                    .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                    .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                    .List<Pregunta>();

                        foreach (var result in res)
                        {
                            Domain.Pregunta item = new Domain.Pregunta();

                            item.PZBQSTN_CODE = result.PZBQSTN_CODE;
                            item.PZBQSTN_NAME = result.PZBQSTN_NAME;
                            item.PZBQSTN_USER = result.PZBQSTN_USER;
                            item.PZBQSTN_DESCRIPTION = result.PZBQSTN_DESCRIPTION;
                            item.PZBQSTN_DATA_ORIGIN = result.PZBQSTN_DATA_ORIGIN;
                            item.PZBQSTN_ACTIVITY_DATE = DateTime.Today;




                            re.Add(item);
                        }
                        return re;
                    }
                    catch (Exception e)
                    {

                    }
                    return re;

                }

            }
            
        }

        public Pregunta Add(Pregunta parametro)
        {
            throw new NotImplementedException();
        }

        public Pregunta GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public Pregunta GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(Pregunta parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(Pregunta parametro)
        {
            throw new NotImplementedException();
        }
    }
}