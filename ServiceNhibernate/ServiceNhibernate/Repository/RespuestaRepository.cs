﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class RespuestaRepository : IRespuestaRepository
    {
        FunctionsRepository _f = new FunctionsRepository();

        public List<Respuesta> GetPZRAWAP(int id, string idins, string idcal, string _plap) {

            List<Domain.Respuesta> re = new List<Domain.Respuesta>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try {

                        var es = session.
                                CreateSQLQuery(@"SELECT       *
                                                          FROM PZBCRIT,
                                                               PZBAREA,
                                                               PZVTYAW,
                                                               PZBPOLL,
                                                               PZBQSTN,
                                                               PZBCALR,
                                                               PZVPFLE,
                                                               PZVPOST,
                                                               PZVDNCY,
                                                               PZRDYPT,
                                                               PZBPRSO,
                                                               PZRASPE,
                                                               PZBAPPL,
                                                               PZRPLAP,
                                                               PZBPLQS,
                                                               PZRAWAP
                                                 WHERE  PZBPOLL.PZBPOLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE
                                                         AND PZBAPPL.PZBAPPL_POLL_CODE = PZBPLQS.PZBPLQS_POLL_CODE
                                                         AND PZRPLAP.PZRPLAP_POLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE
                                                         AND PZBAPPL.PZBAPPL_SEQ_NUMBER = PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER
                                                         AND PZBAPPL.PZBAPPL_SEQ_NUMBER = PZRPLAP.PZRPLAP_APPL_SEQ_NUMBER  
                                                         AND PZBCALR.PZBCALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                         AND PZBAPPL.PZBAPPL_CALR_CODE = PZBPLQS.PZBPLQS_CALR_CODE
                                                         AND PZVPFLE.PZVPFLE_CODE = PZBAPPL.PZBAPPL_PFLE_CODE
                                                         AND PZBQSTN.PZBQSTN_CODE = PZBPLQS.PZBPLQS_QSTN_CODE
                                                         AND PZBCRIT.PZBCRIT_CODE = PZBPLQS.PZBPLQS_CRIT_CODE
                                                         AND PZBAREA.PZBAREA_CODE = PZBPLQS.PZBPLQS_AREA_CODE
                                                         AND PZBPLQS.PZBPLQS_TYAW_CODE = PZVTYAW.PZVTYAW_CODE
                                                         AND PZBPLQS.PZBPLQS_SEQ_NUMBER = PZRAWAP.PZRAWAP_PLQS_SEQ_NUMBER
                                                         AND PZRPLAP.PZRPLAP_SEQ_NUMBER = PZRAWAP.PZRAWAP_PLAP_SEQ_NUMBER
                                                         AND PZRPLAP.PZRPLAP_POLL_CODE = PZBAPPL.PZBAPPL_POLL_CODE
                                                         AND PZRPLAP.PZRPLAP_CALR_CODE = PZBAPPL.PZBAPPL_CALR_CODE
                                                         AND PZRPLAP.PZRPLAP_PFLE_CODE = PZBAPPL.PZBAPPL_PFLE_CODE
                                                         AND PZRPLAP.PZRPLAP_PFLE_CODE = PZRASPE.PZRASPE_PFLE_CODE
                                                         AND PZRASPE.PZRASPE_PFLE_CODE = PZVPFLE.PZVPFLE_CODE
                                                         AND PZRPLAP.PZRPLAP_POST_CODE = PZRASPE.PZRASPE_POST_CODE
                                                         AND PZRASPE.PZRASPE_POST_CODE = PZRDYPT.PZRDYPT_POST_CODE
                                                         AND PZRDYPT.PZRDYPT_POST_CODE =  PZVPOST.PZVPOST_CODE
                                                         AND PZRPLAP.PZRPLAP_DNCY_CODE = PZRASPE.PZRASPE_DNCY_CODE
                                                         AND PZRASPE.PZRASPE_DNCY_CODE = PZRDYPT.PZRDYPT_DNCY_CODE
                                                         AND PZRDYPT.PZRDYPT_DNCY_CODE =  PZVDNCY.PZVDNCY_CODE
                                                         AND PZRPLAP.PZRPLAP_PIDM = PZRASPE.PZRASPE_PIDM
                                                         AND PZRASPE.PZRASPE_PIDM = PZBPRSO.PZBPRSO_PIDM
                                                       /*  AND PZBPOLL.PZBPOLL_CODE='EVAOBJ'
                                                         AND PZBPRSO.PZBPRSO_PIDM = '272588' 
                                                            AND PZBCALR.PZBCALR_CODE ='CAOB19' */
                                                     ORDER BY PZBPLQS.PZBPLQS_ORDER , PZBPLQS.PZBPLQS_AREA_CODE"
                                                    )
                                                    .AddEntity("PZBAREA", typeof(Domain.Area))
                                                    .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                    .AddEntity("PZBQSTN", typeof(Domain.Pregunta))
                                                    .AddEntity("PZVTYAW", typeof(Domain.TipoRespuesta))
                                                    .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                                    .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                                    .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                                    .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                    .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                                    .AddEntity("PZBCRIT", typeof(Domain.Criterio))
                                                    .AddEntity("PZVPOST", typeof(Domain.Cargo))
                                                    .AddEntity("PZVDNCY", typeof(Domain.Dependencia))
                                                    .AddEntity("PZRDYPT", typeof(Domain.AsignacionCargoDependencia))
                                                    .AddEntity("PZBPRSO", typeof(Domain.Persona))
                                                    .AddEntity("PZRASPE", typeof(Domain.AsignacionCargoPersona))
                                                    .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                                    .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPregunta))
                                                    .AddEntity("PZRAWAP", typeof(Domain.Respuesta))
                                                    .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                                    .List<Respuesta>();


                foreach (var x in es)
                        {
                            Domain.Respuesta resp = new Domain.Respuesta();
                            resp.PZRAWAP_SEQ_NUMBER = (int)x.PZRAWAP_SEQ_NUMBER;
                            resp.PZRAWAP_ACTIVITY_DATE = x.PZRAWAP_ACTIVITY_DATE;
                            resp.PZRAWAP_DESCRIPTION = x.PZRAWAP_DESCRIPTION;
                            resp.PZRAWAP_PLAP_SEQ_NUMBER = x.PZRAWAP_PLAP_SEQ_NUMBER;
                            resp.PZRAWAP_PLQS_SEQ_NUMBER = x.PZRAWAP_PLQS_SEQ_NUMBER;
                            resp.PZRAWAP_PERC_IMP = x.PZRAWAP_PERC_IMP;
                            resp.PZRAWAP_USER = x.PZRAWAP_USER;
                            resp.PZRAWAP_VALUE = x.PZRAWAP_VALUE;
                            resp.PZRAWAP_DATA_ORIGIN = x.PZRAWAP_DATA_ORIGIN;



                            Domain.AsignacionInstrumentoPregunta _insPre = new Domain.AsignacionInstrumentoPregunta();
                            _insPre.PZBPLQS_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_SEQ_NUMBER;
                            _insPre.PZBPLQS_USER = x.PZBPLQS.PZBPLQS_USER;
                            _insPre.PZBPLQS_DATA_ORIGIN = x.PZBPLQS.PZBPLQS_DATA_ORIGIN;
                            _insPre.PZBPLQS_ACTIVITY_DATE = x.PZBPLQS.PZBPLQS_ACTIVITY_DATE;
                            _insPre.PZBPLQS_QSTN_CODE = x.PZBPLQS.PZBPLQS_QSTN_CODE;
                            _insPre.PZBPLQS_POLL_CODE = x.PZBPLQS.PZBPLQS_POLL_CODE;
                            _insPre.PZBPLQS_CALR_CODE = x.PZBPLQS.PZBPLQS_CALR_CODE;
                            _insPre.PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS.PZBPLQS_APPL_SEQ_NUMBER;
                            _insPre.PZBPLQS_AREA_CODE = x.PZBPLQS.PZBPLQS_AREA_CODE;
                            _insPre.PZBPLQS_CRIT_CODE = x.PZBPLQS.PZBPLQS_CRIT_CODE;
                            _insPre.PZBPLQS_TYAW_CODE = x.PZBPLQS.PZBPLQS_TYAW_CODE;
                            _insPre.PZBPLQS_MAX_OVAL = x.PZBPLQS.PZBPLQS_MAX_OVAL;
                            _insPre.PZBPLQS_MAX_VALUE = x.PZBPLQS.PZBPLQS_MAX_VALUE;
                            _insPre.PZBPLQS_MIN_OVAL = x.PZBPLQS.PZBPLQS_MIN_OVAL;
                            _insPre.PZBPLQS_MIN_VALUE = x.PZBPLQS.PZBPLQS_MIN_VALUE;
                            _insPre.PZBPLQS_ORDER = x.PZBPLQS.PZBPLQS_ORDER;
                            _insPre.PZBPLQS_STATUS = x.PZBPLQS.PZBPLQS_STATUS;


                            Domain.InstrumentoAplicable apl = new Domain.InstrumentoAplicable();
                            apl.PZBAPPL_USER = x.PZBPLQS.PZBAPPL.PZBAPPL_USER;
                            apl.PZBAPPL_PRCNT = x.PZBPLQS.PZBAPPL.PZBAPPL_PRCNT;
                            apl.PZBAPPL_SEQ_NUMBER = x.PZBPLQS.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                            apl.PZBAPPL_POLL_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_POLL_CODE;
                            apl.PZBAPPL_PFLE_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_PFLE_CODE;
                            apl.PZBAPPL_CALR_CODE = x.PZBPLQS.PZBAPPL.PZBAPPL_CALR_CODE;
                            _insPre.PZBAPPL = apl;

                            Domain.Instrumento ins = new Domain.Instrumento();
                            ins.PZBPOLL_NAME = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_NAME;
                            ins.PZBPOLL_CODE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_CODE;
                            ins.PZBPOLL_USER = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_USER;
                            ins.PZBPOLL_DATA_ORIGIN = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_DATA_ORIGIN;
                            ins.PZBPOLL_ACTIVITY_DATE = x.PZBPLQS.PZBAPPL.PZBPOLL.PZBPOLL_ACTIVITY_DATE;
                            _insPre.PZBAPPL.PZBPOLL = ins;

                            //DATOS DEL criterio de Evaluacion
                            Domain.Criterio _cri = new Domain.Criterio();
                            _cri.PZBCRIT_CODE = x.PZBPLQS.PZBCRIT.PZBCRIT_CODE;
                            _cri.PZBCRIT_USER = x.PZBPLQS.PZBCRIT.PZBCRIT_USER;
                            _cri.PZBCRIT_DESCRIPTION = x.PZBPLQS.PZBCRIT.PZBCRIT_DESCRIPTION;
                            _cri.PZBCRIT_DATA_ORIGIN = x.PZBPLQS.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                            _cri.PZBCRIT_ACTIVITY_DATE = x.PZBPLQS.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                            _insPre.PZBCRIT = _cri;

                            //Datos del area a la cual pertenece la pregunta
                            Domain.Area _area = new Domain.Area();
                            _area.PZBAREA_CODE = x.PZBPLQS.PZBAREA.PZBAREA_CODE;
                            _area.PZBAREA_USER = x.PZBPLQS.PZBAREA.PZBAREA_USER;
                            _area.PZBAREA_DESCRIPTION = x.PZBPLQS.PZBAREA.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_DATA_ORIGIN = x.PZBPLQS.PZBAREA.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = x.PZBPLQS.PZBAREA.PZBAREA_ACTIVITY_DATE;
                            _insPre.PZBAREA = _area;

                            Domain.Pregunta item = new Domain.Pregunta();

                            item.PZBQSTN_CODE = x.PZBPLQS.PZBQSTN.PZBQSTN_CODE;
                            item.PZBQSTN_NAME = x.PZBPLQS.PZBQSTN.PZBQSTN_NAME;
                            item.PZBQSTN_USER = x.PZBPLQS.PZBQSTN.PZBQSTN_USER;
                            item.PZBQSTN_DESCRIPTION = x.PZBPLQS.PZBQSTN.PZBQSTN_DESCRIPTION;
                            item.PZBQSTN_DATA_ORIGIN = x.PZBPLQS.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                            item.PZBQSTN_ACTIVITY_DATE = x.PZBPLQS.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                            _insPre.PZBQSTN = item;

                            resp.PZBPLQS = _insPre;


                            re.Add(resp);

                        }





                    }

                    catch (Exception e)
                    {


                    }
                    List<Domain.AsignacionInstrumentoPregunta> obj = _f.NoAnswerQstn(id, idins, idcal, _plap);

                    int lon = obj.Count();

                    if (lon > 0)
                    {
                        foreach (var x in obj)
                        {

                            Domain.Respuesta resp = new Domain.Respuesta();
                            resp.PZRAWAP_SEQ_NUMBER = 0;
                            resp.PZRAWAP_ACTIVITY_DATE = DateTime.Today;
                            resp.PZRAWAP_DESCRIPTION = null;
                            resp.PZRAWAP_PLAP_SEQ_NUMBER = 0;
                            resp.PZRAWAP_PLQS_SEQ_NUMBER = 0;
                            resp.PZRAWAP_PERC_IMP = 0;
                            resp.PZRAWAP_USER = null;
                            resp.PZRAWAP_VALUE = 0;
                            resp.PZRAWAP_DATA_ORIGIN = null;

                            Domain.AsignacionInstrumentoPregunta _insPre = new Domain.AsignacionInstrumentoPregunta();

                            _insPre.PZBPLQS_SEQ_NUMBER = x.PZBPLQS_SEQ_NUMBER;
                            _insPre.PZBPLQS_USER = x.PZBPLQS_USER;
                            _insPre.PZBPLQS_DATA_ORIGIN = x.PZBPLQS_DATA_ORIGIN;
                            _insPre.PZBPLQS_ACTIVITY_DATE = x.PZBPLQS_ACTIVITY_DATE;
                            _insPre.PZBPLQS_QSTN_CODE = x.PZBPLQS_QSTN_CODE;
                            _insPre.PZBPLQS_POLL_CODE = x.PZBPLQS_POLL_CODE;
                            _insPre.PZBPLQS_CALR_CODE = x.PZBPLQS_CALR_CODE;
                            _insPre.PZBPLQS_APPL_SEQ_NUMBER = x.PZBPLQS_APPL_SEQ_NUMBER;
                            _insPre.PZBPLQS_AREA_CODE = x.PZBPLQS_AREA_CODE;
                            _insPre.PZBPLQS_CRIT_CODE = x.PZBPLQS_CRIT_CODE;
                            _insPre.PZBPLQS_TYAW_CODE = x.PZBPLQS_TYAW_CODE;
                            _insPre.PZBPLQS_MAX_OVAL = x.PZBPLQS_MAX_OVAL;
                            _insPre.PZBPLQS_MAX_VALUE = x.PZBPLQS_MAX_VALUE;
                            _insPre.PZBPLQS_MIN_OVAL = x.PZBPLQS_MIN_OVAL;
                            _insPre.PZBPLQS_MIN_VALUE = x.PZBPLQS_MIN_VALUE;
                            _insPre.PZBPLQS_ORDER = x.PZBPLQS_ORDER;
                            _insPre.PZBPLQS_STATUS = x.PZBPLQS_STATUS;
                            resp.PZBPLQS = _insPre;

                            //DATOS DEL criterio de Evaluacion
                            Domain.Criterio _cri = new Domain.Criterio();
                            _cri.PZBCRIT_CODE = x.PZBCRIT.PZBCRIT_CODE;
                            _cri.PZBCRIT_USER = x.PZBCRIT.PZBCRIT_USER;
                            _cri.PZBCRIT_DESCRIPTION = x.PZBCRIT.PZBCRIT_DESCRIPTION;
                            _cri.PZBCRIT_DATA_ORIGIN = x.PZBCRIT.PZBCRIT_DATA_ORIGIN;
                            _cri.PZBCRIT_ACTIVITY_DATE = x.PZBCRIT.PZBCRIT_ACTIVITY_DATE;
                            _insPre.PZBCRIT = _cri;
                            resp.PZBPLQS.PZBCRIT = _cri;

                            //Datos del area a la cual pertenece la pregunta
                            Domain.Area _area = new Domain.Area();
                            _area.PZBAREA_CODE = x.PZBAREA.PZBAREA_CODE;
                            _area.PZBAREA_USER = x.PZBAREA.PZBAREA_USER;
                            _area.PZBAREA_DESCRIPTION = x.PZBAREA.PZBAREA_DESCRIPTION;
                            _area.PZBAREA_DATA_ORIGIN = x.PZBAREA.PZBAREA_DATA_ORIGIN;
                            _area.PZBAREA_ACTIVITY_DATE = x.PZBAREA.PZBAREA_ACTIVITY_DATE;
                            _insPre.PZBAREA = _area;
                            resp.PZBPLQS.PZBAREA = _area;

                            Domain.Pregunta item = new Domain.Pregunta();

                            item.PZBQSTN_CODE = x.PZBQSTN.PZBQSTN_CODE;
                            item.PZBQSTN_NAME = x.PZBQSTN.PZBQSTN_NAME;
                            item.PZBQSTN_USER = x.PZBQSTN.PZBQSTN_USER;
                            item.PZBQSTN_DESCRIPTION = x.PZBQSTN.PZBQSTN_DESCRIPTION;
                            item.PZBQSTN_DATA_ORIGIN = x.PZBQSTN.PZBQSTN_DATA_ORIGIN;
                            item.PZBQSTN_ACTIVITY_DATE = x.PZBQSTN.PZBQSTN_ACTIVITY_DATE;
                            _insPre.PZBQSTN = item;
                            resp.PZBPLQS.PZBQSTN = item;

                            re.Add(resp);

                        }
                    }
                    return (re);

                }
            }


       }

        public Respuesta Add(Respuesta parametro)
        {
            throw new NotImplementedException();
        }

        public Respuesta GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public Respuesta GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(Respuesta parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(Respuesta parametro)
        {
            throw new NotImplementedException();
        }
    }
}