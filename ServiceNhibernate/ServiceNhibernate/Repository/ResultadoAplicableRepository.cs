﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;
using NHibernate.Transform;

namespace ServiceNhibernate.Repository
{
  
    class ResultadoAplicableRepository : IResultadoAplicableRepository
    {

       
         public List<ResultadosParciales> GetPZBAPRS(string _appl, string _poll,
                                                    string _calr, string _pidm,
                                                    string _perfil, string _status,
                                                    int _plap, string _type)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    List<Domain.ResultadoAplicable> result = new List<Domain.ResultadoAplicable>();
                    List<Domain.ResultadosParciales> valor = new List<Domain.ResultadosParciales>();


                    string estado = null;//_validator.validarStatusInstrumento(_pidm, _poll, _calr, _plap);

                    if (estado == "F")
                    {
                        //     return BadRequest("Este instrumento ya ha sido evaluado previamente");
                    }
                    else
                    {
                        try
                        {

                            var res = session.CreateSQLQuery(@" SELECT *
                                                              FROM 
                                                                   PZVTYRS PZVTYRS,
                                                                   PZBAPPL PZBAPPL,
                                                                   PZBPOLL PZBPOLL,
                                                                   PZBCALR PZBCALR,
                                                                   PZVPFLE PZVPFLE,
                                                                   PZBAPRS PZBAPRS 
                                                            WHERE pzbaprs_calr_code = pzbappl_calr_code
                                                              and pzbaprs_tyrs_code = pzvtyrs_code
                                                              and pzbaprs_poll_code = pzbappl_poll_code
                                                              and pzbaprs_appl_seq_number = pzbappl_seq_number
                                                              and pzbappl_calr_code = pzbcalr_code
                                                              and pzbappl_poll_code = pzbpoll_code" +
                                                              " and pzbappl_seq_number = '" + _appl + "' " +
                                                              " and pzbpoll_code = '" + _poll + "' " +
                                                              " and pzbcalr_code ='" + _calr + "' " +
                                                              " order by pzbaprs.pzbaprs_order asc ")

                                              .AddEntity("PZBPOLL", typeof(Domain.Instrumento))
                                              .AddEntity("PZBCALR", typeof(Domain.Calendario))
                                              .AddEntity("PZVPFLE", typeof(Domain.Perfil))
                                              .AddEntity("PZBAPPL", typeof(Domain.InstrumentoAplicable))
                                              .AddEntity("PZVTYRS", typeof(Domain.TipoResultado))
                                              .AddEntity("PZBAPRS", typeof(Domain.ResultadoAplicable))
                                               .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                              //    .SetResultTransformer(Transformers.AliasToBean(typeof(ResultadoAplicable)))
                                              .List<ResultadoAplicable>();



                            foreach (var X in res)
                            {

                                //Datos del Instrumento Aplicable
                                Domain.ResultadoAplicable _resApl = new Domain.ResultadoAplicable();
                                _resApl.PZBAPRS_CALR_CODE = X.PZBAPRS_CALR_CODE;
                                _resApl.PZBAPRS_APPL_SEQ_NUMBER = X.PZBAPRS_APPL_SEQ_NUMBER;
                                _resApl.PZBAPRS_POLL_CODE = X.PZBAPRS_POLL_CODE;
                                _resApl.PZBAPRS_SEQ_NUMBER = X.PZBAPRS_SEQ_NUMBER;
                                _resApl.PZBAPRS_USER = X.PZBAPRS_USER;
                                _resApl.PZBAPRS_DATA_ORIGIN = X.PZBAPRS_DATA_ORIGIN;
                                _resApl.PZBAPRS_ACTIVITY_DATE = X.PZBAPRS_ACTIVITY_DATE;
                                _resApl.PZBAPRS_ORDER = X.PZBAPRS_ORDER;
                                _resApl.PZBAPRS_APPL_SEQ_NUMBER = X.PZBAPRS_APPL_SEQ_NUMBER;
                                _resApl.PZBAPRS_TYRS_CODE = X.PZBAPRS_TYRS_CODE;

                                Domain.TipoResultado tr = new Domain.TipoResultado();
                                tr.PZVTYRS_CODE = X.PZVTYRS.PZVTYRS_CODE;
                                tr.PZVTYRS_DESCRIPTION = X.PZVTYRS.PZVTYRS_DESCRIPTION;
                                tr.PZVTYRS_ACTIVITY_DATE = X.PZVTYRS.PZVTYRS_ACTIVITY_DATE;
                                tr.PZVTYRS_DATA_ORIGIN = X.PZVTYRS.PZVTYRS_DATA_ORIGIN;
                                tr.PZVTYRS_USER = X.PZVTYRS.PZVTYRS_USER;
                                _resApl.PZVTYRS = tr;

                                Domain.InstrumentoAplicable _insApl = new Domain.InstrumentoAplicable();
                                _insApl.PZBAPPL_CALR_CODE = X.PZBAPPL.PZBAPPL_CALR_CODE;
                                _insApl.PZBAPPL_PFLE_CODE = X.PZBAPPL.PZBAPPL_PFLE_CODE;
                                _insApl.PZBAPPL_PRCNT = X.PZBAPPL.PZBAPPL_PRCNT;
                                _insApl.PZBAPPL_POLL_CODE = X.PZBAPPL.PZBAPPL_POLL_CODE;
                                _insApl.PZBAPPL_SEQ_NUMBER = X.PZBAPPL.PZBAPPL_SEQ_NUMBER;
                                _insApl.PZBAPPL_USER = X.PZBAPPL.PZBAPPL_USER;
                                _insApl.PZBAPPL_DATA_ORIGIN = X.PZBAPPL.PZBAPPL_DATA_ORIGIN;
                                _insApl.PZBAPPL_ACTIVITY_DATE = X.PZBAPPL.PZBAPPL_ACTIVITY_DATE;
                                //_insApl._PZBAPPL_POLL_CODE = X.PZBAPPL_POLL_CODE;
                                _resApl.PZBAPPL = _insApl;

                                Domain.Calendario cal = new Domain.Calendario();
                                cal.PZBCALR_CODE = X.PZBAPPL.PZBCALR.PZBCALR_CODE;
                                cal.PZBCALR_NAME = X.PZBAPPL.PZBCALR.PZBCALR_NAME;
                                cal.PZBCALR_USER = X.PZBAPPL.PZBCALR.PZBCALR_USER;
                                cal.PZBCALR_TERM = X.PZBAPPL.PZBCALR.PZBCALR_TERM;
                                cal.PZBCALR_SRL = X.PZBAPPL.PZBCALR.PZBCALR_SRL;
                                cal.PZBCALR_END_DATE = X.PZBAPPL.PZBCALR.PZBCALR_END_DATE;
                                cal.PZBCALR_INIT_DATE = X.PZBAPPL.PZBCALR.PZBCALR_INIT_DATE;
                                cal.PZBCALR_DATA_ORIGIN = X.PZBAPPL.PZBCALR.PZBCALR_DATA_ORIGIN;
                                cal.PZBCALR_ACTIVITY_DATE = X.PZBAPPL.PZBCALR.PZBCALR_ACTIVITY_DATE;

                                _resApl.PZBAPPL.PZBCALR = cal;

                                result.Add(_resApl);

                            }

                            int cont = result.Count;
                            decimal calculate = 0;

                            for (int dimension = 0; dimension < cont; dimension++)
                            {
                                Domain.ResultadosParciales item = new Domain.ResultadosParciales();
                                FunctionsRepository _f = new FunctionsRepository();


                                if (result[dimension].PZBAPRS_TYRS_CODE == "SUMOBJ")
                                {

                                    calculate = _f.GetSumAns(_perfil, _calr, _poll, _pidm);

                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "MEDOBJ")
                                {

                                    calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);

                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "PEROBJ")
                                {

                                    calculate = _f.GetPercObjT(_perfil, _calr, _poll, _pidm);

                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "TOTOBJ")
                                {

                                    calculate = _f.GetDifItem(_perfil, _calr, _poll, _pidm);

                                }


                                if (result[dimension].PZBAPRS_TYRS_CODE == "SUMCOM")
                                {

                                    calculate = _f.GetSumAns(_perfil, _calr, _poll, _pidm /*_area*/);


                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "MEDCOM")
                                {

                                    calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);

                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "PTOACU")
                                {


                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "SUMPTO")
                                {


                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "PERINS")
                                {

                                    calculate = _f.GetPercProf(_perfil, _calr, _poll, result[dimension].PZBAPPL.PZBCALR.PZBCALR_TERM);

                                }
                                if (result[dimension].PZBAPRS_TYRS_CODE == "EVAPER")
                                {

                                    calculate = _f.GetPercPoll(_perfil, result[dimension].PZBAPPL.PZBCALR.PZBCALR_TERM, _poll, _calr, _pidm);

                                }
                                if (result[dimension].PZBAPRS_TYRS_CODE == "MEDIA")
                                {

                                    calculate = _f.GetMedItem(_perfil, _calr, _poll, _pidm);

                                }

                                if (result[dimension].PZBAPRS_TYRS_CODE == "TAKVAL")
                                {

                                    //      calculate = 

                                }

                                item.PZRRSPL_NUMB_VALUE = (short)calculate;
                                item.PZRRSPL_APRS_SEQ_NUMBER = result[dimension].PZBAPRS_SEQ_NUMBER;
                                item.PZRRSPL_ACTIVITY_DATE = DateTime.Today;
                                item.PZRRSPL_DATA_ORIGIN = "BZPKEVAL";
                                item.PZRRSPL_USER = "SYSTEM";
                                item.PZRRSPL_PLAP_SEQ_NUMBER = _plap;

                                Domain.ResultadoAplicable _resApl = new Domain.ResultadoAplicable();
                                _resApl.PZBAPRS_CALR_CODE = result[dimension].PZBAPRS_CALR_CODE;
                                _resApl.PZBAPRS_POLL_CODE = result[dimension].PZBAPRS_POLL_CODE;
                                _resApl.PZBAPRS_SEQ_NUMBER = result[dimension].PZBAPRS_SEQ_NUMBER;
                                _resApl.PZBAPRS_USER = result[dimension].PZBAPRS_USER;
                                _resApl.PZBAPRS_DATA_ORIGIN = result[dimension].PZBAPRS_DATA_ORIGIN;
                                _resApl.PZBAPRS_ACTIVITY_DATE = result[dimension].PZBAPRS_ACTIVITY_DATE;
                                _resApl.PZBAPRS_ORDER = result[dimension].PZBAPRS_ORDER;
                                _resApl.PZBAPRS_APPL_SEQ_NUMBER = result[dimension].PZBAPRS_APPL_SEQ_NUMBER;
                                _resApl.PZBAPRS_TYRS_CODE = result[dimension].PZBAPRS_TYRS_CODE;


                                Domain.TipoResultado tr = new Domain.TipoResultado();
                                tr.PZVTYRS_CODE = result[dimension].PZVTYRS.PZVTYRS_CODE;
                                tr.PZVTYRS_DESCRIPTION = result[dimension].PZVTYRS.PZVTYRS_DESCRIPTION;
                                tr.PZVTYRS_ACTIVITY_DATE = result[dimension].PZVTYRS.PZVTYRS_ACTIVITY_DATE;
                                tr.PZVTYRS_DATA_ORIGIN = result[dimension].PZVTYRS.PZVTYRS_DATA_ORIGIN;
                                tr.PZVTYRS_USER = result[dimension].PZVTYRS.PZVTYRS_USER;
                                _resApl.PZVTYRS = tr;
                                item.PZBAPRS = _resApl;

                                valor.Add(item);

                            }


                        }



                        catch (Exception e)
                        {

                        }



                        FunctionsRepository _reg = new FunctionsRepository();
                        string resultParcial = "";

                        if (_type == "A")
                        {
                            // resultParcial = _reg.PostPZRRSPL(_status, _plap, valor);
                        }

                        else
                        {

                            if (_type == "E")
                            {

                                ///   resultParcial = _reg.PutPZRRSPL(_status, _plap, valor);

                            }

                        }


                        if (resultParcial == "Error")
                        {

                            return null;
                        }



                        return valor;
                    }
                }
            
            }
            return null;
        }

        public ResultadoAplicable Add(ResultadoAplicable parametro)
        {
            throw new NotImplementedException();
        }

        public ResultadoAplicable GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public ResultadoAplicable GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(ResultadoAplicable parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(ResultadoAplicable parametro)
        {
            throw new NotImplementedException();
        }
    }
}