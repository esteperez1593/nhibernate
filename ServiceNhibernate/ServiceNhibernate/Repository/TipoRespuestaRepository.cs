﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class TipoRespuestaRepository : ITipoRespuestaRepository
    {

        public List<TipoRespuesta> GetPZVTYAWs()
        {
            List<Domain.TipoRespuesta> result = new List<Domain.TipoRespuesta>();

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        var res = session.CreateSQLQuery(@"Select * from PZVTYAW")
                                              .AddEntity("PZVTYAW", typeof(Domain.TipoRespuesta))
                                              .SetResultTransformer(new FirstTupleDistinctResultTransformer())
                                              .List<TipoRespuesta>();


                        foreach (var x in res)
                        {

                            //Datos del tipo de Respuesta
                            Domain.TipoRespuesta tr = new Domain.TipoRespuesta();
                            tr.PZVTYAW_CODE = x.PZVTYAW_CODE;
                            tr.PZVTYAW_DESCRIPTION = x.PZVTYAW_DESCRIPTION;
                            tr.PZVTYAW_ACTIVITY_DATE = x.PZVTYAW_ACTIVITY_DATE;
                            tr.PZVTYAW_DATA_ORIGIN = x.PZVTYAW_DATA_ORIGIN;
                            tr.PZVTYAW_USER = x.PZVTYAW_USER;

                            result.Add(tr);

                        }

                        return (result);

                    }

                    catch (Exception e)
                    {


                    }
                }
            }
            return result;
        }

        public TipoRespuesta Add(TipoRespuesta parametro)
        {
            throw new NotImplementedException();
        }

        public TipoRespuesta GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public TipoRespuesta GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

    
        public void Remove(TipoRespuesta parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(TipoRespuesta parametro)
        {
            throw new NotImplementedException();
        }
    }
}