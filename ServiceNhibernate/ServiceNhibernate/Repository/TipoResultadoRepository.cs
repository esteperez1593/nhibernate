﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;
using NHibernate.Transform;

namespace ServiceNhibernate.Repository
{
    class TipoResultadoRepository : ITipoResultadoRepository
    {
        public TipoResultado Add(TipoResultado parametro)
        {
            throw new NotImplementedException();
        }

        public TipoResultado GetById(decimal parametro)
        {
            throw new NotImplementedException();
        }

        public TipoResultado GetByName(string parametro)
        {
            throw new NotImplementedException();
        }

        public void Remove(TipoResultado parametro)
        {
            throw new NotImplementedException();
        }

        public void Update(TipoResultado parametro)
        {
            throw new NotImplementedException();
        }

        public List<TipoResultado> GetPZVTYRS()
        {

            List<Domain.TipoResultado> result = new List<Domain.TipoResultado>();
            using (ISession session = NHibernateHelper.OpenSession())
            {

                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        var res = session.
                         CreateSQLQuery(@"Select * from PZVTYRS")
                         .SetResultTransformer(Transformers.AliasToBean(typeof(TipoResultado)))
                         .List<TipoResultado>();


                        foreach (var x in res)
                        {

                            //Datos del tipo de Respuesta
                            Domain.TipoResultado tr = new Domain.TipoResultado();
                            tr.PZVTYRS_CODE = x.PZVTYRS_CODE;
                            tr.PZVTYRS_DESCRIPTION = x.PZVTYRS_DESCRIPTION;
                            tr.PZVTYRS_ACTIVITY_DATE = x.PZVTYRS_ACTIVITY_DATE;
                            tr.PZVTYRS_DATA_ORIGIN = x.PZVTYRS_DATA_ORIGIN;
                            tr.PZVTYRS_USER = x.PZVTYRS_USER;

                            result.Add(tr);

                        }

                        return (result);

                    }

                    catch (Exception e)
                    {

                    }


                }
            }
            return (result);
        }
    }
}