﻿using ServiceNhibernate.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceNhibernate.Domain;
using NHibernate;

namespace ServiceNhibernate.Repository
{
    class ValidatorRepository : IValidatorRepository
    {
        public String validarComponentesInstrumentos(string parametro)
        {

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        var res = session.CreateSQLQuery(@"SELECT  pzbplqs.* 
                                                            FROM pzbplqs,pzbarea
                                                            WHERE pzbplqs.pzbplqs_area_code = pzbarea.pzbarea_code
                                                                and pzbarea_code ='" + parametro + "' " +
                                                        " UNION SELECT   pzbplqs.* " +
                                                          "  FROM pzbplqs,pzbcrit " +
                                                          "  WHERE pzbplqs.pzbplqs_crit_code = pzbcrit.pzbcrit_code " +
                                                           " and pzbcrit.pzbcrit_code = '" + parametro + "' " +
                                                     "   UNION SELECT  pzbplqs.* " +
                                                         "   FROM pzbplqs,pzbqstn " +
                                                          "  where pzbplqs.pzbplqs_qstn_code = pzbqstn.pzbqstn_code " +
                                                          "  and pzbqstn.pzbqstn_code ='" + parametro + "' " +
                                                     " UNION SELECT  pzbplqs.* " +
                                                         "   FROM pzbplqs,pzbcalr " +
                                                         "   where pzbplqs.pzbplqs_calr_code = pzbcalr_code " +
                                                          "  and pzbcalr.pzbcalr_code ='" + parametro + "'")
                                            .AddEntity("PZBPLQS", typeof(Domain.AsignacionInstrumentoPregunta))
                                            .SetResultTransformer(new FirstTupleDistinctResultTransformer());

                        if (res != null)
                        {

                            return "CAMPO ASIGNADO";

                        }
                        else
                        {
                            return "OK";

                        }

                    }
                    catch (Exception e) { }
                    {

                    }

                    return "Error";



                }
            }
        }

        public String validarStatusPoll(string _pidm, string _poll, string _calr, int id)
        {
            Domain.AsignacionInstrumentoPersona _plap = new Domain.AsignacionInstrumentoPersona();

            string _est = "";

            using (ISession session = NHibernateHelper.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        var res = session.CreateSQLQuery(@" Select *  from PZRPLAP 
                                                            where pzrplap_poll_code = '" + _poll + "' " +
                                                            " and pzrplap_calr_code = '" + _calr + "' " +
                                                            " and pzrplap_pidm = '" + _pidm + "'  " +
                                                            " and PZRPLAP_SEQ_NUMBER = '" + id + "' ")
                                            .AddEntity("PZRPLAP", typeof(Domain.AsignacionInstrumentoPersona))
                                            .SetResultTransformer(new FirstTupleDistinctResultTransformer()).List<AsignacionInstrumentoPersona>();

                        foreach (var result in res)
                        {

                            _plap.PZRPLAP_STATUS = result.PZRPLAP_STATUS;
                            _est = _plap.PZRPLAP_STATUS;
                        }

                        if (_est == "F")
                        {

                            return ("Este instrumento ya se encuentra evaluado");

                        }
                        else
                        {
                            return ("");
                        }


                    }
                    catch (Exception e) { }



                    {

                    }



                    return "Error";


                }
            }
        }
    }
}

